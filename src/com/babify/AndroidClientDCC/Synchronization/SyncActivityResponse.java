package com.babify.AndroidClientDCC.Synchronization;

import com.babify.AndroidClientDCC.DomainModel.ScheduledActivity;
import com.babify.AndroidClientDCC.DomainModel.ScheduledActivityDate;
import com.babify.AndroidClientDCC.DomainModel.SyncAction;

import java.util.ArrayList;
import java.util.List;

public class SyncActivityResponse extends SyncActivityRequest {
    public String id;

    public ScheduledActivity getScheduledActivity(String kigaId, long actId){
        List<ScheduledActivityDate> actDates = new ArrayList<ScheduledActivityDate>();
        List<String> kidsExternalIds = new ArrayList<String>();

        if(dates != null){
            ScheduledActivityDate newDate;
            for(SyncActivityDateRequest date:dates){
                newDate = new ScheduledActivityDate(0, date.id, Long.valueOf(date.scheduled_at)*1000L, Integer.valueOf(date.notification_schedule),date.description);
                actDates.add(newDate);
            }
        }

        if(children != null){
            for(SyncActivityChildRequest child:children){
                if(child.is_active){
                    kidsExternalIds.add(child.id);
                }
            }
        }

        return new ScheduledActivity(kigaId, SyncAction.None.getCode(), actId, id, is_active, Integer.valueOf(activity_type_id), name, description, actDates, kidsExternalIds);
    }
}