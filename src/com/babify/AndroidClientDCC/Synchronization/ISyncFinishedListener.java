package com.babify.AndroidClientDCC.Synchronization;

public interface ISyncFinishedListener {
    public void SyncFinished(int totalCount, int successCount, String type);
}
