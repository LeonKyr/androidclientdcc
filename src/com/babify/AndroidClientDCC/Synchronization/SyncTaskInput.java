package com.babify.AndroidClientDCC.Synchronization;

import android.content.Context;

public class SyncTaskInput{
    public Context context;
    public String kigaId;
    public Boolean isSynchnonizing;
    public String type;
    public String deviceId;

    public static String NewImagesType = "kids";
    public static String GeneralType = "general";
    public static String EventsOnlyType = "events";
    public static String KidsOnlyType = "kids";
    public static String KidContactsOnlyType = "kidcontacts";

    private int mUpdatesCount = 0;
    private int mTotalUpdatesCount = 0;
    private int mSuccessCount = 0;
    private ISyncFinishedListener mListener;

    public int getUpdatesCount(){
        return mTotalUpdatesCount;
    }

    public SyncTaskInput(ISyncFinishedListener listener){
        mListener = listener;
    }

    public void decrement(boolean isSuccess){
        mUpdatesCount -= 1;

        if(isSuccess)
            mSuccessCount += 1;

        if(mUpdatesCount == 0){
            forceFinish();
        }
    }

    public void forceFinish(){
        mListener.SyncFinished(mTotalUpdatesCount, mSuccessCount, type);
    }

    public void increment(int count){
        mUpdatesCount += count;
        mTotalUpdatesCount += count;
    }

    public void clear(){
        mUpdatesCount = 0;
        mTotalUpdatesCount = 0;
        mSuccessCount = 0;
    }
}