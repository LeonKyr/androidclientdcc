package com.babify.AndroidClientDCC.Synchronization;

import java.util.List;

public class SyncActivityRequest {
    public SyncDevice device;
    public String teacher_id;
    public String name;
    public String description;
    public Boolean is_active;
    public String activity_type_id;
    public List<SyncActivityChildRequest> children;
    public List<SyncActivityDateRequest> dates;
}

