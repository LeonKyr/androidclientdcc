package com.babify.AndroidClientDCC.Synchronization;

import java.util.List;

public class SyncKidResponse extends SyncKidBase{
    public String id;
    public String code;
    public String hasListeners;
    public SyncImageResponse image;
    public List<SyncKidContactResponse> contacts;
}