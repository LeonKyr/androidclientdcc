package com.babify.AndroidClientDCC.Synchronization;

import java.util.List;

public class SyncEventRequest{
    public List<SyncEventChild> children;
    public List<SyncEventProperty> properties;
}