package com.babify.AndroidClientDCC.Synchronization;

import android.os.AsyncTask;
import com.babify.AndroidClientDCC.DataLayer.DatabaseHelper;
import com.babify.AndroidClientDCC.DomainModel.*;
import com.babify.AndroidClientDCC.Helpers.WebServiceHelper;

import java.util.ArrayList;
import java.util.List;

public class SyncTask extends AsyncTask<SyncTaskInput, Void, Boolean> {
    SyncTaskInput input;

    protected Boolean doInBackground(SyncTaskInput... inputs) {
        input = inputs[0];

        //if(!input.isSynchnonizing)
        {
            try
            {
                List<Group> groups = new ArrayList<Group>();
                List<Teacher> teachers = new ArrayList<Teacher>();
                List<Kid> kids = new ArrayList<Kid>();
                List<Feedback> feedbacks = new ArrayList<Feedback>();
                List<Event> events = new ArrayList<Event>();
                List<KidContact> kidContacts = new ArrayList<KidContact>();
                List<Media> medias = new ArrayList<Media>();
                List<ScheduledActivity> scheduledActivities = new ArrayList<ScheduledActivity>();

                Kindergarten kiga = null;

                final DatabaseHelper dbHelper = new DatabaseHelper(input.context);

                Boolean makeBackSync = false;

                if(input.type.equals(SyncTaskInput.NewImagesType))
                    medias = dbHelper.readMediaForSync(input.kigaId, true);
                else if(input.type.equals(SyncTaskInput.KidsOnlyType))
                    kids = dbHelper.readKids(input.kigaId, 0, true);
                else if (input.type.equals(SyncTaskInput.EventsOnlyType))
                    events = dbHelper.readEventsForSync(input.kigaId);
                else if (input.type.equals(SyncTaskInput.KidContactsOnlyType))
                    kidContacts = dbHelper.readKidContactsForSync(input.kigaId);
                else{
                    groups = dbHelper.readGroups(input.kigaId, true);
                    teachers = dbHelper.readTeachers(input.kigaId, true);
                    kids = dbHelper.readKids(input.kigaId, 0, true);
                    kidContacts = dbHelper.readKidContactsForSync(input.kigaId);
                    feedbacks = dbHelper.readFeedbacksForSync(input.kigaId);

                    kiga = dbHelper.getKindergarten(input.kigaId);
                    if(kiga.getSyncAction() == SyncAction.None.getCode())
                        kiga = null;

                    events = dbHelper.readEventsForSync(input.kigaId);
                    medias = dbHelper.readMediaForSync(input.kigaId, false);

                    scheduledActivities = dbHelper.readScheduledActivities(input.kigaId,true);

                    makeBackSync = true;
                    input.increment(1); // back sync
                }

                input.increment(groups.size());
                input.increment(teachers.size());
                input.increment(kids.size());
                input.increment(feedbacks.size());
                input.increment(events.size());
                input.increment(medias.size());
                input.increment(scheduledActivities.size());

                if(kiga != null) input.increment(1);

                for(Group group:groups){
                    try
                    {
                        WebServiceHelper.TrySyncGroup(input, group);
                    }catch(Exception ignored){input.decrement(false);}
                }

                for(Teacher teacher:teachers){
                    try
                    {
                        WebServiceHelper.TrySyncTeacher(input, teacher);
                    }catch(Exception ignored){input.decrement(false);}
                }

                for(Kid kid:kids){
                    try
                    {
                        WebServiceHelper.TrySyncKid(input, kid);
                    }catch(Exception ignored){input.decrement(false);}
                    }

                for(Feedback feedback:feedbacks){
                    try
                    {
                        WebServiceHelper.TrySyncFeedback(input, feedback);
                    }catch(Exception ignored){input.decrement(false);}
                }

                if(kiga != null){
                    try
                    {
                        WebServiceHelper.TrySyncKiga(input, kiga);
                    }catch(Exception ignored){input.decrement(false);}
                }

                for(Event event:events){
                    try
                    {
                        WebServiceHelper.TrySyncEvent(input, event);
                    }catch(Exception ignored){input.decrement(false);}
                }

                for(KidContact kidContact:kidContacts){
                    try
                    {
                        WebServiceHelper.TrySyncKidContact(input, kidContact);
                    }catch(Exception ignored){input.decrement(false);}
                }

                for(Media media:medias){
                    try
                    {
                        WebServiceHelper.TrySyncMedia(input, media);
                    }catch(Exception ignored){input.decrement(false);}
                }

                for(ScheduledActivity activity:scheduledActivities){
                    try
                    {
                        WebServiceHelper.TrySyncScheduledActivity(input, activity);
                    }catch(Exception ignored){input.decrement(false);}
                }

                if(makeBackSync){
                    try{
                        WebServiceHelper.TryGetUpdates(input);
                    }catch(Exception ignored){input.decrement(false);}
                }

                if(input.getUpdatesCount() == 0)
                    input.forceFinish();
            }
            catch(Exception ex){
                return false;
            }
            finally {
            }

        }
        return true;
    }

    protected void onPostExecute(Boolean isSuccess) {
        if(isSuccess){
            //DCCHelper.showToast(input.context, "Successfully synchronized data!", Toast.LENGTH_SHORT, DCCHelper.TDSuccess);
        }else{
            //DCCHelper.showToast(input.context, "Error while synchronizing data!", Toast.LENGTH_SHORT, DCCHelper.TDError);
        }
    }
}