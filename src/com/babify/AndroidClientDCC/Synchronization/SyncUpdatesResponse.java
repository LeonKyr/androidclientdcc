package com.babify.AndroidClientDCC.Synchronization;

import java.util.List;

public class SyncUpdatesResponse{
    public SyncDevice device;
    public SyncKigaResponse kindergarten;
    public List<SyncKidResponse> children;
    public List<SyncTeacherResponse> teachers;
    public List<SyncGroupResponse> groups;
    public List<SyncActivityResponse> activities;
}