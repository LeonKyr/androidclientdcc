package com.babify.AndroidClientDCC.Activities;

import android.content.*;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.babify.AndroidClientDCC.BabifyApplication;
import com.babify.AndroidClientDCC.Helpers.DCCHelper;
import com.babify.AndroidClientDCC.Helpers.WebServiceHelper;
import com.babify.AndroidClientDCC.R;
import com.mbalychev.Shared.Helpers.UIHelper;

public class MainActivity extends SliderBaseActivity {

    TextView syncStatusText;
    TextView syncStatusMessageText;
    ImageView syncStatusImage;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_main, R.layout.left_slider_menu_full, R.string.babify);

        syncStatusText = (TextView)findViewById(R.id.sync_status);
        syncStatusMessageText = (TextView)findViewById(R.id.sync_status_message);
        syncStatusImage = (ImageView)findViewById(R.id.sync_status_image);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setSyncBox();
    }

    private void setSyncBox(){
        if(isSynchnonizing()){
            setSyncBoxFields(R.string.synchronizingStatus, R.string.systemSynchronizing, R.drawable.pm_sync);
        }else{
            if(getDbHelper().isSystemOnSync(getCurrentKigaId())){
                setSyncBoxFields(R.string.synchronizedStatus, R.string.systemInSync, R.drawable.pm_ok);
            }else{
                setSyncBoxFields(R.string.notSynchronizedStatus, R.string.systemNotInSync, R.drawable.pm_error);
            }
        }
    }

    private void setSyncBoxFields(int statusResourceId, int messageId, int imageId){
        syncStatusText.setText(getString(statusResourceId));
        syncStatusMessageText.setText(getString(messageId));
        syncStatusImage.setImageResource(imageId);
    }

    public void onSync(View v){
        if(UIHelper.hasNetwork(getApplicationContext())){
            setSyncBoxFields(R.string.synchronizingStatus, R.string.systemSynchronizing, R.drawable.pm_sync);
            ((BabifyApplication)getApplicationContext()).runSyncTask(getCurrentKigaId());
        }else{
            UIHelper.showToast(getApplicationContext(), getString(R.string.noConnection), Toast.LENGTH_LONG, UIHelper.TDError);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        super.onCreateOptionsMenu(menu);
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_logout:
                getDbHelper().deactivateActiveKiga();
                intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }

        return true;
    }

    public void onClick(View view){
        int id = view.getId();

        Intent intent = null;
        switch (id)
        {
            case R.id.menu_groups:
                intent = new Intent(MainActivity.this, GroupsActivity.class);
                break;
            case R.id.menu_kids:
                intent = new Intent(MainActivity.this, KidsActivity.class);
                break;
            case R.id.menu_teachers:
                intent = new Intent(MainActivity.this, TeachersActivity.class);
                break;
            case R.id.menu_new_event:
                intent = new Intent(MainActivity.this, NewEventActivity.class);
                break;
            /*case R.id.menu_info:
                intent = new Intent(MainActivity.this, KindergartenEditActivity.class);
                break;*/
            case R.id.menu_scheduler:
                intent = new Intent(MainActivity.this, SchedulerActivity.class);
                break;
            case R.id.menu_reports:
                intent = new Intent(MainActivity.this, ReportsActivity.class);
                break;

            /*case R.id.menu_tutorial:
                ShowTutorial();
                break;*/
        }

        if(intent != null)
            startActivity(intent);
    }

    @Override
    public void SyncFinished(Integer total, Integer success) {
        super.SyncFinished(total, success);
        setSyncBox();
    }
}