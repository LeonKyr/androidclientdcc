package com.babify.AndroidClientDCC.Activities;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import com.babify.AndroidClientDCC.BabifyApplication;
import com.babify.AndroidClientDCC.DomainModel.Kindergarten;
import com.babify.AndroidClientDCC.Helpers.DCCHelper;
import com.babify.AndroidClientDCC.R;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Required;

public class KindergartenEditActivity extends SliderBaseActivity  implements Validator.ValidationListener{
    @Required(order = 1)
    EditText name;

    @Required(order = 2)
    EditText address;

    @Required(order = 3)
    EditText phone;

    @Required(order = 4)
    EditText contactPerson;

    @Required(order = 5)
    @Email(order = 6, message = "Enter valid email")
    EditText email;
    CheckBox isAutoLogin;
    Button changePassword;
    Validator validator;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //super.onCreate(savedInstanceState, R.layout.kiga_info_edit, R.layout.left_slider_menu_full, R.string.updateInfo);
        setContentView(R.layout.kiga_info_edit);

        name = (EditText)findViewById(R.id.kindergarten_name);
        address = (EditText)findViewById(R.id.kindergarten_address);
        phone = (EditText)findViewById(R.id.kindergarten_phone);
        email = (EditText)findViewById(R.id.kindergarten_email);
        contactPerson = (EditText)findViewById(R.id.kindergarten_contactPerson);
        isAutoLogin = (CheckBox)findViewById(R.id.kindergarten_is_autologin);
        changePassword = (Button)findViewById(R.id.kindergarten_change_pwd_btn);

        Kindergarten kiga = getDbHelper().getKindergarten(getCurrentKigaId());

        name.setText(kiga.getName());
        address.setText(kiga.getAddress());
        phone.setText(kiga.getPhone());
        email.setText(kiga.getEmail());
        contactPerson.setText(kiga.getContactPerson());
        isAutoLogin.setChecked(kiga.getIsAutoLogin());

        ActionBar bar = getActionBar();
        bar.setSubtitle(getString(R.string.updateInfo));

        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_kindergarten, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case R.id.action_save_kiga_info:
                save();
                break;
            default:
                break;
        }

        return true;
    }

    public void save(){
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        getDbHelper().updateKigaInfo(getCurrentKigaId(), name.getText().toString(), address.getText().toString(), phone.getText().toString(),
                                email.getText().toString(), isAutoLogin.isChecked(), contactPerson.getText().toString());
        setCurrentKindergarten(getDbHelper().getKindergarten(getCurrentKigaId()));

        ((BabifyApplication)getApplicationContext()).runSyncTask(getCurrentKigaId());
        finish();
    }

    @Override
    public void onValidationFailed(View failedView, Rule<?> failedRule) {
        DCCHelper.validationFailedRule(getApplicationContext(), failedView, failedRule);
    }

    public void changePwdClick(View v){
        Intent intent = new Intent(KindergartenEditActivity.this, ChangePasswordActivity.class);
        startActivity(intent);
    }
}