package com.babify.AndroidClientDCC.Activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.babify.AndroidClientDCC.Adapters.ActivityDatesAdapter;
import com.babify.AndroidClientDCC.Adapters.KidsAdapter;
import com.babify.AndroidClientDCC.BabifyApplication;
import com.babify.AndroidClientDCC.DomainModel.Action.SpinnerKeyValuePair;
import com.babify.AndroidClientDCC.DomainModel.Group;
import com.babify.AndroidClientDCC.DomainModel.Internal.ButtonTagContainer;
import com.babify.AndroidClientDCC.DomainModel.Kid;
import com.babify.AndroidClientDCC.DomainModel.ScheduledActivity;
import com.babify.AndroidClientDCC.DomainModel.ScheduledActivityDate;
import com.babify.AndroidClientDCC.DomainModel.SyncAction;
import com.babify.AndroidClientDCC.Helpers.DCCHelper;
import com.babify.AndroidClientDCC.R;
import com.mbalychev.Shared.Helpers.UIHelper;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Required;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class SchedulerActivityEditActivity  extends SliderBaseActivity implements Validator.ValidationListener {
    private boolean isNew;
    private ScheduledActivity existingActivity;
    private long id;
    private int ADD_KID_REQUEST_CODE = 1;
    private int ADD_DATE_REQUEST_CODE = 2;

    Validator validator;

    ListView kidsList;
    ListView datesList;
    CheckBox isActiveCheck;

    @Required(order = 1)
    EditText titleText;
    @Required(order = 2)
    EditText descriptionText;
    @Required(order = 3)
    Spinner typeSpinner;

    private List<String> kidsExternalIds;
    private List<ScheduledActivityDate> dates;

    public void onCreate(Bundle savedInstanceState) {
        //super.onCreate(savedInstanceState);
        super.onCreate(savedInstanceState, R.layout.scheduledactivity_edit, 0, R.string.updateScheduledActivity);

        kidsList = (ListView) findViewById(R.id.activity_kids_listview);
        datesList = (ListView) findViewById(R.id.activity_dates_listview);
        isActiveCheck = (CheckBox) findViewById(R.id.activity_active);
        titleText = (EditText) findViewById(R.id.activity_title);
        descriptionText = (EditText) findViewById(R.id.activity_description);
        typeSpinner = (Spinner)findViewById(R.id.activity_type);

        ArrayAdapter typeAdapter = new ArrayAdapter(this,
                android.R.layout.simple_spinner_item, new SpinnerKeyValuePair[] {
                new SpinnerKeyValuePair( 0, getResources().getString(com.babify.AndroidClientDCC.R.string.scheduledactivity_type_0) ),
                new SpinnerKeyValuePair( 1, getResources().getString(com.babify.AndroidClientDCC.R.string.scheduledactivity_type_1) ),
                new SpinnerKeyValuePair( 2, getResources().getString(com.babify.AndroidClientDCC.R.string.scheduledactivity_type_2) ),
                new SpinnerKeyValuePair( 3, getResources().getString(com.babify.AndroidClientDCC.R.string.scheduledactivity_type_3) ),
                new SpinnerKeyValuePair( 4, getResources().getString(com.babify.AndroidClientDCC.R.string.scheduledactivity_type_4) ),
                new SpinnerKeyValuePair( 5, getResources().getString(com.babify.AndroidClientDCC.R.string.scheduledactivity_type_5) ),
                new SpinnerKeyValuePair( 6, getResources().getString(com.babify.AndroidClientDCC.R.string.scheduledactivity_type_6) ),
                new SpinnerKeyValuePair( 7, getResources().getString(com.babify.AndroidClientDCC.R.string.scheduledactivity_type_7) ),
                new SpinnerKeyValuePair( 8, getResources().getString(com.babify.AndroidClientDCC.R.string.scheduledactivity_type_8) ),
                new SpinnerKeyValuePair( 9, getResources().getString(com.babify.AndroidClientDCC.R.string.scheduledactivity_type_9) ),
        });
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typeSpinner.setAdapter(typeAdapter);
        typeSpinner.setSelection(0);

        Intent intent = getIntent();
        id = intent.getLongExtra("Id",0);

        isNew = id == 0;

        ActionBar bar = getActionBar();
        if(!isNew){
            existingActivity = getDbHelper().getScheduledActivity(getCurrentKigaId(), id);

            isActiveCheck.setChecked(existingActivity.getIsActive());
            titleText.setText(existingActivity.getTitle());
            descriptionText.setText(existingActivity.getDescription());
            typeSpinner.setSelection(existingActivity.getType());
            kidsExternalIds = existingActivity.getKidsExternalIds();

            dates = existingActivity.getDates();

            bar.setSubtitle(getString(R.string.updateScheduledActivity));
        }
        else{
            bar.setSubtitle(getString(R.string.newScheduledActivity));
        }

        if(kidsExternalIds == null)
            kidsExternalIds = new ArrayList<String>();
        if(dates == null)
            dates = new ArrayList<ScheduledActivityDate>();

        updateKidsAdapter();
        updateDatesAdapter();

        datesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ScheduledActivityDate selectedDate = dates.get(position);
                Intent intent = new Intent(SchedulerActivityEditActivity.this, ScheduledActivityDateEditActivity.class);
                intent.putExtra("Id",selectedDate.getId());
                intent.putExtra("date",selectedDate.getDate());
                intent.putExtra("reminderType",selectedDate.getReminderType());
                intent.putExtra("reminderMessage",selectedDate.getReminderMessage());
                startActivityForResult(intent, ADD_DATE_REQUEST_CODE);
            }
        });

        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    @Override
    public void onValidationSucceeded() {
        SaveActivity();
        ((BabifyApplication)getApplicationContext()).runSyncTask(getCurrentKigaId());
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onValidationFailed(View failedView, Rule<?> failedRule) {
        DCCHelper.validationFailedRule(getApplicationContext(), failedView, failedRule);
    }

    private void updateKidsAdapter(){
        List<Kid> kids = getDbHelper().readKids(getCurrentKigaId(), 0, kidsExternalIds, false);
        kidsList.setAdapter(new KidsAdapter(SchedulerActivityEditActivity.this,R.layout.scheduledactivity_kid_row, kids));
    }

    private void updateDatesAdapter(){
        Collections.sort(dates);

        datesList.setAdapter(new ActivityDatesAdapter(SchedulerActivityEditActivity.this,R.layout.scheduledactivity_date_row, dates));
    }

    @Override
    public void onGenericClick(View v){
        ImageView btn = (ImageView)v;
        ButtonTagContainer tagContainer = (ButtonTagContainer)btn.getTag();

        if(tagContainer.getName().equals("kid")){
            String externalId = (String)tagContainer.getValue();

            for (Iterator<String> iter = kidsExternalIds.listIterator(); iter.hasNext(); ) {
                String a = iter.next();
                if (a.equals(externalId)) {
                    iter.remove();
                }
            }

            updateKidsAdapter();
        }
        else if(tagContainer.getName().equals("activityDate")){
            Long dateId = (Long)tagContainer.getValue();

            for (Iterator<ScheduledActivityDate> iter = dates.listIterator(); iter.hasNext(); ) {
                ScheduledActivityDate a = iter.next();
                if (a.getId() == dateId) {
                    iter.remove();
                }
            }

            updateDatesAdapter();
        }
    }

    public void addKid(View v){
        Intent intent = new Intent(this, ChooseKidActivity.class);
        startActivityForResult(intent, ADD_KID_REQUEST_CODE);
    }

    public void addDate(View v){
        Intent intent = new Intent(this, ScheduledActivityDateEditActivity.class);
        startActivityForResult(intent, ADD_DATE_REQUEST_CODE);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == ADD_KID_REQUEST_CODE) {
                ArrayList<String> newKidExternalIds = data.getStringArrayListExtra("kidExternalIds");
                if(newKidExternalIds != null && newKidExternalIds.size() > 0){
                    for(String newKidExternalId:newKidExternalIds){
                        if(!kidsExternalIds.contains(newKidExternalId)){
                            kidsExternalIds.add(newKidExternalId);
                        }
                    }
                    updateKidsAdapter();
                }
            }
            if (requestCode == ADD_DATE_REQUEST_CODE) {
                long id = data.getLongExtra("Id",0);
                Long date = data.getLongExtra("date",0);
                Integer reminderType = data.getIntExtra("reminderType",2); // 12am of the previous day
                String reminderMessage = data.getStringExtra("reminderMessage");

                if(date > 0){
                    if(id != 0){
                        for(ScheduledActivityDate scheduledActivityDate:dates){
                            if(scheduledActivityDate.getId() == id){
                                scheduledActivityDate.setDate(date);
                                scheduledActivityDate.setReminderType(reminderType);
                                scheduledActivityDate.setReminderMessage(reminderMessage);
                            }
                        }
                    }else{
                        ScheduledActivityDate newDate = new ScheduledActivityDate(-UIHelper.getRandomNumber(), null, date, reminderType, reminderMessage);
                        dates.add(newDate);
                    }
                    updateDatesAdapter();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                validator.validate();
                break;
            default:
                break;
        }

        return true;
    }

    private void SaveActivity(){
        boolean isActive = isActiveCheck.isChecked();
        String title = titleText.getText().toString();
        String description = descriptionText.getText().toString();
        int syncAction = isNew ? SyncAction.Add.getCode() : SyncAction.Change.getCode();
        String externalId = existingActivity != null ? existingActivity.getExternalId() : null;
        int type = ((SpinnerKeyValuePair)typeSpinner.getSelectedItem()).getKey();

        ScheduledActivity sa = new ScheduledActivity(getCurrentKigaId(), syncAction, id, externalId, isActive, type, title, description, dates, kidsExternalIds);

        getDbHelper().addOrUpdateScheduledActivity(sa);
    }
}