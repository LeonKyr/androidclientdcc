package com.babify.AndroidClientDCC.Activities.Reports;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.babify.AndroidClientDCC.Activities.SliderBaseActivity;
import com.babify.AndroidClientDCC.Adapters.ReportNowKidsAdapter;
import com.babify.AndroidClientDCC.DataLayer.DatabaseHelper;
import com.babify.AndroidClientDCC.DomainModel.Action.SpinnerKeyValuePair;
import com.babify.AndroidClientDCC.DomainModel.EventsSearchParams;
import com.babify.AndroidClientDCC.DomainModel.EventsSearchResult;
import com.babify.AndroidClientDCC.DomainModel.Group;
import com.babify.AndroidClientDCC.DomainModel.Kid;
import com.babify.AndroidClientDCC.R;
import com.mbalychev.Shared.Domain.ActionType;

import java.util.ArrayList;
import java.util.List;

public class ReportNowFragment  extends Fragment {

    Spinner groupSpinner;
    Spinner typeSpinner;
    TextView report_now_total_kids;
    TextView report_now_total_notmarked_kids;
    TextView report_now_total_signedin_kids;
    TextView report_now_total_signedout_kids;
    ListView kidsList;
    DatabaseHelper dbHelper;
    String kigaId;
    ReportNowKidsAdapter kidsAdapter;

    int REPORT_NOW_SEARCH_TYPE = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report_now, container, false);

        dbHelper = new DatabaseHelper(getActivity());
        kigaId = ((SliderBaseActivity)getActivity()).getCurrentKigaId();

        groupSpinner = (Spinner)view.findViewById(R.id.report_now_group);
        typeSpinner = (Spinner)view.findViewById(R.id.report_now_type);
        kidsList = (ListView)view.findViewById(R.id.report_now_kids);

        report_now_total_kids = (TextView)view.findViewById(R.id.report_now_total_kids);
        report_now_total_notmarked_kids = (TextView)view.findViewById(R.id.report_now_total_notmarked_kids);
        report_now_total_signedin_kids = (TextView)view.findViewById(R.id.report_now_total_signedin_kids);
        report_now_total_signedout_kids = (TextView)view.findViewById(R.id.report_now_total_signedout_kids);

        List<Group> groups = dbHelper.readGroups(kigaId, false);
        groups.add(0, new Group(getString(R.string.allGroups)));
        ArrayAdapter<Group> groupsAdapter = new ArrayAdapter<Group>(getActivity(), android.R.layout.simple_spinner_item, groups);
        groupsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        groupSpinner.setAdapter(groupsAdapter);

        ArrayAdapter typeAdapter = new ArrayAdapter(this.getActivity(),
                android.R.layout.simple_spinner_item, new SpinnerKeyValuePair[] {
                new SpinnerKeyValuePair( 0, getResources().getString(R.string.report_now_type_0) ),
                new SpinnerKeyValuePair( 1, getResources().getString(R.string.report_now_type_1) ),
                new SpinnerKeyValuePair( 2, getResources().getString(R.string.report_now_type_2) ),
                new SpinnerKeyValuePair( 3, getResources().getString(R.string.report_now_type_3) ),
        });
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typeSpinner.setAdapter(typeAdapter);
        typeSpinner.setSelection(1);

        //kidsAdapter = new ReportNowKidsAdapter(getActivity(), R.layout.report_now_kid_row, new ArrayList<ReportNowResult>());
        //kidsList.setAdapter(kidsAdapter);

        groupSpinner.setOnItemSelectedListener(listener);
        typeSpinner.setOnItemSelectedListener(listener);

        //ShowKids(0, 1);

        return view;
    }

    AdapterView.OnItemSelectedListener listener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Group group = (Group)groupSpinner.getSelectedItem();
            SpinnerKeyValuePair typePair = (SpinnerKeyValuePair)typeSpinner.getSelectedItem();

            ShowKids(group.getId(), typePair.getKey());
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {}
    };

    private class GetDataTask extends AsyncTask<Request, String, ReportNowResults> {
        @Override
        protected ReportNowResults doInBackground(Request... reqs) {
            Request request = reqs[0];

            List<Kid> kids = dbHelper.readKids(kigaId, request.GroupId, false);

            List<ReportNowResult> reportResults = new ArrayList<ReportNowResult>();

            int totalCount = 0;
            int totalSignedIn = 0;
            int totalSignedOut = 0;
            int totalNotMarked= 0;

            if(kids.size() > 0)
            {
                List<Integer> kidIds = new ArrayList<Integer>();
                for(Kid kid : kids){
                    kidIds.add(kid.getId());
                }

                EventsSearchParams params = new EventsSearchParams();
                params.kidIds = kidIds;
                params.type = REPORT_NOW_SEARCH_TYPE;
                List<EventsSearchResult> results = dbHelper.readEventsSearchResults(params);

                for(Kid kid : kids){
                    List<EventsSearchResult> kidResults = new ArrayList<EventsSearchResult>();

                    for(EventsSearchResult res : results){
                        if(res.KidId == kid.getId())
                            kidResults.add(res);
                    }

                    ReportNowResult res = new ReportNowResult();
                    res.Kid = kid;

                    int type = 0;
                    totalCount ++;

                    if(kidResults.size() == 0){
                        type = 3;
                        totalNotMarked ++;
                    }
                    else if ((kidResults.size() > 0) && (kidResults.get(0).Type.equals(ActionType.Signin))){
                        type = 1;
                        totalSignedIn ++;
                    }
                    else if ((kidResults.size() > 0) && (kidResults.get(0).Type.equals(ActionType.Signout))){
                        type = 2;
                        totalSignedOut ++;
                    }

                    res.Type = type;

                    if(request.Type == 0 || request.Type == type)
                        reportResults.add(res);
                }
            }

            ReportNowResults res = new ReportNowResults();
            res.Results = reportResults;
            res.TotalCount = totalCount;
            res.TotalNotMarked = totalNotMarked;
            res.TotalSignedIn = totalSignedIn;
            res.TotalSignedOut = totalSignedOut;

            return res;
        }

        @Override
        protected void onPostExecute(ReportNowResults r) {
            report_now_total_kids.setText(String.valueOf(r.TotalCount));
            report_now_total_notmarked_kids.setText(String.valueOf(r.TotalNotMarked));
            report_now_total_signedin_kids.setText(String.valueOf(r.TotalSignedIn));
            report_now_total_signedout_kids.setText(String.valueOf(r.TotalSignedOut));

            //kidsAdapter.clear();
            //kidsAdapter.addAll(r.Results);
            //kidsAdapter.notifyDataSetChanged();
            kidsAdapter = new ReportNowKidsAdapter(getActivity(), R.layout.report_now_kid_row, r.Results);
            kidsList.setAdapter(kidsAdapter);

        }
    };

    private void ShowKids(final long groupId, final int filterType){
        Request req = new Request();
        req.GroupId = groupId;
        req.Type = filterType;

        new GetDataTask().execute(req);
    }

    private class Request{
        public long GroupId;
        public int Type;
    }

    private class ReportNowResults{
        public List<ReportNowResult> Results;
        public int TotalCount;
        public int TotalSignedIn;
        public int TotalSignedOut;
        public int TotalNotMarked;
    }

    public class ReportNowResult{
        public Kid Kid;
        public int Type;
    }
}
