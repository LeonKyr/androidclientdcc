package com.babify.AndroidClientDCC.Activities.EventFragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import com.babify.AndroidClientDCC.DomainModel.Action.*;
import com.babify.AndroidClientDCC.R;
import com.mbalychev.Shared.Domain.ActionType;

public class MealFragment extends Fragment implements IActionFactory{

    Spinner typeSpinner;
    Spinner qualitySpinner;
    EditText messageEditText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_meal, container, false);

        typeSpinner = (Spinner)view.findViewById(R.id.action_meal_type);
        qualitySpinner = (Spinner)view.findViewById(R.id.action_meal_quality);
        messageEditText = (EditText)view.findViewById(R.id.action_meal_message);

        ArrayAdapter typeAdapter = new ArrayAdapter(this.getActivity(),
                android.R.layout.simple_spinner_item, new SpinnerKeyValuePair[] {
                new SpinnerKeyValuePair( 1, getResources().getString(R.string.mealType1) ),
                new SpinnerKeyValuePair( 2, getResources().getString(R.string.mealType2) ),
                new SpinnerKeyValuePair( 3, getResources().getString(R.string.mealType3) ),
                new SpinnerKeyValuePair( 4, getResources().getString(R.string.mealType4) ),
                new SpinnerKeyValuePair( 5, getResources().getString(R.string.mealType5) ),
                new SpinnerKeyValuePair( 6, getResources().getString(R.string.mealType6) ),
        });

        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typeSpinner.setAdapter(typeAdapter);

        ArrayAdapter qualityAdapter = new ArrayAdapter(this.getActivity(),
                android.R.layout.simple_spinner_item, new SpinnerKeyValuePair[] {
                new SpinnerKeyValuePair( 1, getResources().getString(R.string.mealQuality1) ),
                new SpinnerKeyValuePair( 2, getResources().getString(R.string.mealQuality2) ),
                new SpinnerKeyValuePair( 3, getResources().getString(R.string.mealQuality3) ),
                new SpinnerKeyValuePair( 4, getResources().getString(R.string.mealQuality4) ),
                new SpinnerKeyValuePair( 5, getResources().getString(R.string.mealQuality5) ),
        });

        qualityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        qualitySpinner.setAdapter(qualityAdapter);

        Reset();

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        return view;
    }

    @Override
    public Action GetAction() {
        SpinnerKeyValuePair type = (SpinnerKeyValuePair)typeSpinner.getSelectedItem();
        SpinnerKeyValuePair quality = (SpinnerKeyValuePair)qualitySpinner.getSelectedItem();

        return new MealAction(type.getKey(), quality.getKey(), messageEditText.getText().toString());
    }

    @Override
    public String GetActionType() {
        return ActionType.Meal;
    }

    @Override
    public void Reset() {
        typeSpinner.setSelection(0);
        qualitySpinner.setSelection(0);

        messageEditText.setText(null);
    }

    @Override
    public boolean isValid() {
        return true;
    }
}