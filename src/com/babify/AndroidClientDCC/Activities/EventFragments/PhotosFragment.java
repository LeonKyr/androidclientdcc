package com.babify.AndroidClientDCC.Activities.EventFragments;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import com.babify.AndroidClientDCC.Adapters.ImageAdapter;
import com.babify.AndroidClientDCC.DomainModel.Action.Action;
import com.mbalychev.Shared.Domain.ActionType;
import com.babify.AndroidClientDCC.DomainModel.Action.IActionFactory;
import com.babify.AndroidClientDCC.DomainModel.Action.ImageAction;
import com.babify.AndroidClientDCC.R;
import com.mbalychev.Shared.Activities.ImagePickerActivity;

import java.util.ArrayList;

public class PhotosFragment extends Fragment implements IActionFactory {

    private ArrayList<String> imagePaths = new ArrayList<String>();
    private ImageAdapter adapter;
    private GridView gridView;
    private LinearLayout addImageLayout;
    private EditText messageEditText;

    private final int PICK_BITMAP = 54123;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photos, container, false);

        gridView = (GridView)view.findViewById(R.id.images_gridview);
        addImageLayout = (LinearLayout)view.findViewById(R.id.add_image_layout);

        addImageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ImagePickerActivity.class);
                intent.putExtra("returnOnlyPath", true);
                PhotosFragment.this.startActivityForResult(intent, PICK_BITMAP);
                //Toast.makeText(getActivity(), "Starting ImagePickerActivity", Toast.LENGTH_LONG).show();
            }
        });

        resetAdapter();

        messageEditText = (EditText)view.findViewById(R.id.action_image_message);

        return view;
    }

    @Override
    public Action GetAction() {
        return new ImageAction(imagePaths, messageEditText.getText().toString());
    }

    @Override
    public String GetActionType() {
        return ActionType.Photos;
    }

    @Override
    public void Reset() {
        messageEditText.setText(null);
        imagePaths = new ArrayList<String>();
        resetAdapter();
    }

    @Override
    public boolean isValid() {
        return imagePaths.size() > 0;
    }

    private void resetAdapter(){
        adapter = new ImageAdapter(getActivity(), imagePaths, 200);
        gridView.setAdapter(adapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Toast.makeText(getActivity(), "In Fragment OnActivityResult. ResultCode:"+resultCode, Toast.LENGTH_LONG).show();

        if (resultCode == Activity.RESULT_OK && requestCode == PICK_BITMAP){
            String path = data.getStringExtra("path");
            if(path != null)
            {
                imagePaths.add(path);
                resetAdapter();
            }
        }
    }
}