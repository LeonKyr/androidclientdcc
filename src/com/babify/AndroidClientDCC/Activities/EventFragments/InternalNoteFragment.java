package com.babify.AndroidClientDCC.Activities.EventFragments;

import com.mbalychev.Shared.Domain.ActionType;

public class InternalNoteFragment extends MessageFragment{
    @Override
    public String GetActionType() {
        return ActionType.InternalNote;
    }
}