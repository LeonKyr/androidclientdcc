package com.babify.AndroidClientDCC.Activities.EventFragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import com.babify.AndroidClientDCC.DomainModel.Action.*;
import com.babify.AndroidClientDCC.R;
import com.mbalychev.Shared.Domain.ActionType;

public class AbsenceFragment extends Fragment implements IActionFactory {

    Spinner reasonSpinner;
    EditText messageEditText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_absence, container, false);

        reasonSpinner = (Spinner)view.findViewById(R.id.action_absence_reason);
        messageEditText = (EditText)view.findViewById(R.id.action_absence_message);

        ArrayAdapter reasonAdapter = new ArrayAdapter(this.getActivity(),
                android.R.layout.simple_spinner_item, new SpinnerKeyValuePair[] {
                new SpinnerKeyValuePair( 1, getResources().getString(R.string.absenceReason1) ),
                new SpinnerKeyValuePair( 2, getResources().getString(R.string.absenceReason2) ),
                new SpinnerKeyValuePair( 3, getResources().getString(R.string.absenceReason3) ),
        });

        reasonAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        reasonSpinner.setAdapter(reasonAdapter);

        Reset();

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        return view;
    }

    @Override
    public Action GetAction() {
        SpinnerKeyValuePair reason = (SpinnerKeyValuePair)reasonSpinner.getSelectedItem();

        return new AbsenceAction(reason.getKey(), messageEditText.getText().toString());
    }

    @Override
    public String GetActionType() {
        return ActionType.Absence;
    }

    @Override
    public void Reset() {
        reasonSpinner.setSelection(0);

        messageEditText.setText(null);
    }

    @Override
    public boolean isValid() {
        return true;
    }
}
