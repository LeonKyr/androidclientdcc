package com.babify.AndroidClientDCC.Activities.EventFragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import com.babify.AndroidClientDCC.DomainModel.Action.*;
import com.babify.AndroidClientDCC.R;
import com.mbalychev.Shared.Domain.ActionType;

public class SleepFragment extends Fragment implements IActionFactory{

    Spinner durationSpinner;
    Spinner qualitySpinner;
    EditText messageEditText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sleep, container, false);

        durationSpinner = (Spinner)view.findViewById(R.id.action_sleep_duration);
        qualitySpinner = (Spinner)view.findViewById(R.id.action_sleep_quality);
        messageEditText = (EditText)view.findViewById(R.id.action_sleep_message);

        ArrayAdapter durationAdapter = new ArrayAdapter(this.getActivity(),
                android.R.layout.simple_spinner_item, new SpinnerKeyValuePair[] {
                new SpinnerKeyValuePair( 1, getResources().getString(R.string.sleepDuration1) ),
                new SpinnerKeyValuePair( 2, getResources().getString(R.string.sleepDuration2) ),
                new SpinnerKeyValuePair( 3, getResources().getString(R.string.sleepDuration3) ),
                new SpinnerKeyValuePair( 4, getResources().getString(R.string.sleepDuration4) ),
                new SpinnerKeyValuePair( 5, getResources().getString(R.string.sleepDuration5) ),
                new SpinnerKeyValuePair( 6, getResources().getString(R.string.sleepDuration6) ),
                new SpinnerKeyValuePair( 7, getResources().getString(R.string.sleepDuration7) )
        });

        durationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        durationSpinner.setAdapter(durationAdapter);

        ArrayAdapter qualityAdapter = new ArrayAdapter(this.getActivity(),
                android.R.layout.simple_spinner_item, new SpinnerKeyValuePair[] {
                new SpinnerKeyValuePair( 1, getResources().getString(R.string.sleepQuality1) ),
                new SpinnerKeyValuePair( 2, getResources().getString(R.string.sleepQuality2) ),
                new SpinnerKeyValuePair( 3, getResources().getString(R.string.sleepQuality3) ),
                new SpinnerKeyValuePair( 4, getResources().getString(R.string.sleepQuality4) ),
                new SpinnerKeyValuePair( 5, getResources().getString(R.string.sleepQuality5) ),
        });

        qualityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        qualitySpinner.setAdapter(qualityAdapter);

        Reset();

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        return view;
    }

    @Override
    public Action GetAction() {
        SpinnerKeyValuePair duration = (SpinnerKeyValuePair)durationSpinner.getSelectedItem();
        SpinnerKeyValuePair quality = (SpinnerKeyValuePair)qualitySpinner.getSelectedItem();

        return new SleepAction(duration.getKey(), quality.getKey(), messageEditText.getText().toString());
    }

    @Override
    public String GetActionType() {
        return ActionType.Sleep;
    }

    @Override
    public void Reset() {
        durationSpinner.setSelection(2);
        qualitySpinner.setSelection(0);

        messageEditText.setText(null);
    }

    @Override
    public boolean isValid() {
        return true;
    }
}