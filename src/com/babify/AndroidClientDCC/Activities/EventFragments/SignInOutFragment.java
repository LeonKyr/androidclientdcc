package com.babify.AndroidClientDCC.Activities.EventFragments;

import android.app.*;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.babify.AndroidClientDCC.DomainModel.Action.Action;
import com.babify.AndroidClientDCC.DomainModel.Action.IActionFactory;
import com.babify.AndroidClientDCC.DomainModel.Action.SignInOutAction;
import com.babify.AndroidClientDCC.Helpers.DCCHelper;
import com.babify.AndroidClientDCC.R;
import com.mbalychev.Shared.Helpers.UIHelper;

import java.util.Calendar;
import java.util.Date;

public class SignInOutFragment extends Fragment implements IActionFactory {
    private EditText timeText;
    private CheckBox useCurrentTimeCheck;
    private long selectedTime;
    private String actionType;

    public SignInOutFragment(String type){
        actionType = type;
    }

    public SignInOutFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_signinout, container, false);

        timeText = (EditText)view.findViewById(R.id.signinout_time);

        selectedTime = Calendar.getInstance().getTimeInMillis();
        SetTimeText(selectedTime);

        timeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTimePickerDialog timePickerDialog = new FragmentTimePickerDialog();
                timePickerDialog.show(getFragmentManager(), "time");
                useCurrentTimeCheck.setChecked(false);
            }
        });

        useCurrentTimeCheck = (CheckBox)view.findViewById(R.id.signinout_usecurrenttime);
        useCurrentTimeCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                timeText.setEnabled(!isChecked);
            }
        });

        return view;
    }

    @Override
    public Action GetAction() {

        long time;
        if(useCurrentTimeCheck.isChecked())
        {
            Calendar c = Calendar.getInstance();
            time = c.getTimeInMillis();
        }else{
           time = selectedTime;
        }
        return new SignInOutAction(time);
    }

    @Override
    public String GetActionType() {
        return actionType;
    }

    @Override
    public void Reset() {
        useCurrentTimeCheck.setChecked(true);
        selectedTime = Calendar.getInstance().getTimeInMillis();
        SetTimeText(selectedTime);
    }

    @Override
    public boolean isValid() {
        if(!useCurrentTimeCheck.isChecked() && selectedTime <= 0){
            //DCCHelper.showToast(getActivity(), "", Toast.LENGTH_LONG, DCCHelper.TDSuccess);
            return false;
        }

        return true;
    }

    private class FragmentTimePickerDialog extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            Calendar c = Calendar.getInstance();

            int hours = c.get(Calendar.HOUR_OF_DAY);
            int minutes = c.get(Calendar.MINUTE);

            return new TimePickerDialog(getActivity(), myTimePickerCallBack, hours, minutes, false);
        }
    }

    TimePickerDialog.OnTimeSetListener myTimePickerCallBack = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            Calendar c = Calendar.getInstance();
            Date date = UIHelper.getDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), hourOfDay, minute);

            selectedTime = date.getTime();
            SetTimeText(selectedTime);
        }};

    private void SetTimeText(long time){
        timeText.setText(UIHelper.getDateString(time, DCCHelper.TIME_FORMAT));
    }
}