package com.babify.AndroidClientDCC.Activities.EventFragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import com.babify.AndroidClientDCC.DomainModel.Action.IActionFactory;
import com.babify.AndroidClientDCC.DomainModel.Action.Action;
import com.mbalychev.Shared.Domain.ActionType;
import com.babify.AndroidClientDCC.DomainModel.Action.MessageAction;
import com.babify.AndroidClientDCC.R;

public class MessageFragment extends Fragment implements IActionFactory{

    EditText subjectEditText;
    EditText messageEditText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message, container, false);

        subjectEditText = (EditText)view.findViewById(R.id.action_message_subject);
        messageEditText = (EditText)view.findViewById(R.id.action_message_message);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        return view;
    }

    @Override
    public Action GetAction() {
        return new MessageAction(subjectEditText.getText().toString(), messageEditText.getText().toString());
    }

    @Override
    public String GetActionType() {
        return ActionType.Note;
    }

    @Override
    public void Reset() {
        subjectEditText.setText(null);
        messageEditText.setText(null);
    }

    @Override
    public boolean isValid() {
        subjectEditText.setError(null);
        messageEditText.setError(null);


        String subject = subjectEditText.getText().toString();
        String message = messageEditText.getText().toString();

        if (subject.isEmpty() || message.isEmpty()){
            if (subject.isEmpty())
                subjectEditText.setError("Required");
            if(message.isEmpty())
                messageEditText.setError("Required");

           return false;
        }
        return true;
    }
}