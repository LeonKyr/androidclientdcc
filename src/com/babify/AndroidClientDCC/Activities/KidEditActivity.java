package com.babify.AndroidClientDCC.Activities;

import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.*;
import android.widget.*;
import com.babify.AndroidClientDCC.Adapters.KidContactsAdapter;
import com.babify.AndroidClientDCC.BabifyActivity;
import com.babify.AndroidClientDCC.BabifyApplication;
import com.babify.AndroidClientDCC.DomainModel.Group;
import com.babify.AndroidClientDCC.DomainModel.Kid;
import com.babify.AndroidClientDCC.DomainModel.KidContact;
import com.babify.AndroidClientDCC.DomainModel.SyncAction;
import com.babify.AndroidClientDCC.Helpers.DCCHelper;
import com.babify.AndroidClientDCC.R;
import com.mbalychev.Shared.Activities.ImagePickerActivity;
import com.mbalychev.Shared.Helpers.ImageHelper;
import com.mbalychev.Shared.Helpers.UIHelper;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Required;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class KidEditActivity extends SliderBaseActivity  implements Validator.ValidationListener {
    ListView contactsList;

    @Required(order = 1)
    EditText nameText;

    @Required(order = 2)
    EditText birthdayText;

    Spinner groupSpinner;

    ImageView pictureImageView;
    LinearLayout kidUniqueCodeArea;
    TextView kidUniqueCode;

    boolean isNew;
    long id;
    KidContactsAdapter kidContactsAdapter;
    List<KidContact> kidContacts;
    KidContact selectedContact;

    Bitmap bitmap = null;
    long birthdayInMilis = 0;

    Validator validator;

    int ADD_CONTACT_REQUEST_CODE = 1;
    int UPDATE_CONTACT_REQUEST_CODE = 2;
    int PICK_BITMAP = 3;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        super.onCreate(savedInstanceState, R.layout.kid_edit, 0, R.string.updateKid);
        //setContentView(R.layout.kid_edit);

        contactsList = (ListView) findViewById(R.id.kid_contacts_list);
        LayoutInflater inflater = LayoutInflater.from(this);
        View top = inflater.inflate(R.layout.kid_edit_fields, null);
        contactsList.addHeaderView(top);

        nameText = (EditText)findViewById(R.id.kid_name);
        birthdayText = (EditText)findViewById(R.id.kid_birthday);
        pictureImageView = (ImageView)findViewById(R.id.kid_picture);

        kidUniqueCodeArea = (LinearLayout)findViewById(R.id.kid_unique_code_area);
        kidUniqueCode = (TextView)findViewById(R.id.kid_unique_code);

        birthdayText.setKeyListener(null);

        groupSpinner = (Spinner) findViewById(R.id.type);
        List<Group> groups = getDbHelper().readGroups(getCurrentKigaId(), false);

        if(groups == null || groups.size() == 0){
            UIHelper.showToast(getApplicationContext(), getString(R.string.noGroupsRegistered), Toast.LENGTH_LONG, UIHelper.TDError);
            finish();
        }

        ArrayAdapter<Group> adapter = new ArrayAdapter<Group>(this, android.R.layout.simple_spinner_item, groups);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        groupSpinner.setAdapter(adapter);

        Intent intent = getIntent();
        id = intent.getIntExtra("Id",0);

        isNew = id == 0;

        ActionBar bar = getActionBar();

        kidContacts = new ArrayList<KidContact>();

        kidUniqueCodeArea.setVisibility(View.GONE);

        if(!isNew){
            Kid kid = getDbHelper().getKid(id, null);
            nameText.setText(kid.getName());

            for (Group group : groups) {
                if (group.getId() == kid.getGroupId()) {
                    {
                        int position = groups.indexOf(group);
                        groupSpinner.setSelection(position);
                        break;
                    }
                }
            }

            if(kid.getImage() != null){
                bitmap = ImageHelper.getBitmap(kid.getImage().getData());
                pictureImageView.setImageBitmap(bitmap);
            }

            birthdayInMilis = kid.getBirthday();
            birthdayText.setText(UIHelper.getDateString(birthdayInMilis, DCCHelper.BIRTHDAY_FORMAT));
            kidContacts = getDbHelper().readKidContacts(id);

            bar.setSubtitle(getString(R.string.updateKid));

            String uniqueId = kid.getUniqueId();

            if(uniqueId != null){
                kidUniqueCodeArea.setVisibility(View.VISIBLE);
                kidUniqueCode.setText(uniqueId);
            }
        }
        else{
            bar.setSubtitle(getString(R.string.newKid));
        }

        kidContactsAdapter = new KidContactsAdapter(this, R.layout.kidcontact_row, kidContacts);
        contactsList.setAdapter(kidContactsAdapter);

        contactsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(KidEditActivity.this, KidContactEditActivity.class);

                selectedContact = kidContacts.get(position - 1); // TODO: check!

                int contactId = selectedContact.getId();

                if (contactId > 0)
                    intent.putExtra("id", contactId);
                else {
                    intent.putExtra("name", selectedContact.getName());
                    intent.putExtra("relation", selectedContact.getRelation());
                    intent.putExtra("phone", selectedContact.getPhone());
                    intent.putExtra("email", selectedContact.getEmail());
                }
                startActivityForResult(intent, UPDATE_CONTACT_REQUEST_CODE);
            }
        });

        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_kid, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case R.id.action_save_kid:
                validator.validate();
                break;
            case R.id.action_add_kid_contact:
                addContact(null);
                break;
            default:
                break;
        }

        return true;
    }

    public void addContact(View v){
        Intent intent = new Intent(this, KidContactEditActivity.class);
        startActivityForResult(intent, ADD_CONTACT_REQUEST_CODE);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == ADD_CONTACT_REQUEST_CODE || requestCode == UPDATE_CONTACT_REQUEST_CODE) {
                String name = data.getStringExtra("name");
                String phone = data.getStringExtra("phone");
                String email = data.getStringExtra("email");
                int relation = data.getIntExtra("relation",0);
                int contactId = data.getIntExtra("id",0);

                if (requestCode == ADD_CONTACT_REQUEST_CODE){
                    KidContact contact = new KidContact(name, phone, email, relation);
                    contact.setSyncAction(SyncAction.Add.getCode());
                    kidContacts.add(contact);
                }else{
                    selectedContact.setName(name);
                    selectedContact.setRelation(relation);
                    selectedContact.setPhone(phone);
                    selectedContact.setEmail(email);

                    if(selectedContact.getSyncAction() != SyncAction.Add.getCode())
                        selectedContact.setSyncAction(SyncAction.Change.getCode());
                }

                kidContactsAdapter.swapItems(kidContacts);
            }

            if (requestCode == PICK_BITMAP){
                byte[] bytes = data.getByteArrayExtra("bitmap_bytes");
                if(bytes != null)
                {
                    bitmap = ImageHelper.getBitmap(bytes);
                    pictureImageView.setImageBitmap(bitmap);
                }
            }
        }
    };

    public void makePicture(View v){
        Intent intent = new Intent(KidEditActivity.this, ImagePickerActivity.class);
        startActivityForResult(intent, PICK_BITMAP);
    }


    public void onDatePick(View view) {
        if (view.getId() == R.id.kid_birthday) {
            showDialog(0);
        }
    }

    protected Dialog onCreateDialog(int id) {

        Calendar c = Calendar.getInstance();

        if(birthdayInMilis > 0){
            c.setTimeInMillis(birthdayInMilis);
        }

        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(this, myDatePickerCallBack, year, month, day);
    }

    DatePickerDialog.OnDateSetListener myDatePickerCallBack = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Date birthday = UIHelper.getDate(year, monthOfYear, dayOfMonth, 0, 0);

            birthdayInMilis = birthday.getTime();
            birthdayText.setText(UIHelper.getDateString(birthdayInMilis, DCCHelper.BIRTHDAY_FORMAT));
        }
    };

    /*public void onRemoveContactClick(View view) {
        dbHelper.deleteKidContact(view.getTag().toString());
    }*/

    @Override
    public void onValidationSucceeded() {
        Group group = (Group)groupSpinner.getSelectedItem();
        String name = nameText.getText().toString().trim();

        byte[] imageBytes = null;
        if(bitmap != null)
            imageBytes = ImageHelper.getBytes(bitmap);


        long kidId = id;

        if(isNew){
            kidId = getDbHelper().addKid(getCurrentKigaId(), name, group.getId(), birthdayInMilis, imageBytes);
            UIHelper.showToast(getApplicationContext(), getString(R.string.kidWasAdded), Toast.LENGTH_SHORT, UIHelper.TDInfo);
        }else{
            getDbHelper().updateKid(id, name, group.getId(), birthdayInMilis, imageBytes);
            UIHelper.showToast(getApplicationContext(), getString(R.string.kidWasUpdated), Toast.LENGTH_SHORT, UIHelper.TDInfo);
        }

        if(kidId > 0){
            for(KidContact kidContact:kidContacts){
                if(kidContact.getSyncAction() == SyncAction.Add.getCode())
                    if(kidContact.getId() == 0)
                        getDbHelper().addKidContact(getCurrentKigaId (), (int)kidId,kidContact.getName(),kidContact.getPhone(), kidContact.getEmail(), kidContact.getRelation());
                    else
                        getDbHelper().updateKidContact(kidContact.getId(), kidContact.getName(), kidContact.getPhone(),kidContact.getEmail(),kidContact.getRelation());
                if(kidContact.getSyncAction() == SyncAction.Change.getCode())
                    getDbHelper().updateKidContact(kidContact.getId(), kidContact.getName(), kidContact.getPhone(),kidContact.getEmail(),kidContact.getRelation());
            }
        }

        ((BabifyApplication)getApplicationContext()).runSyncTask(getCurrentKigaId());
        finish();
    }

    @Override
    public void onValidationFailed(View failedView, Rule<?> failedRule) {
        DCCHelper.validationFailedRule(getApplicationContext(), failedView, failedRule);
    }
}