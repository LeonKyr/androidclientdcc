package com.babify.AndroidClientDCC.Activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import com.babify.AndroidClientDCC.Adapters.TeachersAdapter;
import com.babify.AndroidClientDCC.DomainModel.Media;
import com.babify.AndroidClientDCC.DomainModel.Teacher;
import com.babify.AndroidClientDCC.Helpers.DCCHelper;
import com.babify.AndroidClientDCC.R;
import com.mbalychev.Shared.Helpers.ImageHelper;

import java.util.List;

public class TeachersActivity extends SliderBaseActivity {
    GridView gridview;
    List<Teacher> teachers;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_teachers, R.layout.left_slider_menu_full,R.string.teachers);
        //setContentView(R.layout.activity_teachers);

        gridview = (GridView) findViewById(R.id.teachers_gridview);

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, android.view.View v, int position, long id) {
                Teacher teacher = teachers.get(position);

                if(teacher.getId() == DCCHelper.AddEntityId){
                    redirectToNew();
                }else{
                    getDbHelper().activateTeacher(getCurrentKigaId(), teacher.getId());

                    Intent intent = new Intent(TeachersActivity.this, TeacherEditActivity.class);
                    intent.putExtra("Id",teacher.getId());
                    startActivity(intent);
                }
                /*Teacher teacher = teachers.get(position);

                if(teacher.getId() == DCCHelper.AddEntityId){
                    redirectToNew();
                }else{
                    getDbHelper().activateTeacher(getCurrentKigaId(), teacher.getId());
                    updateAdapter();
                }*/
            }
        });

        /*gridview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Teacher teacher = teachers.get(position);

                if(teacher.getId() == DCCHelper.AddEntityId){
                    redirectToNew();
                }else{
                    getDbHelper().activateTeacher(getCurrentKigaId(), teacher.getId());

                    Intent intent = new Intent(TeachersActivity.this, TeacherEditActivity.class);
                    intent.putExtra("Id",teacher.getId());
                    startActivity(intent);
                }
                return false;
            }
        });*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_teachers, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case R.id.action_new_teacher:
                redirectToNew();
                break;
            default:
                break;
        }

        return true;
    }
    public void redirectToNew(){
        Intent intent = new Intent(TeachersActivity.this, TeacherEditActivity.class);
        startActivity(intent);
    }


    @Override
    protected void onResume() {
        super.onResume();

        updateAdapter();
    }

    private void updateAdapter(){
        teachers  = getDbHelper().readTeachers(getCurrentKigaId(), false);

        Teacher fakeTeacher = new Teacher(getString(R.string.newTeacher));
        fakeTeacher.setId(DCCHelper.AddEntityId);
        Bitmap bitmap = ((BitmapDrawable)getApplicationContext().getResources().getDrawable(R.drawable.pm_new)).getBitmap();
        Media image = new Media(ImageHelper.getBytes(bitmap),null);
        fakeTeacher.setImage(image);
        teachers.add(0, fakeTeacher);

        gridview.setAdapter(new TeachersAdapter(getApplicationContext(), R.layout.teacher_row, teachers, false));
    }
}