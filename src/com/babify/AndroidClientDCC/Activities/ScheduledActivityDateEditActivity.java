package com.babify.AndroidClientDCC.Activities;

import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import com.babify.AndroidClientDCC.BabifyActivity;
import com.babify.AndroidClientDCC.DomainModel.Action.SpinnerKeyValuePair;
import com.babify.AndroidClientDCC.Helpers.DCCHelper;
import com.babify.AndroidClientDCC.R;
import com.mbalychev.Shared.Helpers.UIHelper;

import java.util.Calendar;
import java.util.Date;

public class ScheduledActivityDateEditActivity extends BabifyActivity {
    private Spinner reminderTypeSpinner;
    private EditText reminderText;
    private EditText dateText;

    private long id;
    private boolean isNew;
    private long date;
    private String reminderMessage;
    private int reminderType;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UIHelper.showAsPopup(this);

        setContentView(R.layout.activity_scheduleddateedit);

        dateText = (EditText)findViewById(com.babify.AndroidClientDCC.R.id.scheduledactivitydate_date);
        reminderText = (EditText)findViewById(com.babify.AndroidClientDCC.R.id.scheduledactivitydate_remindermessage);
        reminderTypeSpinner = (Spinner)findViewById(com.babify.AndroidClientDCC.R.id.scheduledactivitydate_remindertype);

        dateText.setKeyListener(null);

        ArrayAdapter typeAdapter = new ArrayAdapter(this,
                android.R.layout.simple_spinner_item, new SpinnerKeyValuePair[] {
                new SpinnerKeyValuePair( 0, getResources().getString(com.babify.AndroidClientDCC.R.string.scheduledactivitydate_type_0) ),
                new SpinnerKeyValuePair( 1, getResources().getString(com.babify.AndroidClientDCC.R.string.scheduledactivitydate_type_1) ),
                new SpinnerKeyValuePair( 2, getResources().getString(com.babify.AndroidClientDCC.R.string.scheduledactivitydate_type_2) ),
                new SpinnerKeyValuePair( 3, getResources().getString(com.babify.AndroidClientDCC.R.string.scheduledactivitydate_type_3) ),
                new SpinnerKeyValuePair( 5, getResources().getString(com.babify.AndroidClientDCC.R.string.scheduledactivitydate_type_5) ),
                new SpinnerKeyValuePair( 6, getResources().getString(com.babify.AndroidClientDCC.R.string.scheduledactivitydate_type_6) ),
        });
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        reminderTypeSpinner.setAdapter(typeAdapter);


        Intent intent = getIntent();
        id = intent.getLongExtra("Id",0);
        date = intent.getLongExtra("date",System.currentTimeMillis());
        reminderMessage = intent.getStringExtra("reminderMessage");
        reminderType = intent.getIntExtra("reminderType",2); // 12am of the previous day

        isNew = id == 0;

        ActionBar bar = getActionBar();
        if(!isNew){
            bar.setSubtitle(getString(com.babify.AndroidClientDCC.R.string.modifyDate));
        }else{
            bar.setSubtitle(getString(com.babify.AndroidClientDCC.R.string.newDate));
        }
        reminderText.setText(reminderMessage);
        reminderTypeSpinner.setSelection(reminderType);
        dateText.setText(UIHelper.getDateString(date, DCCHelper.SCHEDULED_ACTIVITY_FORMAT));
    }

    public void onDatePick(View view) {
        if (view.getId() == com.babify.AndroidClientDCC.R.id.scheduledactivitydate_date) {
            showDialog(0);
        }
    }

    protected Dialog onCreateDialog(int id) {

        Calendar c = Calendar.getInstance();

        if(date > 0){
            c.setTimeInMillis(date);
        }

        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(this, myDatePickerCallBack, year, month, day);
    }

    DatePickerDialog.OnDateSetListener myDatePickerCallBack = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Date dd = UIHelper.getDate(year, monthOfYear, dayOfMonth, 0, 0);

            date = dd.getTime();
            dateText.setText(UIHelper.getDateString(date, DCCHelper.SCHEDULED_ACTIVITY_FORMAT));
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                SpinnerKeyValuePair typePair = (SpinnerKeyValuePair)reminderTypeSpinner.getSelectedItem();

                Intent data = new Intent();
                data.putExtra("Id",id);
                data.putExtra("date", date);
                data.putExtra("reminderType", typePair.getKey());
                data.putExtra("reminderMessage", reminderText.getText().toString());
                setResult(RESULT_OK, data);
                finish();
                break;
            default:
                break;
        }

        return true;
    }
}