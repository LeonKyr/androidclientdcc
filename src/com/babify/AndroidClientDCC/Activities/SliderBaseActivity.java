package com.babify.AndroidClientDCC.Activities;

import android.app.ActionBar;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.*;
//import com.agimind.widget.SlideHolder;
import com.babify.AndroidClientDCC.BabifyActivity;
import com.babify.AndroidClientDCC.Helpers.DCCHelper;
import com.babify.AndroidClientDCC.R;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.mbalychev.Shared.Helpers.UIHelper;

//https://github.com/jfeinstein10/SlidingMenu - another one

public abstract class SliderBaseActivity extends BabifyActivity {
    SlidingMenu menu;
    SharedPreferences prefs;

    public void onCreate(Bundle savedInstanceState, int mainLayoutId, int menuLayoutId, int titleId) {
        super.onCreate(savedInstanceState);

        setContentView(mainLayoutId);

        ActionBar bar = getActionBar();
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setSubtitle(getString(titleId));

        if(menuLayoutId != 0){
            menu = new SlidingMenu(this);
            menu.setMode(SlidingMenu.LEFT);
            menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
            menu.setShadowWidthRes(R.dimen.shadow_width);
            menu.setShadowDrawable(R.drawable.shadow);
            menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
            menu.setBehindWidth(UIHelper.dpToPx(getApplicationContext(), 300));
            menu.setFadeDegree(0.35f);
            menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
            menu.setMenu(menuLayoutId);
        }
        prefs = getSharedPreferences(DCCHelper.sharedPrefsName, MODE_PRIVATE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        int[] tutorialImageIds = DCCHelper.getTutorialImages(this.getClass().getSimpleName());

        if(tutorialImageIds != null && tutorialImageIds.length > 0){
            String key = "tutorial_for_"+this.getClass().getName()+"_shown";
            Boolean tutorialShown = prefs.getBoolean(key, false);
            if(!tutorialShown){
                prefs.edit().putBoolean(key, true).commit();

                showTutorial(tutorialImageIds);
            }
        }
    }

    private void showTutorial(int[] imageIds){
        Intent intent = new Intent(this, TutorialActivity.class);
        intent.putExtra(DCCHelper.tutorialImages, imageIds);
        intent.putExtra("orientation", "land");
        intent.putExtra(DCCHelper.redirectToExtra, "close");
        startActivity(intent);
    }

    public void toogleMenu(){
        if(menu != null){
            menu.toggle(true);
        }
    }

    @Override
    public void onBackPressed() {
        if (menu != null && menu.isMenuShowing()) {
            menu.showContent();
        } else {
            super.onBackPressed();
        }
    }

    public void onSliderItemClick(View v){
        int viewId = v.getId();

        Intent intent = null;
        switch (viewId)
        {
            case R.id.left_slider_groups:
                intent = new Intent(SliderBaseActivity.this, GroupsActivity.class);
                break;
            case R.id.left_slider_add_group:
                intent = new Intent(SliderBaseActivity.this, GroupEditActivity.class);
                break;
            case R.id.left_slider_teachers:
                intent = new Intent(SliderBaseActivity.this, TeachersActivity.class);
                break;
            case R.id.left_slider_add_teacher:
                intent = new Intent(SliderBaseActivity.this, TeacherEditActivity.class);
                break;
            case R.id.left_slider_kids:
                intent = new Intent(SliderBaseActivity.this, KidsActivity.class);
                break;
            case R.id.left_slider_add_kid:
                intent = new Intent(SliderBaseActivity.this, KidEditActivity.class);
                break;
            case R.id.left_slider_main:
                intent = new Intent(SliderBaseActivity.this, MainActivity.class);
                break;
            case R.id.left_slider_add_event:
                intent = new Intent(SliderBaseActivity.this, NewEventActivity.class);
                break;
            case R.id.left_slider_feedback:
                intent = new Intent(SliderBaseActivity.this, FeedbackActivity.class);
                break;
            case R.id.left_slider_settings:
                intent = new Intent(SliderBaseActivity.this, SettingsActivity.class);
                break;
            case R.id.left_slider_reports:
                intent = new Intent(SliderBaseActivity.this, ReportsActivity.class);
                break;
            case R.id.left_slider_kiga_info:
                intent = new Intent(SliderBaseActivity.this, KindergartenEditActivity.class);
                break;

            /*case R.id.left_slider_tutorial:
                intent = new Intent(SliderBaseActivity.this, TutorialActivity.class);
                intent.putExtra(DCCHelper.tutorialImages, DCCHelper.tutorialAfterLoginImagesArray);
                intent.putExtra(DCCHelper.redirectToExtra, "close");
                break;
            */
        }

        if(intent != null)
            startActivity(intent);

        //finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        int[] tutorialImageIds = DCCHelper.getTutorialImages(this.getClass().getSimpleName());
        if(tutorialImageIds != null && tutorialImageIds.length > 0)
        {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.menu_base, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_tutorial:
                int[] tutorialImageIds = DCCHelper.getTutorialImages(this.getClass().getSimpleName());
                if(tutorialImageIds != null && tutorialImageIds.length > 0){
                    showTutorial(tutorialImageIds);
                }
                break;
            case android.R.id.home:
                toogleMenu();
                break;
        }

        return true;
    }

    public void onGenericClick(View v){
    }
}