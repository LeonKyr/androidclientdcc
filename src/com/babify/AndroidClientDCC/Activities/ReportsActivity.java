package com.babify.AndroidClientDCC.Activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.babify.AndroidClientDCC.Activities.Reports.ReportNowFragment;
import com.babify.AndroidClientDCC.Adapters.ReportTypesAdapter;
import com.babify.AndroidClientDCC.DomainModel.Report.ReportType;
import com.babify.AndroidClientDCC.R;

public class ReportsActivity extends SliderBaseActivity {
    Fragment currentFragment;
    ListView reportTypesListView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_reports, R.layout.left_slider_menu_full,R.string.reports);

        reportTypesListView = (ListView) findViewById(R.id.reports_types_listview);
        reportTypesListView.setAdapter(new ReportTypesAdapter(this, R.layout.report_type_row, ReportType.values));

        reportTypesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String type = ((ReportTypesAdapter.ReportTypeHolder)view.getTag()).type;

                if(type.equals(ReportType.Now))
                    SetFragment(new ReportNowFragment());
            }
        });
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                toogleMenu();
                break;
            default:
                break;
        }

        return true;
    }*/

    private void SetFragment(Fragment fragment){
        currentFragment = fragment;
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.report_details, fragment);
        ft.commit();
    }
}