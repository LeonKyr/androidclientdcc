package com.babify.AndroidClientDCC.Activities;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.babify.AndroidClientDCC.BabifyActivity;
import com.babify.AndroidClientDCC.BabifyApplication;
import com.babify.AndroidClientDCC.DomainModel.Group;
import com.babify.AndroidClientDCC.Helpers.DCCHelper;
import com.babify.AndroidClientDCC.R;
import com.mbalychev.Shared.Helpers.UIHelper;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Required;

public class GroupEditActivity extends SliderBaseActivity implements Validator.ValidationListener{
    @Required(order = 1)
    EditText nameEditText;

    ImageView colorImageView;

    boolean isNew;
    long id;

    int color;

    int PICK_COLOR_REQUEST_CODE = 1;
    static final int DEFAULT_COLOR = 0xFF00FFFF;//0x4FDBE2;

    Validator validator;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        super.onCreate(savedInstanceState, R.layout.group_edit, 0, R.string.updateGroup);
        //setContentView(R.layout.group_edit);

        nameEditText = (EditText)findViewById(R.id.group_name);
        colorImageView = (ImageView)findViewById(R.id.group_color);

        Intent intent = getIntent();
        id = intent.getLongExtra("Id",0);

        isNew = id == 0;

        ActionBar bar = getActionBar();
        if(!isNew){
            Group group = getDbHelper().getGroup(id, null);
            nameEditText.setText(group.getName());

            color = group.getColor();

            bar.setSubtitle(getString(R.string.updateGroup));
        }
        else{
            color = DEFAULT_COLOR;
            bar.setSubtitle(getString(R.string.newGroup));
        }

        colorImageView.setBackgroundColor(color);

        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_group, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case R.id.action_save_group:
                save();
                break;
            default:
                break;
        }

        return true;
    }

    public void save(){
        validator.validate();
    }

    public void selectColorClick(View view){
        Intent intent = new Intent(this, ColorPickerActivity.class);
        intent.putExtra("color",color);
        startActivityForResult(intent, PICK_COLOR_REQUEST_CODE);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == PICK_COLOR_REQUEST_CODE) {
                color = data.getIntExtra("color", DEFAULT_COLOR);
                colorImageView.setBackgroundColor(color);
            }
        }
    };

    @Override
    public void onValidationSucceeded() {
        String name = nameEditText.getText().toString().trim();

        if(isNew){
            id = getDbHelper().addGroup(getCurrentKigaId(), name, color);
            UIHelper.showToast(getApplicationContext(), getString(R.string.groupWasAdded), Toast.LENGTH_SHORT, UIHelper.TDInfo);
        }else{
            getDbHelper().updateGroup(id, name, color);
            UIHelper.showToast(getApplicationContext(), getString(R.string.groupWasUpdated), Toast.LENGTH_SHORT, UIHelper.TDInfo);
        }

        ((BabifyApplication)getApplicationContext()).runSyncTask(getCurrentKigaId());
        finish();
    }

    @Override
    public void onValidationFailed(View failedView, Rule<?> failedRule) {
        DCCHelper.validationFailedRule(getApplicationContext(), failedView, failedRule);
    }
}