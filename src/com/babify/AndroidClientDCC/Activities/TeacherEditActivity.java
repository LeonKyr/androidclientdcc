package com.babify.AndroidClientDCC.Activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.babify.AndroidClientDCC.BabifyActivity;
import com.babify.AndroidClientDCC.BabifyApplication;
import com.babify.AndroidClientDCC.DomainModel.Teacher;
import com.babify.AndroidClientDCC.Helpers.DCCHelper;
import com.babify.AndroidClientDCC.R;
import com.mbalychev.Shared.Activities.ImagePickerActivity;
import com.mbalychev.Shared.Helpers.ImageHelper;
import com.mbalychev.Shared.Helpers.UIHelper;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Required;

public class TeacherEditActivity extends SliderBaseActivity  implements Validator.ValidationListener{
    @Required(order = 1)
    EditText nameEditText;

    ImageView pictureImageView;
    boolean isNew;
    long id;

    Bitmap bitmap = null;
    Validator validator;

    private final int PICK_BITMAP = 1;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        super.onCreate(savedInstanceState, R.layout.teacher_edit, 0, R.string.updateTeacher);
        //setContentView(R.layout.teacher_edit);

        nameEditText = (EditText)findViewById(R.id.teacher_name);
        pictureImageView = (ImageView)findViewById(R.id.teacher_picture);

        Intent intent = getIntent();
        id = intent.getLongExtra("Id",0);

        isNew = id == 0;

        ActionBar bar = getActionBar();

        if(!isNew){
            Teacher teacher = getDbHelper().getTeacher(id, null);
            nameEditText.setText(teacher.getName());

            bar.setSubtitle(getString(R.string.updateTeacher));

            if(teacher.getImage() != null){
                bitmap = ImageHelper.getBitmap(teacher.getImage().getData());
                pictureImageView.setImageBitmap(bitmap);
            }
        }
        else{
            bar.setSubtitle(getString(R.string.newTeacher));
        }
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_teacher, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case R.id.action_save_teacher:
                validator.validate();
                break;
            default:
                break;
        }

        return true;
    }

    @Override
    public void onValidationSucceeded() {
        String name = nameEditText.getText().toString().trim();

        byte[] imageBytes = null;
        if(bitmap != null)
            imageBytes = ImageHelper.getBytes(bitmap);

        if(isNew){
            id = getDbHelper().addTeacher(getCurrentKigaId(), name, imageBytes);
            UIHelper.showToast(getApplicationContext(), getString(R.string.teacherWasAdded), Toast.LENGTH_SHORT, UIHelper.TDInfo);
        }else{
            getDbHelper().updateTeacher(id, name, imageBytes);
            UIHelper.showToast(getApplicationContext(), getString(R.string.teacherWasUpdated), Toast.LENGTH_SHORT, UIHelper.TDInfo);
        }

        ((BabifyApplication)getApplicationContext()).runSyncTask(getCurrentKigaId());

        finish();
    }

    @Override
    public void onValidationFailed(View failedView, Rule<?> failedRule) {
        DCCHelper.validationFailedRule(getApplicationContext(), failedView, failedRule);
    }

    public void makePicture(View v){
        Intent intent = new Intent(getApplicationContext(), ImagePickerActivity.class);
        startActivityForResult(intent, PICK_BITMAP);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK && requestCode == PICK_BITMAP){
            byte[] bytes = data.getByteArrayExtra("bitmap_bytes");
            if(bytes != null)
            {
                bitmap = ImageHelper.getBitmap(bytes);
                pictureImageView.setImageBitmap(bitmap);
            }
        }
    }
}