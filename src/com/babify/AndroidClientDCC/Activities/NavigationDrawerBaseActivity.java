package com.babify.AndroidClientDCC.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.ViewStub;
import com.babify.AndroidClientDCC.BabifyActivity;
import com.babify.AndroidClientDCC.R;

public abstract class NavigationDrawerBaseActivity extends BabifyActivity {
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private ViewStub mainView;
    private ViewStub sliderView;

    public void onCreate(Bundle savedInstanceState, int mainLayoutId, int menuLayoutId) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.navigation_drawer_base);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        drawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout, /* DrawerLayout object */
                R.drawable.ic_drawer, /* nav drawer icon to replace 'Up' caret */
                R.string.newKid, /* "open drawer" description */
                R.string.newKid /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                //getActionBar().setTitle(mTitle);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                //getActionBar().setTitle(mTitle);
            }
        };

        // Set the drawer toggle as the DrawerListener
        drawerLayout.setDrawerListener(drawerToggle);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        mainView = (ViewStub)findViewById(R.id.mainview_placeholder);
        mainView.setLayoutResource(mainLayoutId);
        mainView.inflate();

        sliderView = (ViewStub)findViewById(R.id.slider_placeholder);
        sliderView.setLayoutResource(menuLayoutId);
        sliderView.inflate();
    }

    public void onSliderItemClick(View v){
        int viewId = v.getId();

        Intent intent = null;
        switch (viewId)
        {
            case R.id.left_slider_groups:
                intent = new Intent(NavigationDrawerBaseActivity.this, GroupsActivity.class);
                break;
            case R.id.left_slider_add_group:
                intent = new Intent(NavigationDrawerBaseActivity.this, GroupEditActivity.class);
                break;
            case R.id.left_slider_teachers:
                intent = new Intent(NavigationDrawerBaseActivity.this, TeachersActivity.class);
                break;
            case R.id.left_slider_add_teacher:
                intent = new Intent(NavigationDrawerBaseActivity.this, TeacherEditActivity.class);
                break;
            case R.id.left_slider_kids:
                intent = new Intent(NavigationDrawerBaseActivity.this, KidsActivity.class);
                break;
            case R.id.left_slider_add_kid:
                intent = new Intent(NavigationDrawerBaseActivity.this, KidEditActivity.class);
                break;
            case R.id.left_slider_main:
                intent = new Intent(NavigationDrawerBaseActivity.this, MainActivity.class);
                break;
            case R.id.left_slider_add_event:
                intent = new Intent(NavigationDrawerBaseActivity.this, NewEventActivity.class);
                break;

            //case R.id.left_slider_settings:
            //    intent = new Intent(SliderBaseActivity.this, SettingsA .class);
            //    break;

        }

        if(intent != null)
            startActivity(intent);
    }
}