package com.babify.AndroidClientDCC.Activities;

import android.app.ActionBar;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import com.babify.AndroidClientDCC.BabifyActivity;
import com.babify.AndroidClientDCC.BabifyApplication;
import com.babify.AndroidClientDCC.Helpers.DCCHelper;
import com.babify.AndroidClientDCC.R;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Required;

public class FeedbackActivity extends BabifyActivity implements Validator.ValidationListener{
    EditText subjectText;

    @Required(order = 1)
    EditText messageText;

    Validator validator;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //DCCHelper.showAsPopup(this);
        setContentView(R.layout.activity_feedback);

        subjectText = (EditText)findViewById(R.id.feedback_subject);
        messageText = (EditText)findViewById(R.id.feedback_message);

        validator = new Validator(this);
        validator.setValidationListener(this);

        ActionBar bar = getActionBar();
        bar.setTitle(getString(R.string.feedback));
    }

    @Override
    public void onValidationSucceeded() {
        final String subject = subjectText.getText().toString();
        final String message = messageText.getText().toString();

        getDbHelper().addFeedback(getCurrentKigaId(), subject, message);

        ((BabifyApplication)getApplicationContext()).runSyncTask(getCurrentKigaId());

        finish();
    }

    @Override
    public void onValidationFailed(View failedView, Rule<?> failedRule) {
        DCCHelper.validationFailedRule(getApplicationContext(), failedView, failedRule);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_feedback, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_send_feedback:
                save();
                break;
            default:
                break;
        }

        return true;
    }

    public void save(){
        validator.validate();
    }
}