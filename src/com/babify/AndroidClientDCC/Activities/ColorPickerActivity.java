package com.babify.AndroidClientDCC.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import com.babify.AndroidClientDCC.R;
import com.larswerkman.colorpicker.ColorPicker;
import com.larswerkman.colorpicker.OpacityBar;
import com.larswerkman.colorpicker.SVBar;
import com.mbalychev.Shared.Helpers.UIHelper;


public class ColorPickerActivity extends Activity  implements ColorPicker.OnColorChangedListener {

    private ColorPicker picker;
    private SVBar svBar;
    private OpacityBar opacityBar;
    //private Button button;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        UIHelper.showAsPopup(this);
        setContentView(R.layout.activity_colorpicker);

        Intent intent = getIntent();
        int color = intent.getIntExtra("color",0);

        picker = (ColorPicker) findViewById(R.id.colorpicker_picker);
        svBar = (SVBar) findViewById(R.id.colorpicker_svbar);
        opacityBar = (OpacityBar) findViewById(R.id.colorpicker_opacitybar);
        //button = (Button) findViewById(R.id.colorpicker_pickcolor);
        picker.addSVBar(svBar);
        picker.addOpacityBar(opacityBar);
        picker.setOnColorChangedListener(this);

        picker.setOldCenterColor(color);
        picker.setColor(color);
    }

    @Override
    public void onColorChanged(int color){
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                save();
                break;
            default:
                break;
        }

        return true;
    }

    public void save() {
        Intent data = new Intent();
        int color = picker.getColor();
        data.putExtra("color", color);
        setResult(RESULT_OK, data);
        finish();
    }
}