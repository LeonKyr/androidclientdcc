package com.babify.AndroidClientDCC.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import com.babify.AndroidClientDCC.BabifyApplication;
import com.babify.AndroidClientDCC.DataLayer.DatabaseHelper;
import com.babify.AndroidClientDCC.DomainModel.Kindergarten;
import com.babify.AndroidClientDCC.DomainModel.KindergartenStatus;
import com.babify.AndroidClientDCC.Helpers.DCCHelper;
import com.babify.AndroidClientDCC.Helpers.WebServiceHelper;
import com.babify.AndroidClientDCC.R;
import com.babify.AndroidClientDCC.Synchronization.SyncKigaRequest;
import com.babify.AndroidClientDCC.Synchronization.SyncKigaResponse;
import com.kodart.httpzoid.*;
import com.mbalychev.Shared.Helpers.AeSimpleSHA1;
import com.mbalychev.Shared.Helpers.UIHelper;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Checked;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Required;
import com.mobsandgeeks.saripaar.annotation.TextRule;

// https://github.com/ragunathjawahar/android-saripaar

public class RegisterActivity extends Activity implements Validator.ValidationListener {

    @Required(order = 1)
    EditText nameEditText;

    @Required(order = 2)
    EditText addressEditText;

    @Required(order = 3)
    EditText phoneEditText;

    @Required(order = 4)
    EditText contactPersonEditText;

    @Required(order = 5)
    @Email(order = 6, message = "Enter valid email")
    EditText emailEditText;

    @Required(order = 7)
    @TextRule(order = 8, minLength = 6, message = "Enter at least 6 characters.")
    EditText passwordEditText;

    @Checked(order = 9, message = "You must agree to the terms.")
    CheckBox acceptConditionsCheckBox;

    CheckBox isAutoLoginCheckBox;

    DatabaseHelper dbHelper;

    Validator validator;
    ProgressDialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set View to register.xml
        setContentView(R.layout.activity_register);

        dbHelper = new DatabaseHelper(getApplicationContext());

        TextView loginScreen = (TextView) findViewById(R.id.link_to_login);
        Button btnRegister = (Button)findViewById(R.id.btnRegister);

        nameEditText = (EditText)findViewById(R.id.kindergarten_name);
        addressEditText = (EditText)findViewById(R.id.kindergarten_address);
        phoneEditText = (EditText)findViewById(R.id.kindergarten_phone);
        contactPersonEditText = (EditText)findViewById(R.id.kindergarten_contactPerson);
        emailEditText = (EditText)findViewById(R.id.kindergarten_email);
        passwordEditText = (EditText)findViewById(R.id.kindergarten_password);
        isAutoLoginCheckBox = (CheckBox)findViewById(R.id.kindergarten_is_autologin);
        acceptConditionsCheckBox = (CheckBox)findViewById(R.id.kindergarten_accept_conditions);

        // Listening to Login Screen link
        loginScreen.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i);
                finish();
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0)
            {
              validator.validate();
            }
        });

        validator = new Validator(this);
        validator.setValidationListener(this);


        dialog  = new ProgressDialog(RegisterActivity.this);

        // TODO: remove after testing
        /*nameEditText.setText("Test name");
        addressEditText.setText("Test address");
        phoneEditText.setText("12345678");
        emailEditText.setText("test105@test.com");
        passwordEditText.setText("111111");
        contactPersonEditText.setText("test person");
        acceptConditionsCheckBox.setChecked(true);*/
    }

    @Override
    public void onValidationSucceeded() {
        final String name = nameEditText.getText().toString();
        final String address = addressEditText.getText().toString();
        final String phone = phoneEditText.getText().toString();
        final String email = emailEditText.getText().toString();
        final String contactPerson = contactPersonEditText.getText().toString();
        final String password = passwordEditText.getText().toString();

        try
        {
            final String hashPassword = AeSimpleSHA1.SHA1(password);

            Kindergarten kiga = dbHelper.getKindergarten(email, password);
            if(kiga == null)
            {
                if(UIHelper.hasNetwork(getApplicationContext()))
                {
                    dialog.setMessage("Please wait..");
                    dialog.show();

                    final SyncKigaRequest request = new SyncKigaRequest();
                    request.email = email;
                    request.name = name;
                    request.phone = phone;
                    request.address = address;
                    request.password = hashPassword;
                    request.contact_person = contactPerson;

                    String deviceId = dbHelper.getOrCreateSetting(DCCHelper.deviceIdPrefKey, null);

                    request.device = WebServiceHelper.GetDevice(deviceId);

                    Http http = HttpFactory.create(getApplicationContext());
                    http.post(DCCHelper.ServiceUrl+"register/kindergarten/")
                            .data(request)
                            .handler(new ResponseHandler<SyncKigaResponse>(){
                                @Override
                                public void success(SyncKigaResponse response, HttpResponse httpResponse) {
                                    long kigaId = dbHelper.addKigaInfo(response.id, request.name, request.address, request.phone, request.email, request.password,
                                                                       isAutoLoginCheckBox.isChecked(), KindergartenStatus.Active, contactPerson);
                                    ((BabifyApplication) getApplicationContext()).setCurrentKiga(dbHelper.getKindergarten(response.id));

                                    UIHelper.showToast(getApplicationContext(), getString(R.string.welcome), Toast.LENGTH_LONG, UIHelper.TDSuccess);
                                    startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                                }

                                @Override
                                public void error(String message, HttpResponse response) {
                                    UIHelper.showToast(getApplicationContext(), getString(R.string.serviceError), Toast.LENGTH_LONG, UIHelper.TDError);
                                }

                                @Override
                                public void failure(NetworkError error) {
                                    UIHelper.showToast(getApplicationContext(), getString(R.string.serverError), Toast.LENGTH_LONG, UIHelper.TDError);
                                }

                                @Override
                                public void complete() {
                                    dialog.dismiss();
                                }
                            }).send();

                    //if(prefsDeviceId == null)
                        //deviceId = WebServiceHelper.RegisterDeviceId(getApplicationContext());



                    /*if(true)//(prefsDeviceId == null)
                    {
                        RegisterDeviceTask task = new RegisterDeviceTask();
                        task.execute(request);
                    }else{
                        DCCHelper.setDeviceId(getApplicationContext());
                        request.device = WebServiceHelper.GetDevice();

                        RegisterKigaTask registerKigaTask = new RegisterKigaTask();
                        registerKigaTask.execute(request);
                    }*/
                }else{
                    UIHelper.showToast(getApplicationContext(), getString(R.string.noConnection), Toast.LENGTH_LONG, UIHelper.TDError);
                }
            }else
            {
                UIHelper.showToast(getApplicationContext(), getString(R.string.register_kigaAlreadyExists), Toast.LENGTH_SHORT, UIHelper.TDError);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onValidationFailed(View failedView, Rule<?> failedRule) {
        DCCHelper.validationFailedRule(getApplicationContext(), failedView, failedRule);
    }

    /*
    public class RegisterDeviceTask extends AsyncTask<SyncKigaRegisterRequest, Void, String> {
        SyncKigaRegisterRequest input;

        protected String doInBackground(SyncKigaRegisterRequest... inputs) {
            input = inputs[0];

            String deviceId = WebServiceHelper.RegisterDeviceId(getApplicationContext());
            return deviceId;
        }

        protected void onPostExecute(String deviceId) {
            if(deviceId == null)
                DCCHelper.showToast(getApplicationContext(), getString(R.string.register_serverError), Toast.LENGTH_LONG, DCCHelper.TDError);
            else{
                prefs.edit().putString(DCCHelper.deviceIdPrefKey, deviceId).commit();
                DCCHelper.setDeviceId(getApplicationContext());
                input.device = WebServiceHelper.GetDevice();

                RegisterKigaTask registerKigaTask = new RegisterKigaTask();
                registerKigaTask.execute(input);
            }
        }
    }

    public class RegisterKigaTask extends AsyncTask<SyncKigaRegisterRequest, Void, String[]> {

        SyncKigaRegisterRequest input;

        protected String[] doInBackground(SyncKigaRegisterRequest... inputs) {

            input = inputs[0];

            final String[] responseObj = {null, null};

            Http http = HttpFactory.create(getApplicationContext());
            Cancellable canc = http.post(DCCHelper.ServiceUrl+"register/kindergarten/")
                    .data(input)
                    .handler(new ResponseHandler<SyncEntityResponse>() {
                        @Override
                        public void success(SyncEntityResponse response, HttpResponse httpResponse) {
                            responseObj[0] = "1";
                            responseObj[1] = response.externalId;
                        }

                        @Override
                        public void error(String message, HttpResponse response) {
                            responseObj[0] = "2";
                        }

                        @Override
                        public void failure(NetworkError error) {
                            responseObj[0] = "3";
                        }

                        @Override
                        public void complete() {
                            dialog.dismiss();
                        }
                    }).send();

            synchronized (canc){
                try {
                    canc.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            //return deviceId;
            return responseObj;
        }

        protected void onPostExecute(String[] result) {
            if(result[0].equals("1")) {
                DCCHelper.showToast(getApplicationContext(), getString(R.string.welcome), Toast.LENGTH_LONG, DCCHelper.TDSuccess);
                startActivity(new Intent(RegisterActivity.this, MainActivity.class));

                long kigaId = dbHelper.addKigaInfo(result[1], input.name, input.address, input.phone, input.email, input.password, isAutoLoginCheckBox.isChecked());
                if (kigaId > 0)
                    ((BabifyApplication) getApplicationContext()).setCurrentKiga(dbHelper.getKindergarten(result[1]));

            }
            else if(result[0].equals("2"))
                DCCHelper.showToast(getApplicationContext(), getString(R.string.register_serviceError), Toast.LENGTH_LONG, DCCHelper.TDError);

            else if(result[0].equals("3"))
                DCCHelper.showToast(getApplicationContext(), getString(R.string.register_serverError), Toast.LENGTH_LONG, DCCHelper.TDError);
            else
                DCCHelper.showToast(getApplicationContext(), getString(R.string.register_serverError), Toast.LENGTH_LONG, DCCHelper.TDError);
        }
    }       */
}