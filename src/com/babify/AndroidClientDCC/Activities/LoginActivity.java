package com.babify.AndroidClientDCC.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import com.babify.AndroidClientDCC.BabifyApplication;
import com.babify.AndroidClientDCC.DataLayer.DatabaseHelper;
import com.babify.AndroidClientDCC.DomainModel.Kindergarten;
import com.babify.AndroidClientDCC.Helpers.DCCHelper;
import com.babify.AndroidClientDCC.Helpers.WebServiceHelper;
import com.babify.AndroidClientDCC.R;
import com.babify.AndroidClientDCC.Synchronization.SyncKigaResponse;
import com.babify.AndroidClientDCC.Synchronization.SyncObjectWithDeviceRequest;
import com.kodart.httpzoid.*;
import com.mbalychev.Shared.Helpers.AeSimpleSHA1;
import com.mbalychev.Shared.Helpers.UIHelper;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Required;

public class LoginActivity extends Activity implements Validator.ValidationListener {

    @Required(order = 1)
    @Email(order = 2, message = "Enter valid email")
    EditText emailEditText;

    @Required(order = 3)
    EditText passwordEditText;

    CheckBox isAutoLoginCheckBox;

    DatabaseHelper dbHelper;

    ProgressDialog dialog;

    Validator validator;
    SharedPreferences prefs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        TextView registerScreen = (TextView) findViewById(R.id.link_to_register);
        Button btnLogin = (Button)findViewById(R.id.btnLogin);
        TextView forgotPassword = (TextView) findViewById(R.id.forgot_password);

        emailEditText = (EditText)findViewById(R.id.login_email);
        passwordEditText = (EditText)findViewById(R.id.login_password);
        isAutoLoginCheckBox = (CheckBox)findViewById(R.id.kindergarten_is_autologin);

        dbHelper = new DatabaseHelper(getApplicationContext());

        prefs = getSharedPreferences(DCCHelper.sharedPrefsName, MODE_PRIVATE);

        // Listening to register new account link
        registerScreen.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(i);
            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ForgotPasswordActivity.class);
                startActivity(i);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0)
            {
                validator.validate();
            }
        });

        validator = new Validator(this);
        validator.setValidationListener(this);

        dialog  = new ProgressDialog(LoginActivity.this);
    }

    /*@Override
    protected void onResume() {
        super.onResume();

        if(prefs.getBoolean("firstRun", true)){
            prefs.edit().putBoolean("firstRun", false).commit();

            Intent intent = new Intent(LoginActivity.this, TutorialActivity.class);
            intent.putExtra(DCCHelper.tutorialImages, DCCHelper.tutorialFirstTimeLayoutsArray);
            intent.putExtra(DCCHelper.redirectToExtra,"register");
            startActivity(intent);
        }else{
            Kindergarten activeKiga = dbHelper.getActiveKindergarten();
            if(activeKiga != null){
                if(activeKiga.getIsAutoLogin()){
                    ((BabifyApplication)getApplicationContext()).setCurrentKiga(activeKiga);
                    redirectToMainScreen();
                }else{
                    emailEditText.setText(activeKiga.getEmail());
                    isAutoLoginCheckBox.setChecked(activeKiga.getIsAutoLogin());
                }
            }
        }
    }*/

    private void redirectToMainScreen() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onValidationSucceeded() {
        final String email = emailEditText.getText().toString();
        String password =  passwordEditText.getText().toString();

        try{
            final String hashPassword = AeSimpleSHA1.SHA1(password);

            if(UIHelper.hasNetwork(getApplicationContext()))
            {
                dialog.setMessage(getString(R.string.pleaseWait));
                dialog.show();

                String deviceId = dbHelper.getOrCreateSetting(DCCHelper.deviceIdPrefKey, null);

                SyncObjectWithDeviceRequest request = new SyncObjectWithDeviceRequest();
                request.device = WebServiceHelper.GetDevice(deviceId);

                Http http = HttpFactory.create(getApplicationContext());
                http.post(DCCHelper.ServiceUrl+"login/kindergarten/"+email+"/"+hashPassword+"/")
                    .data(request)
                    .handler(new ResponseHandler<SyncKigaResponse>(){
                        @Override
                        public void success(SyncKigaResponse response, HttpResponse httpResponse) {
                            Kindergarten kiga = dbHelper.getKindergarten(response.id);
                            if(kiga == null){
                                dbHelper.addKigaInfo(response.id, response.name, response.address, response.phone, response.email, hashPassword,
                                        isAutoLoginCheckBox.isChecked(), response.status, response.contact_person);
                            }else{
                                dbHelper.updateKigaInfo(response.id, response.name, response.address, response.phone, response.email,
                                        isAutoLoginCheckBox.isChecked(), response.contact_person);

                                dbHelper.setEntitySynchronized(DatabaseHelper.KIGA_TABLE_NAME, kiga.getId(), null);
                                dbHelper.updateKigaStatus(response.id, response.status);
                            }

                            kiga = dbHelper.updateAndGetKindergartenAfterLogin(response.id, isAutoLoginCheckBox.isChecked());

                            initCurrentKiga(kiga);
                        }

                        @Override
                        public void error(String message, HttpResponse response) {
                            UIHelper.showToast(getApplicationContext(), getString(R.string.serviceError), Toast.LENGTH_LONG, UIHelper.TDError);
                        }

                        @Override
                        public void failure(NetworkError error) {
                            UIHelper.showToast(getApplicationContext(), getString(R.string.serverError), Toast.LENGTH_LONG, UIHelper.TDError);
                        }

                        @Override
                        public void complete() {
                            dialog.dismiss();
                        }
                    }).send();
            }else{
                //UIHelper.showToast(getApplicationContext(), getString(R.string.noConnection), Toast.LENGTH_LONG, UIHelper.TDError);
                // try to login using local DB
                Kindergarten kiga = dbHelper.getKindergarten(email, hashPassword);
                if(kiga != null){
                    initCurrentKiga(kiga);
                }else{
                    UIHelper.showToast(getApplicationContext(), getString(R.string.serviceError), Toast.LENGTH_LONG, UIHelper.TDError);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initCurrentKiga(Kindergarten kiga) {
        ((BabifyApplication)getApplicationContext()).setCurrentKiga(kiga);
        ((BabifyApplication)getApplicationContext()).runSyncTask(kiga.getKigaExternalId());
        redirectToMainScreen();
    }

    @Override
    public void onValidationFailed(View failedView, Rule<?> failedRule) {
        String message = failedRule.getFailureMessage();

        if (failedView instanceof EditText) {
            failedView.requestFocus();
            ((EditText) failedView).setError(message);
        } else {
            UIHelper.showToast(getApplicationContext(), message, Toast.LENGTH_SHORT, UIHelper.TDError);
        }
    }
}