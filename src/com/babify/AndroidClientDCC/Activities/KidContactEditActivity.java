package com.babify.AndroidClientDCC.Activities;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import com.babify.AndroidClientDCC.BabifyActivity;
import com.babify.AndroidClientDCC.DomainModel.KidContact;
import com.babify.AndroidClientDCC.Helpers.DCCHelper;
import com.babify.AndroidClientDCC.R;
import com.mbalychev.Shared.Helpers.UIHelper;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Required;

public class KidContactEditActivity extends BabifyActivity implements Validator.ValidationListener{
    @Required(order = 1)
    EditText nameText;

    @Required(order = 2)
    Spinner relationSpinner;

    EditText phoneText;
    EditText emailText;

    boolean isNew;
    int id;

    Validator validator;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        UIHelper.showAsPopup(this);
        setContentView(R.layout.kidcontact_edit);

        nameText = (EditText)findViewById(R.id.kidcontact_name);
        phoneText = (EditText)findViewById(R.id.kidcontact_phone);
        emailText = (EditText)findViewById(R.id.kidcontact_email);
        relationSpinner = (Spinner)findViewById(R.id.kidcontact_relation);

        Intent intent = getIntent();
        id = intent.getIntExtra("id",0);

        isNew = id == 0;

        ActionBar bar = getActionBar();

        if(!isNew){
            KidContact contact = getDbHelper().getKidContact(id, null);
            nameText.setText(contact.getName());
            phoneText.setText(contact.getPhone());
            emailText.setText(contact.getEmail());
            relationSpinner.setSelection(contact.getRelation());

            bar.setSubtitle(getString(R.string.updateContact));
        }
        else{
            String name = intent.getStringExtra("name");
            String phone = intent.getStringExtra("phone");
            String email = intent.getStringExtra("email");
            int relation = intent.getIntExtra("relation", 0);

            nameText.setText(name);
            phoneText.setText(phone);
            emailText.setText(email);
            relationSpinner.setSelection(relation);

            bar.setSubtitle(getString(R.string.newContact));
        }

        bar.setDisplayShowHomeEnabled(false);

        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_kidcontact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save_kidcontact:
                validator.validate();
                break;
            default:
                break;
        }

        return true;
    }

    @Override
    public void onValidationSucceeded() {
        Intent data = new Intent();
        data.putExtra("id",id);
        data.putExtra("name", nameText.getText().toString().trim());
        data.putExtra("relation", relationSpinner.getSelectedItemPosition());
        data.putExtra("phone", phoneText.getText().toString().trim());
        data.putExtra("email", emailText.getText().toString().trim());
        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public void onValidationFailed(View failedView, Rule<?> failedRule) {
        DCCHelper.validationFailedRule(getApplicationContext(), failedView, failedRule);
    }
}