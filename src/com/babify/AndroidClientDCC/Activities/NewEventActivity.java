package com.babify.AndroidClientDCC.Activities;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.babify.AndroidClientDCC.Activities.EventFragments.*;
import com.babify.AndroidClientDCC.Adapters.ActionsAdapter;
import com.babify.AndroidClientDCC.Adapters.GroupsAdapter;
import com.babify.AndroidClientDCC.Adapters.KidsAdapter;
import com.babify.AndroidClientDCC.Adapters.TeachersAdapter;
import com.babify.AndroidClientDCC.BabifyApplication;
import com.babify.AndroidClientDCC.DomainModel.*;
import com.babify.AndroidClientDCC.DomainModel.Action.*;
import com.babify.AndroidClientDCC.R;
import com.mbalychev.Shared.Domain.ActionKey;
import com.mbalychev.Shared.Domain.ActionType;
import com.mbalychev.Shared.Helpers.UIHelper;

import java.util.ArrayList;
import java.util.List;

public class NewEventActivity extends SliderBaseActivity {
    ListView kidsListView;
    ListView actionsListView;
    Spinner currentGroupSpinner;
    //Spinner currentTeacherSpinner;
    CheckBox selectAll;

    List<Kid> kids;
    List<Group> groups;
    List<Teacher> teachers;
    Group activeGroup;
    Teacher activeTecher;
    Fragment currentFragment;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_newevent, R.layout.left_slider_menu_full,R.string.newEvent);

        kidsListView = (ListView) findViewById(R.id.newevent_kids_listview);
        actionsListView = (ListView) findViewById(R.id.newevent_actions_listview);
        currentGroupSpinner = (Spinner) findViewById(R.id.current_group_spinner);
        selectAll = (CheckBox)findViewById(R.id.newevent_select_all);
        //currentTeacherSpinner = (Spinner) findViewById(R.id.current_teacher_spinner);

        checkForTeacherAmdGroup();
        updateKidsAdapter();

        actionsListView.setAdapter(new ActionsAdapter(this, R.layout.addevent_action_row, ActionType.values));

        SetFragment(new SignInOutFragment(ActionType.Signin));
        actionsListView.setItemChecked(0,true); // signin

        groups = getDbHelper().readGroups(getCurrentKigaId(), false);
        groups.add(0, new Group(getString(R.string.allGroups)));
        teachers = getDbHelper().readTeachers(getCurrentKigaId(), false);
        teachers.add(0, new Teacher(getString(R.string.chooseATeacher)));

        GroupsAdapter currentGroupAdapter = new GroupsAdapter(this, R.layout.event_group_row, groups, true);
        TeachersAdapter currentTeacherAdapter = new TeachersAdapter(this, R.layout.event_teacher_row, teachers, true);

        currentGroupSpinner.setAdapter(currentGroupAdapter);
        //currentTeacherSpinner.setAdapter(currentTeacherAdapter);

        ActionBar actionBar = getActionBar();

        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        ActionBar.OnNavigationListener navigationListener = new ActionBar.OnNavigationListener() {

            @Override
            public boolean onNavigationItemSelected(int itemPosition, long itemId) {
                if(itemPosition != 0){
                    Teacher teacher = teachers.get(itemPosition);
                    getDbHelper().activateTeacher(getCurrentKigaId(), teacher.getId());
                    activeTecher = getDbHelper().getActiveTeacher(getCurrentKigaId());
                }else{
                    getDbHelper().activateTeacher(getCurrentKigaId(), 0L);
                    activeTecher = null;
                }

                return false;
            }
        };
        getActionBar().setListNavigationCallbacks(currentTeacherAdapter, navigationListener);


        //currentGroupAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //currentTeacherAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        currentGroupSpinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if(position != 0){
                            Group group = groups.get(position);
                            getDbHelper().activateGroup(getCurrentKigaId(), group.getId());
                            activeGroup = getDbHelper().getActiveGroup(getCurrentKigaId());
                        }
                        else{
                            getDbHelper().activateGroup(getCurrentKigaId(), 0L);
                            activeGroup = null;
                        }

                        selectAll.setChecked(false);
                        updateKidsAdapter();
                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                }
        );
        /*currentTeacherSpinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if(position != 0){
                            Teacher teacher = teachers.get(position);
                            dbHelper.activateTeacher(getCurrentKigaId(), teacher.getId());
                            activeTecher = dbHelper.getActiveTeacher(getCurrentKigaId());
                        }else{
                            dbHelper.activateTeacher(getCurrentKigaId(), 0L);
                            activeTecher = null;
                        }
                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                }
        );*/

        actionsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String action = ((ActionsAdapter.ActionHolder)view.getTag()).action;

                if(action.equals(ActionType.Note))
                    SetFragment(new MessageFragment());
                else if(action.equals(ActionType.Signin))
                    SetFragment(new SignInOutFragment(ActionType.Signin));
                else if(action.equals(ActionType.Signout))
                    SetFragment(new SignInOutFragment(ActionType.Signout));
                else if(action.equals(ActionType.Photos))
                    SetFragment(new PhotosFragment());
                else if(action.equals(ActionType.Sleep))
                    SetFragment(new SleepFragment());
                else if(action.equals(ActionType.Meal))
                    SetFragment(new MealFragment());
                else if(action.equals(ActionType.Absence))
                    SetFragment(new AbsenceFragment());
                else if(action.equals(ActionType.WillHaveMeal))
                    SetFragment(new WillHaveMealFragment());
                else if(action.equals(ActionType.InternalNote))
                    SetFragment(new InternalNoteFragment());

            }
        });


        if(activeGroup != null){
            long id = activeGroup.getId();
            int size =groups.size();
            for(int i=0;i<size;i++)
            {
                if(groups.get(i).getId() == id)
                {
                    currentGroupSpinner.setSelection(i);
                    break;
                }
            }
        }
        if(activeTecher != null){
            long id = activeTecher.getId();
            int size =teachers.size();
            for(int i=0;i<size;i++)
            {
                if(teachers.get(i).getId() == id)
                {
                    //currentTeacherSpinner.setSelection(i);
                    actionBar.setSelectedNavigationItem(i);
                    break;
                }
            }
        }

        selectAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                for ( int i=0; i < kids.size(); i++) {
                    kidsListView.setItemChecked(i, isChecked);
                }
            }
        });
    }

    private void SetFragment(Fragment fragment){
        currentFragment = fragment;
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.newevent_details, fragment);
        ft.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //checkForTeacherAmdGroup();
    }

    private Boolean checkForTeacherAmdGroup(){
        boolean isValid = true;

        activeGroup = getDbHelper().getActiveGroup(getCurrentKigaId());
        activeTecher = getDbHelper().getActiveTeacher(getCurrentKigaId());

        /*if(activeGroup == null){
            DCCHelper.showToast(NewEventActivity.this, getString(R.string.selectGroup), Toast.LENGTH_SHORT, DCCHelper.TDError);
            isValid = false;
        }*/
        if(activeTecher == null){
            UIHelper.showToast(NewEventActivity.this, getString(R.string.selectTeacher), Toast.LENGTH_SHORT, UIHelper.TDError);
            isValid = false;
        }

        return isValid;
    }

    private void updateKidsAdapter(){

        long groupId = 0;
        if(activeGroup != null)
            groupId = activeGroup.getId();

        kids = getDbHelper().readKids(getCurrentKigaId(), groupId, false);
        kidsListView.setAdapter(new KidsAdapter(getApplicationContext(),R.layout.addevent_kid_row, kids));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_newevent, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case R.id.action_send_event:
                send();
                break;
            default:
                break;
        }

        return true;
    }

    public void send(){

        if(checkForTeacherAmdGroup()){
            List<Integer> selectedKidIds = new ArrayList<Integer>();

            SparseBooleanArray checked = kidsListView.getCheckedItemPositions();
            int size =checked.size();
            for(int i=0;i<size;i++)
            {
                int pos= checked.keyAt(i);
                boolean valueat = checked.valueAt(i);
                if(valueat )
                {
                    Kid kid = kids.get(pos);
                    selectedKidIds.add(kid.getId());
                }
            }

            if(currentFragment != null)
            {
                if(selectedKidIds.size() == 0 && !(currentFragment instanceof InternalNoteFragment)){
                    UIHelper.showToast(getApplicationContext(), getString(R.string.selectKids), Toast.LENGTH_SHORT, UIHelper.TDError);
                }

                else if(currentFragment instanceof IActionFactory){
                    IActionFactory actionFactory = (IActionFactory)currentFragment;

                    if(actionFactory.isValid()){
                        Action action = actionFactory.GetAction();

                        String type = actionFactory.GetActionType();

                        List<EventDetail> eventDetails = new ArrayList<EventDetail>();

                        if(type.equals(ActionType.Note)){
                            MessageAction messageAction = (MessageAction)action;

                            eventDetails.add(new EventDetail(ActionKey.Title, messageAction.getSubject()));
                            eventDetails.add(new EventDetail(ActionKey.Message, messageAction.getMessage()));

                        }else if (type.equals(ActionType.Signin) || type.equals(ActionType.Signout))
                        {
                            SignInOutAction signInOutAction = (SignInOutAction)action;

                            eventDetails.add(new EventDetail(ActionKey.Datetime, String.valueOf(signInOutAction.getDateTime())));
                        }
                        else if(type.equals(ActionType.Photos)){
                            ImageAction imageAction = (ImageAction)action;

                            eventDetails.add(new EventDetail(ActionKey.Message, imageAction.getMessage()));

                            for(String imagePath:imageAction.getImagePaths()){
                                long mediaId = getDbHelper().addMedia(getCurrentKigaId(),null,MediaType.image.toString(),null,null,imagePath);
                                eventDetails.add(new EventDetail(ActionKey.MediaId, String.valueOf(mediaId)));
                            }
                        }
                        else if(type.equals(ActionType.Sleep)){
                            SleepAction sleepAction = (SleepAction)action;

                            eventDetails.add(new EventDetail(ActionKey.SleepDuration, String.valueOf(sleepAction.getDuration())));
                            eventDetails.add(new EventDetail(ActionKey.SleepQuality, String.valueOf(sleepAction.getQuality())));
                            eventDetails.add(new EventDetail(ActionKey.Message, sleepAction.getMessage()));
                        }
                        else if(type.equals(ActionType.Meal)){
                            MealAction mealAction = (MealAction)action;

                            eventDetails.add(new EventDetail(ActionKey.MealType, String.valueOf(mealAction.getType())));
                            eventDetails.add(new EventDetail(ActionKey.MealQuality, String.valueOf(mealAction.getQuality())));
                            eventDetails.add(new EventDetail(ActionKey.Message, mealAction.getMessage()));
                        }
                        else if(type.equals(ActionType.Absence)){
                            AbsenceAction absenceAction = (AbsenceAction)action;

                            eventDetails.add(new EventDetail(ActionKey.AbsenceReason, String.valueOf(absenceAction.getReason())));
                            eventDetails.add(new EventDetail(ActionKey.Message, absenceAction.getMessage()));
                            eventDetails.add(new EventDetail(ActionKey.Datetime, String.valueOf(UIHelper.getCurrentTimeUnix())));
                        }
                        else if(type.equals(ActionType.WillHaveMeal)){
                            WillHaveMealAction mealAction = (WillHaveMealAction)action;

                            eventDetails.add(new EventDetail(ActionKey.MealType, String.valueOf(mealAction.getType())));
                            eventDetails.add(new EventDetail(ActionKey.Message, mealAction.getMessage()));
                            eventDetails.add(new EventDetail(ActionKey.Datetime, String.valueOf(UIHelper.getCurrentTimeUnix())));
                        }
                        else if(type.equals(ActionType.InternalNote)){
                            MessageAction messageAction = (MessageAction)action;

                            eventDetails.add(new EventDetail(ActionKey.Title, messageAction.getSubject()));
                            eventDetails.add(new EventDetail(ActionKey.Message, messageAction.getMessage()));
                        }

                        getDbHelper().addEvent(getCurrentKigaId(), type, activeTecher.getId(), selectedKidIds, eventDetails);

                        actionFactory.Reset();
                        UIHelper.showToast(getApplicationContext(), getString(R.string.messageWasSent), Toast.LENGTH_LONG, UIHelper.TDSuccess);

                        ((BabifyApplication)getApplicationContext()).runSyncTask(getCurrentKigaId());
                    }
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Toast.makeText(this, "In Activity OnActivityResult. ResultCode:"+resultCode, Toast.LENGTH_LONG).show();
        super.onActivityResult(requestCode, resultCode, data);
    }
}