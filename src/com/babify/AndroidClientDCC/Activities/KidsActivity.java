package com.babify.AndroidClientDCC.Activities;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import com.babify.AndroidClientDCC.Adapters.GroupsAdapter;
import com.babify.AndroidClientDCC.Adapters.KidsAdapter;
import com.babify.AndroidClientDCC.DomainModel.Group;
import com.babify.AndroidClientDCC.DomainModel.Kid;
import com.babify.AndroidClientDCC.DomainModel.Media;
import com.babify.AndroidClientDCC.Helpers.DCCHelper;
import com.babify.AndroidClientDCC.R;
import com.mbalychev.Shared.Helpers.ImageHelper;

import java.util.List;

public class KidsActivity extends SliderBaseActivity {
    GridView gridview;
    List<Kid> kids;
    List<Group> groups;
    Group activeGroup;
    GroupsAdapter currentGroupAdapter;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_kids, R.layout.left_slider_menu_full, R.string.kids);

        //setContentView(R.layout.activity_kids);

        gridview = (GridView) findViewById(R.id.kids_gridview);

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, android.view.View v, int position, long id) {
                Kid kid = kids.get(position);

                if(kid.getId() == DCCHelper.AddEntityId){
                    redirectToNew();
                }else{

                    Intent intent = new Intent(KidsActivity.this, KidEditActivity.class);
                    intent.putExtra("Id",kid.getId());
                    startActivity(intent);
                }
                /*Kid kid = kids.get(position);
                if(kid.getId() == DCCHelper.AddEntityId){
                    redirectToNew();
                }*/
            }
        });

        /*gridview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Kid kid = kids.get(position);

                if(kid.getId() == DCCHelper.AddEntityId){
                    redirectToNew();
                }else{

                    Intent intent = new Intent(KidsActivity.this, KidEditActivity.class);
                    intent.putExtra("Id",kid.getId());
                    startActivity(intent);
                }
                return false;
            }
        });*/


        activeGroup = getDbHelper().getActiveGroup(getCurrentKigaId());
        groups = getDbHelper().readGroups(getCurrentKigaId(), false);
        groups.add(0, new Group(getString(R.string.allGroups)));

        currentGroupAdapter = new GroupsAdapter(this, R.layout.event_group_row, groups, true);
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        ActionBar.OnNavigationListener navigationListener = new ActionBar.OnNavigationListener() {

            @Override
            public boolean onNavigationItemSelected(int itemPosition, long itemId) {
                if(itemPosition != 0){
                    activeGroup = groups.get(itemPosition);
                }else{
                    activeGroup = null;
                }
                updateAdapter();

                return false;
            }
        };
        getActionBar().setListNavigationCallbacks(currentGroupAdapter, navigationListener);

        if(activeGroup != null){
            long id = activeGroup.getId();
            int size =groups.size();
            for(int i=0;i<size;i++)
            {
                if(groups.get(i).getId() == id)
                {
                    //currentTeacherSpinner.setSelection(i);
                    actionBar.setSelectedNavigationItem(i);
                    break;
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateAdapter();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_kids, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case R.id.action_new_kid:
                redirectToNew();
                break;
            default:
                break;
        }

        return true;
    }

    private void redirectToNew(){
        Intent intent = new Intent(KidsActivity.this, KidEditActivity.class);
        startActivity(intent);
    }

    private void updateAdapter(){
        long groupId = 0;
        if(activeGroup != null)
            groupId = activeGroup.getId();

        kids  = getDbHelper().readKids(getCurrentKigaId(), groupId, false);

        Kid addKid = new Kid(getString(R.string.newKid));
        addKid.setId(DCCHelper.AddEntityId);
        Bitmap bitmap = ((BitmapDrawable)getApplicationContext().getResources().getDrawable(R.drawable.pm_new)).getBitmap();
        Media image = new Media(ImageHelper.getBytes(bitmap),null);
        addKid.setImage(image);
        kids.add(0, addKid);

        gridview.setAdapter(new KidsAdapter(getApplicationContext(),R.layout.kid_row, kids));
    }
}