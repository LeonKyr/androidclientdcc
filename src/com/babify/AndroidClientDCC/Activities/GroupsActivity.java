package com.babify.AndroidClientDCC.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import com.babify.AndroidClientDCC.Adapters.GroupsAdapter;
import com.babify.AndroidClientDCC.DomainModel.Group;
import com.babify.AndroidClientDCC.Helpers.DCCHelper;
import com.babify.AndroidClientDCC.R;

import java.util.List;

public class GroupsActivity extends SliderBaseActivity {
    GridView gridview;
    List<Group> groups;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_groups, R.layout.left_slider_menu_full, R.string.groups);

        //setContentView(R.layout.activity_groups);

        gridview = (GridView) findViewById(R.id.groups_gridview);

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, android.view.View v, int position, long id) {
                Group group = groups.get(position);

                if(group.getId() == DCCHelper.AddEntityId){
                    redirectToNew();
                }
                else{
                    getDbHelper().activateGroup(getCurrentKigaId(), group.getId());

                    Intent intent = new Intent(GroupsActivity.this, GroupEditActivity.class);
                    intent.putExtra("Id",group.getId());
                    startActivity(intent);
                }
                /*Group group = groups.get(position);

                if(group.getId() == DCCHelper.AddEntityId){
                    redirectToNew();
                }
                else{
                    getDbHelper().activateGroup(getCurrentKigaId(), group.getId());
                    updateAdapter();
                }*/
            }
        });

        /*gridview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Group group = groups.get(position);

                if(group.getId() == DCCHelper.AddEntityId){
                    redirectToNew();
                }
                else{
                    getDbHelper().activateGroup(getCurrentKigaId(), group.getId());

                    Intent intent = new Intent(GroupsActivity.this, GroupEditActivity.class);
                    intent.putExtra("Id",group.getId());
                    startActivity(intent);
                }
                return false;
            }
        });*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_groups, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case R.id.action_new_group:
                redirectToNew();
                break;
            default:
                break;
        }

        return true;
    }

    private void redirectToNew(){
        Intent intent = new Intent(this, GroupEditActivity.class);
        startActivity(intent);
    }
    @Override
    protected void onResume() {
        super.onResume();

        updateAdapter();
    }

    private void updateAdapter(){
        groups  = getDbHelper().readGroups(getCurrentKigaId(), false);

        Group addGroup = new Group(getString(R.string.newGroup));
        addGroup.setId(DCCHelper.AddEntityId);
        groups.add(0, addGroup);

        gridview.setAdapter(new GroupsAdapter(getApplicationContext(),R.layout.group_row, groups, false));
    }
}