package com.babify.AndroidClientDCC.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import com.babify.AndroidClientDCC.Adapters.TutorialAdapter;
import com.babify.AndroidClientDCC.BabifyApplication;
import com.babify.AndroidClientDCC.DataLayer.DatabaseHelper;
import com.babify.AndroidClientDCC.DomainModel.Kindergarten;
import com.babify.AndroidClientDCC.Helpers.DCCHelper;
import com.babify.AndroidClientDCC.R;
import com.mbalychev.Shared.Helpers.ZoomOutPageTransformer;
import com.viewpagerindicator.CirclePageIndicator;

public class InitActivity extends FragmentActivity {
    ViewPager viewPager;
    CirclePageIndicator indicator;
    TutorialAdapter adapter;
    Button loginBtn;
    Button registerBtn;
    DatabaseHelper dbHelper;
    //SharedPreferences prefs;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_init);

        dbHelper = new DatabaseHelper(getApplicationContext());

        int[] imageIds =
                {
                        R.drawable.tut_init1,
                        R.drawable.tut_init2,
                        R.drawable.tut_init3,
                };

        viewPager = (ViewPager)findViewById(R.id.tutorialPager);
        viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        indicator = (CirclePageIndicator)findViewById(R.id.tutorialIndicator);
        loginBtn = (Button)findViewById(R.id.loginBtn);
        registerBtn = (Button)findViewById(R.id.registerBtn);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(InitActivity.this, LoginActivity.class));
            }
        });

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(InitActivity.this, RegisterActivity.class));
            }
        });

        adapter = new TutorialAdapter(getSupportFragmentManager(), imageIds);
        viewPager.setAdapter(adapter);
        indicator.setViewPager(viewPager);

        //prefs = getSharedPreferences(DCCHelper.sharedPrefsName, MODE_PRIVATE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Kindergarten activeKiga = dbHelper.getActiveKindergarten();
        if(activeKiga != null){
            if(activeKiga.getIsAutoLogin()){
                ((BabifyApplication)getApplicationContext()).setCurrentKiga(activeKiga);
                ((BabifyApplication)getApplicationContext()).runSyncTask(activeKiga.getKigaExternalId());
                redirectToMainScreen();
            }
        }
    }

    private void redirectToMainScreen() {
        Intent intent = new Intent(InitActivity.this, MainActivity.class);
        startActivity(intent);
    }
}