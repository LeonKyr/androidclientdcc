package com.babify.AndroidClientDCC.Activities;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.babify.AndroidClientDCC.DataLayer.DatabaseHelper;
import com.babify.AndroidClientDCC.DomainModel.Kindergarten;
import com.babify.AndroidClientDCC.Helpers.DCCHelper;
import com.babify.AndroidClientDCC.R;
import com.kodart.httpzoid.*;
import com.mbalychev.Shared.Helpers.UIHelper;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Required;

public class ForgotPasswordActivity extends Activity implements Validator.ValidationListener{
    @Required(order = 1)
    @Email(order = 2, message = "Enter valid email")
    EditText emailText;

    Validator validator;
    DatabaseHelper dbHelper;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //DCCHelper.showAsPopup(this);
        setContentView(R.layout.activity_forgot_password);

        emailText = (EditText)findViewById(R.id.forgot_password_email);

        validator = new Validator(this);
        validator.setValidationListener(this);

        ActionBar bar = getActionBar();
        bar.setTitle(getString(R.string.forgotPassword));

        dbHelper = new DatabaseHelper(getApplicationContext());
    }

    @Override
    public void onValidationSucceeded() {
        final String email = emailText.getText().toString();

        Kindergarten kiga = dbHelper.getKindergarten(email, null);

        if(kiga == null || kiga.getKigaExternalId() == null || kiga.getKigaExternalId().isEmpty())
            UIHelper.showToast(getApplicationContext(), getString(R.string.serviceError), Toast.LENGTH_LONG, UIHelper.TDError);
        else{
            if(UIHelper.hasNetwork(getApplicationContext()))
            {
                Http http = HttpFactory.create(getApplicationContext());
                http.post(DCCHelper.ServiceUrl+"forgot-password/kindergarten/"+kiga.getKigaExternalId()+"/")
                        //.data(request)
                        .handler(new ResponseHandler<String>() {
                            @Override
                            public void success(String response, HttpResponse httpResponse) {
                                UIHelper.showToast(getApplicationContext(), getString(R.string.forgotPasswordSuccess), Toast.LENGTH_LONG, UIHelper.TDSuccess);
                                finish();
                            }

                            @Override
                            public void error(String message, HttpResponse response) {
                                UIHelper.showToast(getApplicationContext(), getString(R.string.serviceError), Toast.LENGTH_LONG, UIHelper.TDError);
                            }

                            @Override
                            public void failure(NetworkError error) {
                                UIHelper.showToast(getApplicationContext(), getString(R.string.serverError), Toast.LENGTH_LONG, UIHelper.TDError);
                            }
                        }).send();
            }else{
                UIHelper.showToast(getApplicationContext(), getString(R.string.noConnection), Toast.LENGTH_LONG, UIHelper.TDError);
            }
        }
    }

    @Override
    public void onValidationFailed(View failedView, Rule<?> failedRule) {
        DCCHelper.validationFailedRule(getApplicationContext(), failedView, failedRule);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_feedback, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_send_feedback:
                save();
                break;
            default:
                break;
        }

        return true;
    }

    public void save(){
        validator.validate();
    }
}