package com.babify.AndroidClientDCC.Activities;

import android.app.ActionBar;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.babify.AndroidClientDCC.BabifyActivity;
import com.babify.AndroidClientDCC.BabifyApplication;
import com.babify.AndroidClientDCC.DomainModel.Kindergarten;
import com.babify.AndroidClientDCC.Helpers.DCCHelper;
import com.babify.AndroidClientDCC.R;
import com.mbalychev.Shared.Helpers.AeSimpleSHA1;
import com.mbalychev.Shared.Helpers.UIHelper;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Required;
import com.mobsandgeeks.saripaar.annotation.TextRule;

public class ChangePasswordActivity extends BabifyActivity implements Validator.ValidationListener{
    @Required(order = 1)
    @TextRule(order = 2, minLength = 6, message = "Enter at least 6 characters.")
    EditText oldPwdText;

    @Required(order = 3)
    @TextRule(order = 4, minLength = 6, message = "Enter at least 6 characters.")
    EditText newPwdText;

    Validator validator;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        UIHelper.showAsPopup(this);
        setContentView(R.layout.activity_changepassword);

        oldPwdText = (EditText)findViewById(R.id.changepwd_oldpwd);
        newPwdText = (EditText)findViewById(R.id.changepwd_newpwd);

        validator = new Validator(this);
        validator.setValidationListener(this);

        ActionBar bar = getActionBar();
        bar.setTitle(getString(R.string.changePassword));
    }

    @Override
    public void onValidationSucceeded() {
        final String oldPwd = oldPwdText.getText().toString();
        final String newPwd = newPwdText.getText().toString();

        try{
            final String hashOldPassword = AeSimpleSHA1.SHA1(oldPwd);
            final String hashNewPassword = AeSimpleSHA1.SHA1(newPwd);

            Kindergarten kiga = getCurrentKindergarten();
            if(!kiga.getPassword().equals(hashOldPassword))
                UIHelper.showToast(getApplicationContext(), getString(R.string.oldPasswordIsWrong), Toast.LENGTH_LONG, UIHelper.TDError);
            else{
                getDbHelper().updateKigaPassword(getCurrentKigaId(), hashNewPassword);
                kiga = getDbHelper().getKindergarten(getCurrentKigaId());
                ((BabifyApplication)getApplicationContext()).setCurrentKiga(kiga);

                UIHelper.showToast(getApplicationContext(), getString(R.string.passwordChanged), Toast.LENGTH_LONG, UIHelper.TDSuccess);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finish();
    }

    @Override
    public void onValidationFailed(View failedView, Rule<?> failedRule) {
        DCCHelper.validationFailedRule(getApplicationContext(), failedView, failedRule);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_feedback, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_send_feedback:
                save();
                break;
            default:
                break;
        }

        return true;
    }

    public void save(){
        validator.validate();
    }
}