package com.babify.AndroidClientDCC.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.babify.AndroidClientDCC.Adapters.GroupsAdapter;
import com.babify.AndroidClientDCC.Adapters.KidsAdapter;
import com.babify.AndroidClientDCC.BabifyActivity;
import com.babify.AndroidClientDCC.DataLayer.DatabaseHelper;
import com.babify.AndroidClientDCC.DomainModel.Group;
import com.babify.AndroidClientDCC.DomainModel.Kid;
import com.babify.AndroidClientDCC.R;
import com.mbalychev.Shared.Helpers.UIHelper;

import java.util.ArrayList;
import java.util.List;

public class ChooseKidActivity extends BabifyActivity {
    private ListView kidsList;
    private Spinner currentGroupSpinner;
    private CheckBox selectAll;

    List<Kid> kids;
    List<Group> groups;
    Group activeGroup;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //super.onCreate(savedInstanceState, com.babify.AndroidClientDCC.R.layout.activity_choosekid, 0, com.babify.AndroidClientDCC.R.string.addKid);
        UIHelper.showAsPopup(this);
        setContentView(R.layout.activity_choosekid);

        kidsList = (ListView) findViewById(R.id.newactivity_kids_listview);
        currentGroupSpinner = (Spinner) findViewById(R.id.current_group_spinner);
        selectAll = (CheckBox)findViewById(R.id.newactivity_select_all);

        groups = getDbHelper().readGroups(getCurrentKigaId(), false);
        groups.add(0, new Group(getString(R.string.allGroups)));

        GroupsAdapter currentGroupAdapter = new GroupsAdapter(this, R.layout.event_group_row, groups, true);
        currentGroupSpinner.setAdapter(currentGroupAdapter);

        currentGroupSpinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if(position != 0){
                            activeGroup = groups.get(position);
                        }
                        else{
                            activeGroup = groups.get(0); // all
                        }

                        selectAll.setChecked(false);
                        updateKidsAdapter();
                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                }
        );

        selectAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                for ( int i=0; i < kids.size(); i++) {
                    kidsList.setItemChecked(i, isChecked);
                }
            }
        });

        activeGroup = groups.get(0); // all
        updateKidsAdapter();
    }

    private void updateKidsAdapter(){
        kids = getDbHelper().readKids(getCurrentKigaId(), activeGroup.getId(), false);

        KidsAdapter adapter = new KidsAdapter(getApplicationContext(), R.layout.addevent_kid_row, kids);
        kidsList.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:

                ArrayList<String> selectedKidIds = new ArrayList<String>();

                SparseBooleanArray checked = kidsList.getCheckedItemPositions();
                int size =checked.size();
                for(int i=0;i<size;i++)
                {
                    int pos= checked.keyAt(i);
                    boolean valueat = checked.valueAt(i);
                    if(valueat )
                    {
                        Kid kid = kids.get(pos);
                        selectedKidIds.add(kid.getExternalId());
                    }
                }
                Intent data = new Intent();
                data.putStringArrayListExtra("kidExternalIds", selectedKidIds);
                setResult(RESULT_OK, data);
                finish();
                break;
            default:
                break;
        }

        return true;
    }
}