package com.babify.AndroidClientDCC.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.babify.AndroidClientDCC.Adapters.ScheduledActivitiesAdapter;
import com.babify.AndroidClientDCC.DomainModel.ScheduledActivity;
import com.babify.AndroidClientDCC.R;

import java.util.List;

public class SchedulerActivity extends SliderBaseActivity {
    private ListView scheduledActivitieslistView;
    private ScheduledActivitiesAdapter scheduledActivitiesAdapter;
    private List<ScheduledActivity> scheduledActivities;

    public void onCreate(Bundle savedInstanceState) {
        //super.onCreate(savedInstanceState);
        super.onCreate(savedInstanceState, R.layout.activity_scheduler, R.layout.left_slider_menu_full, R.string.scheduler);

        scheduledActivitieslistView = (ListView) findViewById(R.id.scheduled_activities_list);

        scheduledActivitieslistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                ScheduledActivity activity = scheduledActivities.get(position);
                Intent intent = new Intent(SchedulerActivity.this, SchedulerActivityEditActivity.class);
                intent.putExtra("Id", activity.getId());
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        scheduledActivities = getDbHelper().readScheduledActivities(getCurrentKigaId(), false);
        scheduledActivitiesAdapter = new ScheduledActivitiesAdapter(getApplicationContext(), R.layout.scheduled_activities_list_row, scheduledActivities);
        scheduledActivitieslistView.setAdapter(scheduledActivitiesAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_scheduledactivities, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case R.id.action_new_activity:
                Intent intent = new Intent(SchedulerActivity.this, SchedulerActivityEditActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }

        return true;
    }
}