package com.babify.AndroidClientDCC.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import com.babify.AndroidClientDCC.Adapters.FullScreenImageAdapter;
import com.babify.AndroidClientDCC.R;

import java.util.ArrayList;

public class FullScreenImageViewActivity extends Activity
{
    private FullScreenImageAdapter adapter;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_fullscreen);

        viewPager = (ViewPager) findViewById(R.id.image_pager);

        Intent i = getIntent();
        int position = i.getIntExtra("position", 0);
        ArrayList<String> filePaths = i.getStringArrayListExtra("filePaths");

        adapter = new FullScreenImageAdapter(FullScreenImageViewActivity.this, filePaths);

        viewPager.setAdapter(adapter);

        // displaying selected image first
        viewPager.setCurrentItem(position);
    }
}