package com.babify.AndroidClientDCC.DataLayer;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.babify.AndroidClientDCC.DomainModel.*;
import com.babify.AndroidClientDCC.Helpers.DCCHelper;
import com.babify.AndroidClientDCC.Synchronization.SyncActivityDateRequest;
import com.babify.AndroidClientDCC.Synchronization.SyncGroupResponse;
import com.babify.AndroidClientDCC.Synchronization.SyncKidContactResponse;
import com.babify.AndroidClientDCC.Synchronization.SyncKidResponse;
import com.babify.AndroidClientDCC.Synchronization.SyncTeacherResponse;
import com.mbalychev.Shared.Domain.ActionType;
import com.mbalychev.Shared.Helpers.UIHelper;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DatabaseHelper  extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    protected static final String DATABASE_NAME = "AndroidClientDCC_db";

    private static final String SYNCHRONIZATION_ACTION_COLUMN = "sync_action";
    private static final String SYNCHRONIZATION_ACTION_SQL = SYNCHRONIZATION_ACTION_COLUMN + " INTEGER NOT NULL,";

    private static final String KIGA_EXTERNALID_COLUMN = "kiga_externalid";
    private static final String KIGA_EXTERNALID_SQL = KIGA_EXTERNALID_COLUMN + " TEXT NOT NULL,";

    private static final String EXTERNALID_COLUMN = "externalid";
    private static final String EXTERNALID_SQL = EXTERNALID_COLUMN + " TEXT,";

    private static final String MEDIA_ID_COLUMN = "mediaid";
    private static final String MEDIA_ID_SQL = MEDIA_ID_COLUMN + " INTEGER,";


    public static final String GROUPS_TABLE_NAME = "groups";
    private static final String GROUPS_TABLE_ID_COLUMN = "_id";
    private static final String GROUPS_TABLE_NAME_COLUMN = "name";
    private static final String GROUPS_TABLE_COLOR_COLUMN = "color";
    private static final String GROUPS_TABLE_ISACTIVE_COLUMN = "isactive";

    private static final String GROUPS_TABLE_CREATE = "CREATE TABLE "
            + GROUPS_TABLE_NAME + "("
            + GROUPS_TABLE_ID_COLUMN   + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KIGA_EXTERNALID_SQL
            + EXTERNALID_SQL
            + SYNCHRONIZATION_ACTION_SQL
            + GROUPS_TABLE_NAME_COLUMN + " TEXT NOT NULL,"
            + GROUPS_TABLE_ISACTIVE_COLUMN + " INTEGER NOT NULL,"
            + GROUPS_TABLE_COLOR_COLUMN + " TEXT); ";


    public static final String TEACHERS_TABLE_NAME = "teachers";
    private static final String TEACHERS_TABLE_ID_COLUMN = "_id";
    private static final String TEACHERS_TABLE_NAME_COLUMN = "name";
    private static final String TEACHERS_TABLE_ISACTIVE_COLUMN = "isactive";

    private static final String TEACHERS_TABLE_CREATE = "CREATE TABLE "
            + TEACHERS_TABLE_NAME + "("
            + TEACHERS_TABLE_ID_COLUMN   + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KIGA_EXTERNALID_SQL
            + EXTERNALID_SQL
            + SYNCHRONIZATION_ACTION_SQL
            + TEACHERS_TABLE_NAME_COLUMN + " TEXT NOT NULL,"
            + MEDIA_ID_SQL
            + TEACHERS_TABLE_ISACTIVE_COLUMN + " INTEGER NOT NULL);";

    public static final String KIDS_TABLE_NAME = "kids";
    private static final String KIDS_TABLE_ID_COLUMN = "_id";
    private static final String KIDS_TABLE_UNIQUEID_COLUMN = "unique_id";
    private static final String KIDS_TABLE_GROUPID_COLUMN = "group_id";
    private static final String KIDS_TABLE_NAME_COLUMN = "name";
    private static final String KIDS_TABLE_BIRTHDAY_COLUMN = "birthday";
    private static final String KIDS_TABLE_HASLISTENERS_COLUMN = "haslisteners";

    private static final String KIDS_TABLE_CREATE = "CREATE TABLE "
            + KIDS_TABLE_NAME + "("
            + KIDS_TABLE_ID_COLUMN   + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KIGA_EXTERNALID_SQL
            + EXTERNALID_SQL
            + SYNCHRONIZATION_ACTION_SQL
            + KIDS_TABLE_UNIQUEID_COLUMN + " TEXT,"
            + KIDS_TABLE_GROUPID_COLUMN + " INTEGER,"
            + KIDS_TABLE_NAME_COLUMN + " TEXT NOT NULL,"
            + KIDS_TABLE_HASLISTENERS_COLUMN + " INTEGER NOT NULL,"
            + MEDIA_ID_SQL
            + KIDS_TABLE_BIRTHDAY_COLUMN + " TEXT);";

    public static final String KID_CONTACTS_TABLE_NAME = "kid_contacts";
    private static final String KID_CONTACTS_TABLE_ID_COLUMN = "_id";
    private static final String KID_CONTACTS_TABLE_KIDID_COLUMN = "kid_id";
    private static final String KID_CONTACTS_TABLE_NAME_COLUMN = "name";
    private static final String KID_CONTACTS_TABLE_RELATION_COLUMN = "relation";
    private static final String KID_CONTACTS_TABLE_PHONE_COLUMN = "phone";
    private static final String KID_CONTACTS_TABLE_EMAIL_COLUMN = "email";
    private static final String KID_CONTACTS_TABLE_IS_INVITED_COLUMN = "is_invited";

    private static final String KID_CONTACTS_TABLE_CREATE = "CREATE TABLE "
            + KID_CONTACTS_TABLE_NAME + "("
            + KID_CONTACTS_TABLE_ID_COLUMN   + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KIGA_EXTERNALID_SQL
            + EXTERNALID_SQL
            + SYNCHRONIZATION_ACTION_SQL
            + KID_CONTACTS_TABLE_KIDID_COLUMN + " INTEGER,"
            + KID_CONTACTS_TABLE_NAME_COLUMN + " TEXT NOT NULL,"
            + KID_CONTACTS_TABLE_RELATION_COLUMN + " INTEGER,"
            + KID_CONTACTS_TABLE_PHONE_COLUMN + " TEXT,"
            + KID_CONTACTS_TABLE_EMAIL_COLUMN + " TEXT,"
            + KID_CONTACTS_TABLE_IS_INVITED_COLUMN + " INTEGER);";

    public static final String EVENTS_TABLE_NAME = "events";
    private static final String EVENTS_TABLE_ID_COLUMN = "_id";
    private static final String EVENTS_TABLE_TYPE_COLUMN = "eventtype";
    private static final String EVENTS_TABLE_TEACHERID_COLUMN = "teacher_id";
    private static final String EVENTS_TABLE_CREATEDON_COLUMN = "createdon";

    private static final String EVENTS_TABLE_CREATE = "CREATE TABLE "
            + EVENTS_TABLE_NAME + "("
            + EVENTS_TABLE_ID_COLUMN   + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + EVENTS_TABLE_TYPE_COLUMN + " TEXT NOT NULL,"
            + KIGA_EXTERNALID_SQL
            + EXTERNALID_SQL
            + SYNCHRONIZATION_ACTION_SQL
            + EVENTS_TABLE_TEACHERID_COLUMN + " INTEGER,"
            + EVENTS_TABLE_CREATEDON_COLUMN + " TEXT NOT NULL);";

    private static final String EVENT_TO_KIDS_TABLE_NAME = "event_kids";
    private static final String EVENT_TO_KIDS_TABLE_ID_COLUMN = "_id";
    private static final String EVENT_TO_KIDS_TABLE_EVENTID_COLUMN = "event_id";
    private static final String EVENT_TO_KIDS_TABLE_KIDID_COLUMN = "kid_id";

    private static final String EVENT_TO_KIDS_TABLE_CREATE = "CREATE TABLE "
            + EVENT_TO_KIDS_TABLE_NAME + "("
            + EVENT_TO_KIDS_TABLE_ID_COLUMN   + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + EVENT_TO_KIDS_TABLE_EVENTID_COLUMN + " INTEGER,"
            + EVENT_TO_KIDS_TABLE_KIDID_COLUMN + " INTEGER);";

    private static final String EVENT_DETAILS_TABLE_NAME = "event_details";
    private static final String EVENT_DETAILS_TABLE_ID_COLUMN = "_id";
    private static final String EVENT_DETAILS_TABLE_EVENTID_COLUMN = "event_id";
    private static final String EVENT_DETAILS_TABLE_KEY_COLUMN = "key";
    private static final String EVENT_DETAILS_TABLE_VALUE_COLUMN = "value";

    private static final String EVENT_DETAILS_TABLE_CREATE = "CREATE TABLE "
            + EVENT_DETAILS_TABLE_NAME + "("
            + EVENT_DETAILS_TABLE_ID_COLUMN   + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + EVENT_DETAILS_TABLE_EVENTID_COLUMN + " INTEGER,"
            + EVENT_DETAILS_TABLE_KEY_COLUMN + " TEXT,"
            + EVENT_DETAILS_TABLE_VALUE_COLUMN + " TEXT);";

    public static final String KIGA_TABLE_NAME = "kindergarten";
    private static final String KIGA_TABLE_ID_COLUMN = "_id";
    private static final String KIGA_TABLE_NAME_COLUMN = "name";
    private static final String KIGA_TABLE_ADDRESS_COLUMN = "address";
    private static final String KIGA_TABLE_PHONE_COLUMN = "phone";
    private static final String KIGA_TABLE_EMAIL_COLUMN = "email";
    private static final String KIGA_TABLE_PASSWORD_HASH_COLUMN = "password_hash";
    private static final String KIGA_TABLE_IS_AUTOLOGIN_COLUMN = "is_autologin";
    private static final String KIGA_TABLE_IS_LAST_COLUMN = "is_last";
    private static final String KIGA_TABLE_STATUS_COLUMN = "status";
    private static final String KIGA_TABLE_CONTACT_PERSON_COLUMN = "contact_person";

    private static final String KIGA_TABLE_CREATE = "CREATE TABLE "
            + KIGA_TABLE_NAME + "("
            + KIGA_TABLE_ID_COLUMN   + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KIGA_EXTERNALID_SQL
            + SYNCHRONIZATION_ACTION_SQL
            + KIGA_TABLE_NAME_COLUMN + " TEXT,"
            + KIGA_TABLE_ADDRESS_COLUMN + " TEXT,"
            + KIGA_TABLE_PHONE_COLUMN + " TEXT,"
            + KIGA_TABLE_EMAIL_COLUMN + " TEXT,"
            + KIGA_TABLE_IS_AUTOLOGIN_COLUMN + " int,"
            + KIGA_TABLE_IS_LAST_COLUMN + " int,"
            + KIGA_TABLE_STATUS_COLUMN + " TEXT,"
            + KIGA_TABLE_CONTACT_PERSON_COLUMN + " TEXT,"
            + KIGA_TABLE_PASSWORD_HASH_COLUMN + " TEXT);";

    public static final String FEEDBACK_TABLE_NAME = "feedback";
    private static final String FEEDBACK_TABLE_ID_COLUMN = "_id";
    private static final String FEEDBACK_TABLE_SUBJECT_COLUMN = "subject";
    private static final String FEEDBACK_TABLE_MESSAGE_COLUMN = "message";

    private static final String FEEDBACK_TABLE_CREATE = "CREATE TABLE "
            + FEEDBACK_TABLE_NAME + "("
            + FEEDBACK_TABLE_ID_COLUMN   + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KIGA_EXTERNALID_SQL
            + SYNCHRONIZATION_ACTION_SQL
            + FEEDBACK_TABLE_SUBJECT_COLUMN + " TEXT,"
            + FEEDBACK_TABLE_MESSAGE_COLUMN + " TEXT);";

    public static final String MEDIA_TABLE_NAME = "media";
    private static final String MEDIA_TABLE_ID_COLUMN = "_id";
    private static final String MEDIA_TABLE_TYPE_COLUMN = "type";
    private static final String MEDIA_TABLE_NAME_COLUMN = "name";
    private static final String MEDIA_TABLE_DATA_COLUMN = "data";
    private static final String MEDIA_TABLE_FILEPATH_COLUMN = "filepath";

    private static final String MEDIA_TABLE_CREATE = "CREATE TABLE "
            + MEDIA_TABLE_NAME + "("
            + MEDIA_TABLE_ID_COLUMN   + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KIGA_EXTERNALID_SQL
            + EXTERNALID_SQL
            + SYNCHRONIZATION_ACTION_SQL
            + MEDIA_TABLE_TYPE_COLUMN + " TEXT,"
            + MEDIA_TABLE_NAME_COLUMN + " TEXT,"
            + MEDIA_TABLE_FILEPATH_COLUMN + " TEXT,"
            + MEDIA_TABLE_DATA_COLUMN + " TEXT);";

    public static final String SETTINGS_TABLE_NAME = "settings";
    private static final String SETTINGS_TABLE_ID_COLUMN = "_id";
    private static final String SETTINGS_TABLE_NAME_COLUMN = "name";
    private static final String SETTINGS_TABLE_VALUE_COLUMN = "value";

    private static final String SETTINGS_TABLE_CREATE = "CREATE TABLE "
            + SETTINGS_TABLE_NAME + "("
            + SETTINGS_TABLE_ID_COLUMN   + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + SETTINGS_TABLE_NAME_COLUMN + " TEXT,"
            + SETTINGS_TABLE_VALUE_COLUMN + " TEXT);";

    public static final String SCHEDULED_ACTIVITIES_TABLE_NAME = "scheduled_activities";
    private static final String SCHEDULED_ACTIVITIES_TABLE_ID_COLUMN = "_id";
    private static final String SCHEDULED_ACTIVITIES_TABLE_ISACTIVE_COLUMN = "isactive";
    private static final String SCHEDULED_ACTIVITIES_TABLE_TYPE_COLUMN = "type";
    private static final String SCHEDULED_ACTIVITIES_TABLE_TITLE_COLUMN = "title";
    private static final String SCHEDULED_ACTIVITIES_TABLE_DESCRIPTION_COLUMN = "description";
    private static final String SCHEDULED_ACTIVITIES_TABLE_CREATEDON_COLUMN = "createdon";

    private static final String SCHEDULED_ACTIVITIES_TABLE_CREATE = "CREATE TABLE "
            + SCHEDULED_ACTIVITIES_TABLE_NAME + "("
            + SCHEDULED_ACTIVITIES_TABLE_ID_COLUMN   + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KIGA_EXTERNALID_SQL
            + EXTERNALID_SQL
            + SYNCHRONIZATION_ACTION_SQL
            + SCHEDULED_ACTIVITIES_TABLE_ISACTIVE_COLUMN + " INTEGER,"
            + SCHEDULED_ACTIVITIES_TABLE_TYPE_COLUMN + " TEXT NOT NULL,"
            + SCHEDULED_ACTIVITIES_TABLE_TITLE_COLUMN + " TEXT,"
            + SCHEDULED_ACTIVITIES_TABLE_DESCRIPTION_COLUMN + " TEXT,"
            + SCHEDULED_ACTIVITIES_TABLE_CREATEDON_COLUMN + " TEXT NOT NULL);";

    private static final String SCHEDULED_ACTIVITY_TO_KIDS_TABLE_NAME = "scheduledactivity_kids";
    private static final String SCHEDULED_ACTIVITY_TO_KIDS_TABLE_ID_COLUMN = "_id";
    private static final String SCHEDULED_ACTIVITY_TO_KIDS_TABLE_SCHEDULED_ACTIVITYID_COLUMN = "scheduledactivity_id";
    private static final String SCHEDULED_ACTIVITY_TO_KIDS_TABLE_KID_EXTERNALID_COLUMN = "kid_externalid";

    private static final String SCHEDULED_ACTIVITY_TO_KIDS_TABLE_CREATE = "CREATE TABLE "
            + SCHEDULED_ACTIVITY_TO_KIDS_TABLE_NAME + "("
            + SCHEDULED_ACTIVITY_TO_KIDS_TABLE_ID_COLUMN   + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + EXTERNALID_SQL
            + SCHEDULED_ACTIVITY_TO_KIDS_TABLE_SCHEDULED_ACTIVITYID_COLUMN + " INTEGER,"
            + SCHEDULED_ACTIVITY_TO_KIDS_TABLE_KID_EXTERNALID_COLUMN + " INTEGER);";

    private static final String SCHEDULED_ACTIVITY_DATES_TABLE_NAME = "scheduledactivity_dates";
    private static final String SCHEDULED_ACTIVITY_DATES_TABLE_ID_COLUMN = "_id";
    private static final String SCHEDULED_ACTIVITY_DATES_TABLE_SCHEDULED_ACTIVITYID_COLUMN = "scheduledactivity_id";
    private static final String SCHEDULED_ACTIVITY_DATES_TABLE_DATE_COLUMN = "date";
    private static final String SCHEDULED_ACTIVITY_DATES_TABLE_REMINDER_TYPE_COLUMN = "reminder_type";
    private static final String SCHEDULED_ACTIVITY_DATES_TABLE_REMINDER_MESSAGE_COLUMN = "reminder_message";

    private static final String SCHEDULED_ACTIVITY_DATES_TABLE_CREATE = "CREATE TABLE "
            + SCHEDULED_ACTIVITY_DATES_TABLE_NAME + "("
            + SCHEDULED_ACTIVITY_DATES_TABLE_ID_COLUMN   + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + SCHEDULED_ACTIVITY_DATES_TABLE_SCHEDULED_ACTIVITYID_COLUMN + " INTEGER,"
            + EXTERNALID_SQL
            + SCHEDULED_ACTIVITY_DATES_TABLE_DATE_COLUMN + " TEXT,"
            + SCHEDULED_ACTIVITY_DATES_TABLE_REMINDER_TYPE_COLUMN + " TEXT,"
            + SCHEDULED_ACTIVITY_DATES_TABLE_REMINDER_MESSAGE_COLUMN + " TEXT);";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(KIGA_TABLE_CREATE);
            db.execSQL(MEDIA_TABLE_CREATE);

            db.execSQL(GROUPS_TABLE_CREATE);
            db.execSQL(TEACHERS_TABLE_CREATE);
            db.execSQL(KIDS_TABLE_CREATE);
            db.execSQL(EVENT_TO_KIDS_TABLE_CREATE);
            db.execSQL(KID_CONTACTS_TABLE_CREATE);

            db.execSQL(EVENTS_TABLE_CREATE);
            db.execSQL(EVENT_DETAILS_TABLE_CREATE);

            db.execSQL(FEEDBACK_TABLE_CREATE);

            db.execSQL(SETTINGS_TABLE_CREATE);

            db.execSQL(SCHEDULED_ACTIVITIES_TABLE_CREATE);
            db.execSQL(SCHEDULED_ACTIVITY_TO_KIDS_TABLE_CREATE);
            db.execSQL(SCHEDULED_ACTIVITY_DATES_TABLE_CREATE);

            Log.e("dbAdapter", "DB sucessfully created");
        } catch (Exception e) {
            Log.e("dbAdapter", e.getMessage().toString());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
    }

    public List<Group> readGroups(String kigaId, Boolean onlyNotSynchronized) {

        List<Group> recordsList = new ArrayList<Group>();

        try {
            String sql = "";
            sql += "SELECT * FROM " + GROUPS_TABLE_NAME;
            sql += " WHERE " + KIGA_EXTERNALID_COLUMN + "= ?";

            if(onlyNotSynchronized)
                sql += " AND " + SYNCHRONIZATION_ACTION_COLUMN + " != '0'";

            sql += " ORDER BY " + GROUPS_TABLE_ID_COLUMN + " ASC";

            String[] selectionArgs = { kigaId };

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, selectionArgs);

            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                    recordsList.add(getGroupFromCursor(cursor));
                    cursor.moveToNext();
            }

            cursor.close();
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return recordsList;
    }

    public Group getGroup(long id, String externalId) {

        Group group = null;

        try {
            String where = null;
            if(id > 0)
                where = " WHERE "+ GROUPS_TABLE_ID_COLUMN + " = '"+String.valueOf(id)+"'";

            if(externalId != null){
                if(where == null)
                    where = " WHERE ";
                else
                    where += " AND ";

                where += EXTERNALID_COLUMN + " = '"+externalId+"'";
            }

            String sql = "";
            sql += "SELECT * FROM " + GROUPS_TABLE_NAME;
            sql += where;

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
            {
                group = getGroupFromCursor(cursor);
            }
            cursor.close();
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return group;
    }

    public Group getActiveGroup(String kigaId) {

        Group group = null;

        try {
            String sql = "";
            sql += "SELECT * FROM " + GROUPS_TABLE_NAME;
            sql += " WHERE " + KIGA_EXTERNALID_COLUMN + "= ?";
            sql += " AND " + GROUPS_TABLE_ISACTIVE_COLUMN + " = '1'";

            String[] selectionArgs = { kigaId };

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, selectionArgs);

            if (cursor.moveToFirst())
            {
                group = getGroupFromCursor(cursor);
            }
            cursor.close();
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return group;
    }

    private Group getGroupFromCursor(Cursor cursor){
        long groupId = Long.parseLong(cursor.getString(cursor.getColumnIndex(GROUPS_TABLE_ID_COLUMN)));
        String name = cursor.getString(cursor.getColumnIndex(GROUPS_TABLE_NAME_COLUMN));
        int color = cursor.getInt(cursor.getColumnIndex(GROUPS_TABLE_COLOR_COLUMN));
        Integer isActive = cursor.getInt(cursor.getColumnIndex(GROUPS_TABLE_ISACTIVE_COLUMN));

        String kigaExternalId = cursor.getString(cursor.getColumnIndex(KIGA_EXTERNALID_COLUMN));
        Integer synchronizationAction = cursor.getInt(cursor.getColumnIndex(SYNCHRONIZATION_ACTION_COLUMN));

        String externalId = cursor.getString(cursor.getColumnIndex(EXTERNALID_COLUMN));

        return new Group(kigaExternalId, synchronizationAction, groupId, externalId, name, color, isActive);
    }

    public long addGroup(String kigaId, String name, int color){
        return addGroup(kigaId, null, name, color, true);
    }

    public long addGroup(String kigaId, String externalId, String name, int color, Boolean setActionToNew) {
        long groupId = 0;
        try
        {
            ContentValues values = new ContentValues();

            values.put(GROUPS_TABLE_NAME_COLUMN, name);
            values.put(GROUPS_TABLE_COLOR_COLUMN, color);
            values.put(GROUPS_TABLE_ISACTIVE_COLUMN, 0);

            values.put(KIGA_EXTERNALID_COLUMN, kigaId);
            values.put(EXTERNALID_COLUMN, externalId);

            if(setActionToNew)
                values.put(SYNCHRONIZATION_ACTION_COLUMN, SyncAction.Add.getCode());
            else
                values.put(SYNCHRONIZATION_ACTION_COLUMN, SyncAction.None.getCode());

            SQLiteDatabase db = this.getWritableDatabase();

            groupId = db.insert(GROUPS_TABLE_NAME, null, values);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return groupId;
    }

    public void updateGroup(long id, String name, int color){
        updateGroup(id, null, name, color, true);
    }

    public void updateGroup(long id, String externalId, String name, int color, Boolean setActionToChange){
        try
        {
            Group group = getGroup(id, externalId);

            ContentValues values = new ContentValues();

            values.put(GROUPS_TABLE_NAME_COLUMN, name);
            values.put(GROUPS_TABLE_COLOR_COLUMN, color);

            if(setActionToChange){
                if(group.getSyncAction() == SyncAction.None.getCode())
                    values.put(SYNCHRONIZATION_ACTION_COLUMN, SyncAction.Change.getCode());
            }else
                values.put(SYNCHRONIZATION_ACTION_COLUMN, SyncAction.None.getCode());

            String where;
            String[] whereArgs;

            if(id > 0){
                where = TEACHERS_TABLE_ID_COLUMN + " = ?";
                whereArgs = new String[]{ Long.toString(id) };
            }else{
                where = EXTERNALID_COLUMN + " = ?";
                whereArgs = new String[]{ externalId };
            }

            SQLiteDatabase db = this.getWritableDatabase();

            db.update(GROUPS_TABLE_NAME, values, where, whereArgs);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void syncGroup(String kigaId, SyncGroupResponse response){
        try
        {
            Group group = getGroup(0, response.id);

            if(group == null){
                addGroup(kigaId, response.id, response.name, Integer.valueOf(response.color), false);
            }else{
                updateGroup(0, response.id, response.name, Integer.valueOf(response.color), false);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void activateGroup(String kigaId, Long id){
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(GROUPS_TABLE_ISACTIVE_COLUMN, "0");
            String where = GROUPS_TABLE_ISACTIVE_COLUMN + " = '1' AND "+ KIGA_EXTERNALID_COLUMN + " = ?";

            String[] whereArgs1 = { kigaId };

            // deactivate old active group
            db.update(GROUPS_TABLE_NAME, values, where, whereArgs1);

            values.clear();

            values.put(GROUPS_TABLE_ISACTIVE_COLUMN, "1");
            where = GROUPS_TABLE_ID_COLUMN + " = ? AND "+ KIGA_EXTERNALID_COLUMN + " = ?";
            String[] whereArgs2 = { Long.toString(id), kigaId };

            // activate group
            db.update(GROUPS_TABLE_NAME, values, where, whereArgs2);


            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Teacher> readTeachers(String kigaId, Boolean onlyNotSynchronized) {

        List<Teacher> recordsList = new ArrayList<Teacher>();

        try {
            String sql = "";
            sql += " SELECT *";
            sql += " FROM " + TEACHERS_TABLE_NAME;
            sql += " WHERE " + KIGA_EXTERNALID_COLUMN + " = ?";

            if(onlyNotSynchronized)
                sql += " AND " + SYNCHRONIZATION_ACTION_COLUMN + " != '0'";


            sql += " ORDER BY " + TEACHERS_TABLE_ID_COLUMN + " DESC";

            SQLiteDatabase db = this.getReadableDatabase();

            String[] selectionArgs = { kigaId };

            Cursor cursor = db.rawQuery(sql, selectionArgs);

            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                recordsList.add(GetTeacherFromCursor(cursor));
                cursor.moveToNext();
            }

            cursor.close();
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return recordsList;
    }

    public Teacher getTeacher(long id, String externalId) {

        Teacher teacher = null;

        String where = null;

        if(id > 0)
            where = " WHERE "+ TEACHERS_TABLE_ID_COLUMN + " = '"+String.valueOf(id)+"'";

        if(externalId != null){
            if(where == null)
                where = " WHERE ";
            else
                where += " AND ";

            where += EXTERNALID_COLUMN + " = '"+externalId+"'";
        }

        try {
            String sql = "";
            sql += " SELECT *";
            sql += " FROM " + TEACHERS_TABLE_NAME;
            sql += where;

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
            {
                teacher = GetTeacherFromCursor(cursor);
            }
            cursor.close();
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return teacher;
    }

    public Teacher getActiveTeacher(String kigaId) {

        Teacher teacher = null;

        try {
            String sql = "";
            sql += " SELECT *";
            sql += " FROM " + TEACHERS_TABLE_NAME;
            sql += " WHERE " + KIGA_EXTERNALID_COLUMN + " = ?";
            sql += " AND " + TEACHERS_TABLE_ISACTIVE_COLUMN + " =  '1'";

            String[] selectionArgs = { kigaId };

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, selectionArgs);

            if (cursor.moveToFirst())
            {
                teacher = GetTeacherFromCursor(cursor);
            }
            cursor.close();
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return teacher;
    }

    private Teacher GetTeacherFromCursor(Cursor cursor){
        long teacherId = Long.parseLong(cursor.getString(cursor.getColumnIndex(TEACHERS_TABLE_ID_COLUMN)));
        String name = cursor.getString(cursor.getColumnIndex(TEACHERS_TABLE_NAME_COLUMN));
        Integer isActive = cursor.getInt(cursor.getColumnIndex(TEACHERS_TABLE_ISACTIVE_COLUMN));

        String kigaExternalId = cursor.getString(cursor.getColumnIndex(KIGA_EXTERNALID_COLUMN));
        Integer synchronizationAction = cursor.getInt(cursor.getColumnIndex(SYNCHRONIZATION_ACTION_COLUMN));

        long imageId  = cursor.getLong(cursor.getColumnIndex(MEDIA_ID_COLUMN));

        Media image = null;
        if(imageId >= 0)
            image = getMedia(imageId);

        String externalId = cursor.getString(cursor.getColumnIndex(EXTERNALID_COLUMN));

        return new Teacher(kigaExternalId, synchronizationAction, teacherId, externalId, name, image, isActive);
    }

    public long addTeacher(String kigaId, String name, byte[] image){
        return addTeacher(kigaId, null, name, null, image, true);
    }

    public long addTeacher(String kigaId, String externalId, String name, String imageExternalId, byte[] image, Boolean setActionToNew){
        long id = 0;
        try {
            ContentValues values = new ContentValues();

            long mediaId = 0;
            if(image != null && image.length > 0){
                mediaId = addMedia(kigaId, imageExternalId, MediaType.image.toString(), "",image, null);
            }

            values.put(TEACHERS_TABLE_NAME_COLUMN, name);
            values.put(MEDIA_ID_COLUMN, mediaId);
            values.put(TEACHERS_TABLE_ISACTIVE_COLUMN, 0);

            values.put(KIGA_EXTERNALID_COLUMN, kigaId);
            values.put(EXTERNALID_COLUMN, externalId);

            if(setActionToNew)
                values.put(SYNCHRONIZATION_ACTION_COLUMN, SyncAction.Add.getCode());
            else
                values.put(SYNCHRONIZATION_ACTION_COLUMN, SyncAction.None.getCode());

            SQLiteDatabase db = this.getWritableDatabase();

            id = db.insert(TEACHERS_TABLE_NAME, null, values);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return id;
    }

    public void updateTeacher(long id, String name, byte[] image){
        updateTeacher(id, null, name, null, image, true);
    }


    public void updateTeacher(long id, String externalId, String name, String imageExternalId, byte[] image, Boolean setActionToChange){
        try
        {
            Teacher teacher = getTeacher(id, externalId);

            ContentValues values = new ContentValues();

            Media currentImage = teacher.getImage();

            if(image != null){
                if(currentImage != null)
                {
                    updateMedia(currentImage.getId(), imageExternalId, "", image);
                }else{
                    long mediaId = addMedia(teacher.getKigaExternalId(), imageExternalId, MediaType.image.toString(), "",image, null);
                    values.put(MEDIA_ID_COLUMN, mediaId);
                }
            }

            values.put(TEACHERS_TABLE_NAME_COLUMN, name);

            if(setActionToChange){
                if(teacher.getSyncAction() == SyncAction.None.getCode())
                    values.put(SYNCHRONIZATION_ACTION_COLUMN, SyncAction.Change.getCode());
            }else
                values.put(SYNCHRONIZATION_ACTION_COLUMN, SyncAction.None.getCode());

            String where;
            String[] whereArgs;

            if(id > 0){
                where = TEACHERS_TABLE_ID_COLUMN + " = ?";
                whereArgs = new String[]{ Long.toString(id) };
            }else{
                where = EXTERNALID_COLUMN + " = ?";
                whereArgs = new String[]{ externalId };
            }

            SQLiteDatabase db = this.getWritableDatabase();
            db.update(TEACHERS_TABLE_NAME, values, where, whereArgs);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public long syncTeacher(String kigaId, SyncTeacherResponse response){
        long teacherId = 0;

        try
        {
            Teacher teacher = getTeacher(0, response.id);

            /*String imageId = null;
            byte[] image = null;
            if(response.image != null)
            {
                try{
                    imageId = response.image.id;
              2      image = ImageHelper.FromBase64(response.image.data);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } */

            if(teacher == null){
                teacherId = addTeacher(kigaId, response.id, response.name, null, null, false);
            }else{
                updateTeacher(0, response.id, response.name, null, null, false);
                teacherId = teacher.getId();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return teacherId;
    }

    public void activateTeacher(String kigaId, long id){
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(TEACHERS_TABLE_ISACTIVE_COLUMN, "0");
            String where = TEACHERS_TABLE_ISACTIVE_COLUMN + " = '1' AND "+ KIGA_EXTERNALID_COLUMN + " = ?";

            String[] whereArgs1 = { kigaId };
            // deactivate old active teacher
            db.update(TEACHERS_TABLE_NAME, values, where, whereArgs1);

            values.clear();

            values.put(TEACHERS_TABLE_ISACTIVE_COLUMN, "1");
            where = TEACHERS_TABLE_ID_COLUMN + " = ? AND "+ KIGA_EXTERNALID_COLUMN + " = ?";

            String[] whereArgs2 = { Long.toString(id), kigaId };

            // activate group
            db.update(TEACHERS_TABLE_NAME, values, where, whereArgs2);

            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*public List<Kid> readKids(String kigaId, long groupId, Boolean onlyNotSynchronized) {
        KidsSearchParams params = new KidsSearchParams();
        params.groupId = groupId;

        return readKids(kigaId, params, onlyNotSynchronized);
    }*/

    public List<Kid> readKids(String kigaId, long groupId, Boolean onlyNotSynchronized) {
        return readKids(kigaId, groupId, null, onlyNotSynchronized);
    }

    private String JoinWithQuotes(List<String> values){
        StringBuilder sb = new StringBuilder();
        int i;
        for(i=0;i<values.size()-1;i++)
            sb.append("'").append(values.get(i)).append("',");
        return sb.toString()+"'"+values.get(i)+"'";
    }

    public List<Kid> readKids(String kigaId, long groupId, List<String> kidsExternalIds, Boolean onlyNotSynchronized) {

        List<Kid> recordsList = new ArrayList<Kid>();

        String where = " WHERE k." + KIGA_EXTERNALID_COLUMN + " = ?";

        if(groupId > 0)
            where += " AND k."+KIDS_TABLE_GROUPID_COLUMN +" = '"+groupId+"'";

        if(onlyNotSynchronized)
            where += " AND k." + SYNCHRONIZATION_ACTION_COLUMN + " != '0' AND g."+EXTERNALID_COLUMN +" IS NOT NULL";

        if(kidsExternalIds != null){
            String ids;
            if(kidsExternalIds.size() > 0){
                ids = JoinWithQuotes(kidsExternalIds);
                where += " AND k."+EXTERNALID_COLUMN + " IN ("+ ids + ")";
            }else{
                where += " AND 1=2"; // false
            }
        }

        String[] whereArgs = { kigaId };

        try {
            String sql = "";
            sql += " SELECT k.*, g.name as groupName, g."+EXTERNALID_COLUMN +" as groupExternalId";
            sql += " FROM " + KIDS_TABLE_NAME + " as k";
            sql += " LEFT JOIN " + GROUPS_TABLE_NAME + " as g ON k."+ KIDS_TABLE_GROUPID_COLUMN + "="+"g."+GROUPS_TABLE_ID_COLUMN;
            sql += where;
            sql += " ORDER BY k." + KIDS_TABLE_NAME_COLUMN + " DESC";

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, whereArgs);

            cursor.moveToFirst();
            while (!cursor.isAfterLast())
            {
                recordsList.add(GetKidFromCursor(cursor));
                cursor.moveToNext();
            }

            cursor.close();
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return recordsList;
    }

    public Kid getKid(long id, String externalId) {

        Kid kid = null;

        try {
            String where = null;

            if(id > 0)
                where = " WHERE k."+ KIDS_TABLE_ID_COLUMN + " = '"+String.valueOf(id)+"'";

            if(externalId != null){
                if(where == null)
                    where = " WHERE ";
                else
                    where += " AND ";

                where += "k."+EXTERNALID_COLUMN + " = '"+externalId+"'";
            }

            String sql = "";
            sql += " SELECT k.*, g.name as groupName, g."+EXTERNALID_COLUMN +" as groupExternalId";
            sql += " FROM " + KIDS_TABLE_NAME + " as k";
            sql += " LEFT JOIN " + GROUPS_TABLE_NAME + " as g ON k."+ KIDS_TABLE_GROUPID_COLUMN + "="+"g."+GROUPS_TABLE_ID_COLUMN;
            sql += where;

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
            {
                kid = GetKidFromCursor(cursor);
            }
            cursor.close();
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return kid;
    }

    private Kid GetKidFromCursor(Cursor cursor){
        int kidId = Integer.parseInt(cursor.getString(cursor.getColumnIndex(KIDS_TABLE_ID_COLUMN)));
        String name = cursor.getString(cursor.getColumnIndex(KIDS_TABLE_NAME_COLUMN));
        Integer kidGroupId = cursor.getInt(cursor.getColumnIndex(KIDS_TABLE_GROUPID_COLUMN));
        Long birthday = cursor.getLong(cursor.getColumnIndex(KIDS_TABLE_BIRTHDAY_COLUMN));
        Integer hasListeners = cursor.getInt(cursor.getColumnIndex(KIDS_TABLE_HASLISTENERS_COLUMN));

        String uniqueId = cursor.getString(cursor.getColumnIndex(KIDS_TABLE_UNIQUEID_COLUMN));

        String groupName = cursor.getString(cursor.getColumnIndex("groupName"));
        String groupExternalId = cursor.getString(cursor.getColumnIndex("groupExternalId"));

        String kigaExternalId = cursor.getString(cursor.getColumnIndex(KIGA_EXTERNALID_COLUMN));
        Integer synchronizationAction = cursor.getInt(cursor.getColumnIndex(SYNCHRONIZATION_ACTION_COLUMN));

        long imageId  = cursor.getLong(cursor.getColumnIndex(MEDIA_ID_COLUMN));

        Media image = null;
        if(imageId >= 0)
            image = getMedia(imageId);

        String externalId = cursor.getString(cursor.getColumnIndex(EXTERNALID_COLUMN));

        return new Kid(kigaExternalId, synchronizationAction, kidId, externalId, kidGroupId, groupName, groupExternalId, name, image, birthday, hasListeners, uniqueId);
    }

    public long addKid(String kigaId, String name, Long groupId, long birthday, byte[] image){
        return addKid(kigaId, null, name, groupId, birthday, 0, null, image, true);
    }

    public long addKid(String kigaId, String externalId, String name, long groupId, long birthday, Integer hasListeners, String imageExternalId, byte[] image, Boolean setActionToNew){
        long kidId = 0;
        try {
            ContentValues values = new ContentValues();

            long mediaId = 0;
            if(image != null && image.length > 0){
                mediaId = addMedia(kigaId, imageExternalId, MediaType.image.toString(), "",image, null);
            }

            values.put(KIDS_TABLE_NAME_COLUMN, name);
            values.put(KIDS_TABLE_GROUPID_COLUMN, groupId);
            values.put(KIDS_TABLE_BIRTHDAY_COLUMN, birthday);
            values.put(KIDS_TABLE_HASLISTENERS_COLUMN, hasListeners);
            values.put(MEDIA_ID_COLUMN, mediaId);
            values.put(EXTERNALID_COLUMN, externalId);

            values.put(KIGA_EXTERNALID_COLUMN, kigaId);

            if(setActionToNew)
                values.put(SYNCHRONIZATION_ACTION_COLUMN, SyncAction.Add.getCode());
            else
                values.put(SYNCHRONIZATION_ACTION_COLUMN, SyncAction.None.getCode());

            SQLiteDatabase db = this.getWritableDatabase();

            kidId = db.insert(KIDS_TABLE_NAME, null, values);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return kidId;
    }

    public void updateKidCodeAndHasListeners(String externalId, String code, Integer hasListeners){
        try
        {
            ContentValues values = new ContentValues();

            values.put(KIDS_TABLE_UNIQUEID_COLUMN, code);
            values.put(KIDS_TABLE_HASLISTENERS_COLUMN, hasListeners);

            String where = EXTERNALID_COLUMN + " = ?";

            String[] whereArgs = { externalId };

            SQLiteDatabase db = this.getWritableDatabase();

            db.update(KIDS_TABLE_NAME, values, where, whereArgs);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateKid(long id, String name, long groupId, long birthday, byte[] image){
        updateKid(id, null, name, groupId, birthday, null, image, true);
    }

    public void updateKid(long id, String externalId, String name, long groupId, long birthday, String imageExternalId, byte[] image, Boolean setActionToChange){
        try
        {
            Kid kid = getKid(id, externalId);

            ContentValues values = new ContentValues();

            Media currentImage = kid.getImage();
            if(image != null){
                if(currentImage != null)
                {
                    updateMedia(currentImage.getId(), imageExternalId, "", image);
                }else{
                    long mediaId = addMedia(kid.getKigaExternalId(), imageExternalId, MediaType.image.toString(), "",image, null);
                    values.put(MEDIA_ID_COLUMN, mediaId);
                }
            }

            values.put(KIDS_TABLE_NAME_COLUMN, name);
            values.put(KIDS_TABLE_GROUPID_COLUMN, groupId);
            values.put(KIDS_TABLE_BIRTHDAY_COLUMN, birthday);

            if(setActionToChange){
                if(kid.getSyncAction() == SyncAction.None.getCode())
                    values.put(SYNCHRONIZATION_ACTION_COLUMN, SyncAction.Change.getCode());
            }else
                values.put(SYNCHRONIZATION_ACTION_COLUMN, SyncAction.None.getCode());

            String where;
            String[] whereArgs;

            if(id > 0){
                where = TEACHERS_TABLE_ID_COLUMN + " = ?";
                whereArgs = new String[]{ Long.toString(id) };
            }else{
                where = EXTERNALID_COLUMN + " = ?";
                whereArgs = new String[]{ externalId };
            }

            SQLiteDatabase db = this.getWritableDatabase();

            db.update(KIDS_TABLE_NAME, values, where, whereArgs);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public long syncKid(String kigaId, SyncKidResponse response){
        long kidId = 0;
        try
        {
            Kid kid = getKid(0, response.id);

            /*String imageId = null;
            byte[] image = null;
            if(response.image != null)
            {
                try{
                    imageId = response.image.id;
                    image = ImageHelper.FromBase64(response.image.data);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }*/

            long groupId = 0;
            if(response.group_id != null){
                Group group = getGroup(0, response.group_id);
                groupId = group.getId();
            }

            long dob = 0;
            if(response.dob != null)
                dob = Long.valueOf(response.dob);

            Boolean hasListeners = Boolean.valueOf(response.hasListeners);
            Integer hasListenersInt = 0;
            if(hasListeners)
                hasListenersInt = 1;

            if(kid == null){
                kidId = addKid(kigaId, response.id, response.name, groupId, dob, hasListenersInt, null, null, false);
            }else{
                updateKid(0, response.id, response.name, groupId, dob, null, null, false);
                kidId = kid.getId();
            }

            updateKidCodeAndHasListeners(response.id, response.code, hasListenersInt);
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return kidId;
    }

    public List<KidContact> readKidContacts(long kidId) {

        List<KidContact> recordsList = new ArrayList<KidContact>();

        try {
            String sql = "";

            sql += "SELECT kc.*, k."+EXTERNALID_COLUMN+" as kidExternalId";
            sql += " FROM " + KID_CONTACTS_TABLE_NAME + " as kc";
            sql += " LEFT JOIN " + KIDS_TABLE_NAME + " as k ON k."+ KIDS_TABLE_ID_COLUMN + "="+"kc."+KID_CONTACTS_TABLE_KIDID_COLUMN;
            sql += " WHERE kc."+KID_CONTACTS_TABLE_KIDID_COLUMN +" = ?";
            sql += " ORDER BY kc." + KID_CONTACTS_TABLE_ID_COLUMN + " ASC";

            String[] whereArgs = { String.valueOf(kidId) };

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, whereArgs);

            cursor.moveToFirst();
            while (!cursor.isAfterLast())
            {
                recordsList.add(GetKidContactFromCursor(cursor));
                cursor.moveToNext();
            }

            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return recordsList;
    }

    public List<KidContact> readKidContactsForSync(String kigaId) {

        List<KidContact> recordsList = new ArrayList<KidContact>();

        try {
            String sql = "";

            sql += "SELECT kc.*, k."+EXTERNALID_COLUMN+" as kidExternalId";
            sql += " FROM " + KID_CONTACTS_TABLE_NAME + " as kc";
            sql += " LEFT JOIN " + KIDS_TABLE_NAME + " as k ON k."+ KIDS_TABLE_ID_COLUMN + "="+"kc."+KID_CONTACTS_TABLE_KIDID_COLUMN;
            sql += " WHERE k."+KIGA_EXTERNALID_COLUMN +" = ?";
            sql += " AND kc." + SYNCHRONIZATION_ACTION_COLUMN + " != '0'";
            sql += " AND k." + SYNCHRONIZATION_ACTION_COLUMN + " = '0'";
            sql += " ORDER BY kc." + KID_CONTACTS_TABLE_ID_COLUMN + " ASC";

            String[] whereArgs = { kigaId };

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, whereArgs);

            cursor.moveToFirst();
            while (!cursor.isAfterLast())
            {
                recordsList.add(GetKidContactFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return recordsList;
    }

    public KidContact getKidContact(int id, String externalId) {

        KidContact contact = null;

        try {
            String sql = "";

            String where = null;

            if(id > 0)
                where = " WHERE kc." + KID_CONTACTS_TABLE_ID_COLUMN + " = '"+ String.valueOf(id) +"'";

            if(externalId != null){
                if(where == null)
                    where = " WHERE ";
                else
                    where += " AND ";

                where += "kc."+EXTERNALID_COLUMN + " = '"+externalId+"'";
            }

            sql += "SELECT kc.*, k."+EXTERNALID_COLUMN+" as kidExternalId";
            sql += " FROM " + KID_CONTACTS_TABLE_NAME + " as kc";
            sql += " LEFT JOIN " + KIDS_TABLE_NAME + " as k ON k."+ KIDS_TABLE_ID_COLUMN + "="+"kc."+KID_CONTACTS_TABLE_KIDID_COLUMN;
            sql += where;

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
            {
                contact = GetKidContactFromCursor(cursor);
            }
            cursor.close();
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return contact;
    }

    private KidContact GetKidContactFromCursor(Cursor cursor){
        int id = Integer.parseInt(cursor.getString(cursor.getColumnIndex(KID_CONTACTS_TABLE_ID_COLUMN)));
        int kidId = Integer.parseInt(cursor.getString(cursor.getColumnIndex(KID_CONTACTS_TABLE_KIDID_COLUMN)));
        String name = cursor.getString(cursor.getColumnIndex(KID_CONTACTS_TABLE_NAME_COLUMN));
        String phone = cursor.getString(cursor.getColumnIndex(KID_CONTACTS_TABLE_PHONE_COLUMN));
        String email = cursor.getString(cursor.getColumnIndex(KID_CONTACTS_TABLE_EMAIL_COLUMN));
        Integer relation = cursor.getInt(cursor.getColumnIndex(KID_CONTACTS_TABLE_RELATION_COLUMN));
        Integer isInvited = cursor.getInt(cursor.getColumnIndex(KID_CONTACTS_TABLE_IS_INVITED_COLUMN));

        String kigaExternalId = cursor.getString(cursor.getColumnIndex(KIGA_EXTERNALID_COLUMN));
        Integer synchronizationAction = cursor.getInt(cursor.getColumnIndex(SYNCHRONIZATION_ACTION_COLUMN));
        String externalId = cursor.getString(cursor.getColumnIndex(EXTERNALID_COLUMN));
        String kidExternalId = cursor.getString(cursor.getColumnIndex("kidExternalId"));

        KidContact contact = new KidContact(kigaExternalId, synchronizationAction, id, externalId, kidId, name,phone,email,relation,(isInvited == 1));
        contact.setKidExternalId(kidExternalId);

        return contact;
    }

    public void addKidContact(String kigaExternalId, long kidId, String name, String phone, String email, Integer relation){
        addKidContact(kigaExternalId, kidId, null, name, phone, email, relation, true);
    }

    public void addKidContact(String kigaExternalId, long kidId, String externalId, String name, String phone, String email, Integer relation, boolean setActionToNew){
        try {
            ContentValues values = new ContentValues();

            values.put(KIGA_EXTERNALID_COLUMN, kigaExternalId);
            values.put(KID_CONTACTS_TABLE_NAME_COLUMN, name);
            values.put(KID_CONTACTS_TABLE_KIDID_COLUMN, kidId);
            values.put(KID_CONTACTS_TABLE_PHONE_COLUMN, phone);
            values.put(KID_CONTACTS_TABLE_EMAIL_COLUMN, email);
            values.put(KID_CONTACTS_TABLE_RELATION_COLUMN, relation);
            values.put(KID_CONTACTS_TABLE_IS_INVITED_COLUMN, 0);

            values.put(EXTERNALID_COLUMN, externalId);

            if(setActionToNew)
                values.put(SYNCHRONIZATION_ACTION_COLUMN, SyncAction.Add.getCode());
            else
                values.put(SYNCHRONIZATION_ACTION_COLUMN, SyncAction.None.getCode());

            SQLiteDatabase db = this.getWritableDatabase();

            db.insert(KID_CONTACTS_TABLE_NAME, null, values);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateKidContact(int id, String name, String phone, String email, Integer relation)
    {
        updateKidContact(id, null, name, phone, email, relation, true);
    }
    public void updateKidContact(int id, String externalId, String name, String phone, String email, Integer relation, boolean setActionToChange){
        try
        {
            KidContact contact = getKidContact(id, externalId);

            ContentValues values = new ContentValues();

            values.put(KID_CONTACTS_TABLE_NAME_COLUMN, name);
            values.put(KID_CONTACTS_TABLE_PHONE_COLUMN, phone);
            values.put(KID_CONTACTS_TABLE_EMAIL_COLUMN, email);
            values.put(KID_CONTACTS_TABLE_RELATION_COLUMN, relation);

            if(setActionToChange){
                if(contact.getSyncAction() == SyncAction.None.getCode())
                    values.put(SYNCHRONIZATION_ACTION_COLUMN, SyncAction.Change.getCode());
            }else
                values.put(SYNCHRONIZATION_ACTION_COLUMN, SyncAction.None.getCode());

            String where;
            String[] whereArgs;

            if(id > 0){
                where = KID_CONTACTS_TABLE_ID_COLUMN + " = ?";
                whereArgs = new String[]{ Long.toString(id) };
            }else{
                where = EXTERNALID_COLUMN + " = ?";
                whereArgs = new String[]{ externalId };
            }

            SQLiteDatabase db = this.getWritableDatabase();

            db.update(KID_CONTACTS_TABLE_NAME, values, where, whereArgs);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean deleteKidContact(String id) {
        boolean deleteSuccessful = false;

        try {

            SQLiteDatabase db = this.getWritableDatabase();
            deleteSuccessful = db.delete(KID_CONTACTS_TABLE_NAME, "id ='" + id + "'", null) > 0;
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return deleteSuccessful;
    }

    public void syncKidContact(String kigaId, String kidId, SyncKidContactResponse response){
        try
        {
            KidContact kidContact = getKidContact(0, response.id);
            Kid kid = getKid(0, kidId);

            int relation = DCCHelper.GetRelation(response.relation);

            if(kidContact == null){
                addKidContact(kigaId, kid.getId(), response.id, response.name, response.phone, response.email, relation, false);
            }else{
                updateKidContact(0, response.id, response.name, response.phone, response.email, relation, false);
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<EventsSearchResult> readEventsSearchResults(EventsSearchParams params) {
        List<EventsSearchResult> recordsList = new ArrayList<EventsSearchResult>();

        try
        {
            Date now = new Date();
            String sql = "";
            sql += " SELECT e."+EVENTS_TABLE_ID_COLUMN+" as eventId, " +
                            "ek."+EVENT_TO_KIDS_TABLE_KIDID_COLUMN+" as kidId, " +
                            "e."+EVENTS_TABLE_TYPE_COLUMN+" as type, " +
                            "e."+EVENTS_TABLE_CREATEDON_COLUMN+" as createdOn"
                 + " FROM "+EVENTS_TABLE_NAME+" as e"
                 + " JOIN "+EVENT_TO_KIDS_TABLE_NAME+" as ek ON ek."+EVENT_TO_KIDS_TABLE_EVENTID_COLUMN+" = e."+EVENTS_TABLE_ID_COLUMN
                 + " WHERE ek."+EVENT_TO_KIDS_TABLE_KIDID_COLUMN+" IN ("+UIHelper.stringJoin(params.kidIds,",")+")"
                 + " AND e."+EVENTS_TABLE_CREATEDON_COLUMN+" BETWEEN "+UIHelper.getStartOfDay(now)+" AND "+UIHelper.getEndOfDay(now);

            if(params.type == 1){
                sql += " AND e."+EVENTS_TABLE_TYPE_COLUMN+" IN ('"+ActionType.Signin+"','"+ActionType.Signout+"')";
            }

            sql += " ORDER BY e."+EVENTS_TABLE_CREATEDON_COLUMN+" DESC";

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, null);

            cursor.moveToFirst();
            while (!cursor.isAfterLast())
            {
                long eventId = Long.parseLong(cursor.getString(cursor.getColumnIndex("eventId")));
                long kidId = Long.parseLong(cursor.getString(cursor.getColumnIndex("kidId")));
                String type = cursor.getString(cursor.getColumnIndex("type"));
                long createdOn = cursor.getLong(cursor.getColumnIndex("createdOn"));

                EventsSearchResult result = new EventsSearchResult();
                result.EventId = eventId;
                result.KidId = kidId;
                result.Type = type;
                result.CreatedOn = createdOn;

                recordsList.add(result);

                cursor.moveToNext();
            }

            cursor.close();
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return recordsList;
    }

    public List<Event> readEventsForSync(String kigaId) {

        List<Event> recordsList = new ArrayList<Event>();

        String[] whereArgs = { kigaId };

        try {
            String sql = "";
            sql += " SELECT e.*, t."+EXTERNALID_COLUMN + " as teacherExternalId";
            sql += " FROM " + EVENTS_TABLE_NAME + " as e";
            sql += " JOIN " + TEACHERS_TABLE_NAME + " as t ON e."+EVENTS_TABLE_TEACHERID_COLUMN + " = t."+TEACHERS_TABLE_ID_COLUMN;
            //sql += " JOIN " + EVENT_TO_KIDS_TABLE_NAME + " as ek ON e."+EVENTS_TABLE_ID_COLUMN + " = ek."+EVENT_TO_KIDS_TABLE_EVENTID_COLUMN;
            //sql += " JOIN " + KIDS_TABLE_NAME + " as k on k."+KIDS_TABLE_ID_COLUMN + " = ek."+EVENT_TO_KIDS_TABLE_KIDID_COLUMN;
            sql += " WHERE e." + SYNCHRONIZATION_ACTION_COLUMN + " != '0'";
            sql += " AND t."+EXTERNALID_COLUMN + " IS NOT NULL";
            sql += " AND e."+KIGA_EXTERNALID_COLUMN +" = ?";

            sql += " AND NOT EXISTS( SELECT k."+EXTERNALID_COLUMN;
            sql += " FROM "+KIDS_TABLE_NAME+" as k";
            sql += " JOIN "+EVENT_TO_KIDS_TABLE_NAME + " as ek ON e."+EVENTS_TABLE_ID_COLUMN + " = ek."+EVENT_TO_KIDS_TABLE_EVENTID_COLUMN;
            sql += " AND k."+KIDS_TABLE_ID_COLUMN + " = ek."+EVENT_TO_KIDS_TABLE_KIDID_COLUMN;
            sql += " WHERE k."+EXTERNALID_COLUMN + " IS NULL)";

            //sql += " GROUP BY e."+EVENTS_TABLE_ID_COLUMN;


            String sqlKids = "";
            sqlKids += " SELECT k."+EXTERNALID_COLUMN;
            sqlKids += " FROM "+EVENT_TO_KIDS_TABLE_NAME + " as ek";
            sqlKids += " JOIN " + KIDS_TABLE_NAME + " as k on k."+KIDS_TABLE_ID_COLUMN + " = ek."+EVENT_TO_KIDS_TABLE_KIDID_COLUMN;
            sqlKids += " WHERE ek."+EVENT_TO_KIDS_TABLE_EVENTID_COLUMN + " = ?";

            String sqlDetails = "";
            sqlDetails += " SELECT *";
            sqlDetails += " FROM "+EVENT_DETAILS_TABLE_NAME;
            sqlDetails += " WHERE "+EVENT_DETAILS_TABLE_EVENTID_COLUMN + " = ?";

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, whereArgs);

            cursor.moveToFirst();
            while (!cursor.isAfterLast())
            {
                int id = Integer.parseInt(cursor.getString(cursor.getColumnIndex(EVENTS_TABLE_ID_COLUMN)));
                String type = cursor.getString(cursor.getColumnIndex(EVENTS_TABLE_TYPE_COLUMN));
                String teacherExternalId = cursor.getString(cursor.getColumnIndex("teacherExternalId"));
                long createdOn = cursor.getLong(cursor.getColumnIndex(EVENTS_TABLE_CREATEDON_COLUMN));
                String kigaExternalId = cursor.getString(cursor.getColumnIndex(KIGA_EXTERNALID_COLUMN));
                Integer synchronizationAction = cursor.getInt(cursor.getColumnIndex(SYNCHRONIZATION_ACTION_COLUMN));
                String externalId = cursor.getString(cursor.getColumnIndex(EXTERNALID_COLUMN));

                ArrayList<String> kidsExternalIds = new ArrayList<String>();
                // kids
                String[] kidsWhereArgs = { String.valueOf(id) };
                Cursor kidsCursor = db.rawQuery(sqlKids, kidsWhereArgs);
                kidsCursor.moveToFirst();
                while (!kidsCursor.isAfterLast())
                {
                    String kidExternalId = kidsCursor.getString(kidsCursor.getColumnIndex(EXTERNALID_COLUMN));
                    kidsExternalIds.add(kidExternalId);

                    kidsCursor.moveToNext();
                }
                kidsCursor.close();

                ArrayList<EventDetail> eventDetails = new ArrayList<EventDetail>();

                //event details
                String[] eventDetailsWhereArgs = { String.valueOf(id) };
                Cursor eventDetailsCursor = db.rawQuery(sqlDetails, eventDetailsWhereArgs);
                eventDetailsCursor.moveToFirst();
                while (!eventDetailsCursor.isAfterLast())
                {
                    String key = eventDetailsCursor.getString(eventDetailsCursor.getColumnIndex(EVENT_DETAILS_TABLE_KEY_COLUMN));
                    String value = eventDetailsCursor.getString(eventDetailsCursor.getColumnIndex(EVENT_DETAILS_TABLE_VALUE_COLUMN));

                    EventDetail detail = new EventDetail(id, key, value);
                    eventDetails.add(detail);
                    eventDetailsCursor.moveToNext();
                }
                eventDetailsCursor.close();

                Event event = new Event(kigaExternalId, synchronizationAction, id,externalId, createdOn, type, teacherExternalId, eventDetails, kidsExternalIds);
                recordsList.add(event);

                cursor.moveToNext();
            }

            cursor.close();
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return recordsList;
    }

    public void addEvent(String kigaId, String type, Long teacherId, List<Integer> kidIds, List<EventDetail> eventDetails){
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();

            Date date = new Date();
            values.put(EVENTS_TABLE_TYPE_COLUMN, type);
            values.put(EVENTS_TABLE_TEACHERID_COLUMN, teacherId);
            values.put(EVENTS_TABLE_CREATEDON_COLUMN, date.getTime());
            values.put(KIGA_EXTERNALID_COLUMN, kigaId);

            values.put(SYNCHRONIZATION_ACTION_COLUMN, SyncAction.Add.getCode());

            long eventId = db.insert(EVENTS_TABLE_NAME, null, values);

            values.clear();

            for(int kidId:kidIds){
                values.put(EVENT_TO_KIDS_TABLE_EVENTID_COLUMN, eventId);
                values.put(EVENT_TO_KIDS_TABLE_KIDID_COLUMN, kidId);

                db.insert(EVENT_TO_KIDS_TABLE_NAME, null, values);
            }

            for(EventDetail detail:eventDetails){
                values.clear();
                values.put(EVENT_DETAILS_TABLE_EVENTID_COLUMN, eventId);
                values.put(EVENT_DETAILS_TABLE_KEY_COLUMN, detail.getKey());
                values.put(EVENT_DETAILS_TABLE_VALUE_COLUMN, detail.getValue());

                db.insert(EVENT_DETAILS_TABLE_NAME, null, values);
            }

            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public long addKigaInfo(String externalId, String name, String address, String phone, String email, String passwordHash, boolean autoLogin, String status, String contactPerson){
        long kigaId = 0;
        try
        {
            deactivateActiveKiga();

            ContentValues values = new ContentValues();

            values.put(KIGA_TABLE_NAME_COLUMN, name);
            values.put(KIGA_TABLE_ADDRESS_COLUMN, address);
            values.put(KIGA_TABLE_PHONE_COLUMN, phone);
            values.put(KIGA_TABLE_EMAIL_COLUMN, email);
            values.put(KIGA_TABLE_PASSWORD_HASH_COLUMN, passwordHash);
            values.put(KIGA_TABLE_IS_AUTOLOGIN_COLUMN, autoLogin ? 1 : 0);
            values.put(KIGA_TABLE_IS_LAST_COLUMN, 1);
            values.put(KIGA_TABLE_STATUS_COLUMN, status);
            values.put(KIGA_TABLE_CONTACT_PERSON_COLUMN, contactPerson);

            values.put(KIGA_EXTERNALID_COLUMN, externalId);
            values.put(SYNCHRONIZATION_ACTION_COLUMN, SyncAction.None.getCode());

            SQLiteDatabase db = this.getWritableDatabase();

            kigaId = db.insert(KIGA_TABLE_NAME, null, values);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return kigaId;
    }

    public void updateKigaInfo(String externalId, String name, String address, String phone, String email, boolean autoLogin, String contactPerson){
        try
        {
            ContentValues values = new ContentValues();

            values.put(KIGA_TABLE_NAME_COLUMN, name);
            values.put(KIGA_TABLE_ADDRESS_COLUMN, address);
            values.put(KIGA_TABLE_PHONE_COLUMN, phone);
            values.put(KIGA_TABLE_EMAIL_COLUMN, email);
            values.put(KIGA_TABLE_CONTACT_PERSON_COLUMN, contactPerson);
            values.put(KIGA_TABLE_IS_AUTOLOGIN_COLUMN, autoLogin ? 1 : 0);

            values.put(SYNCHRONIZATION_ACTION_COLUMN, SyncAction.Change.getCode());

            String where = KIGA_EXTERNALID_COLUMN + " = ?";

            String[] whereArgs = { externalId };

            SQLiteDatabase db = this.getWritableDatabase();

            db.update(KIGA_TABLE_NAME, values, where, whereArgs);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void syncKigaInfo(String externalId, String name, String address, String phone, String email, String contactPerson, String status){
        try
        {
            ContentValues values = new ContentValues();

            values.put(KIGA_TABLE_NAME_COLUMN, name);
            values.put(KIGA_TABLE_ADDRESS_COLUMN, address);
            values.put(KIGA_TABLE_PHONE_COLUMN, phone);
            values.put(KIGA_TABLE_EMAIL_COLUMN, email);
            values.put(KIGA_TABLE_CONTACT_PERSON_COLUMN, contactPerson);
            values.put(KIGA_TABLE_STATUS_COLUMN, status);

            values.put(SYNCHRONIZATION_ACTION_COLUMN, SyncAction.None.getCode());

            String where = KIGA_EXTERNALID_COLUMN + " = ?";

            String[] whereArgs = { externalId };

            SQLiteDatabase db = this.getWritableDatabase();

            db.update(KIGA_TABLE_NAME, values, where, whereArgs);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateKigaPassword(String externalId, String passwordHash){
        try
        {
            ContentValues values = new ContentValues();

            values.put(KIGA_TABLE_PASSWORD_HASH_COLUMN, passwordHash);
            values.put(SYNCHRONIZATION_ACTION_COLUMN, SyncAction.Change.getCode());

            String where = KIGA_EXTERNALID_COLUMN + " = ?";

            String[] whereArgs = { externalId };

            SQLiteDatabase db = this.getWritableDatabase();

            db.update(KIGA_TABLE_NAME, values, where, whereArgs);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateKigaStatus(String externalId, String status){
        try
        {
            ContentValues values = new ContentValues();

            values.put(KIGA_TABLE_STATUS_COLUMN, status);

            String where = KIGA_EXTERNALID_COLUMN + " = ?";

            String[] whereArgs = { externalId };

            SQLiteDatabase db = this.getWritableDatabase();

            db.update(KIGA_TABLE_NAME, values, where, whereArgs);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Kindergarten updateAndGetKindergartenAfterLogin(String externalId, boolean autoLogin){
        Kindergarten kiga = null;
        try
        {
            deactivateActiveKiga();

            ContentValues values = new ContentValues();

            values.put(KIGA_TABLE_IS_AUTOLOGIN_COLUMN, autoLogin ? 1 : 0);
            values.put(KIGA_TABLE_IS_LAST_COLUMN, 1);

            String where = KIGA_EXTERNALID_COLUMN + " = ?";

            String[] whereArgs = { externalId };

            SQLiteDatabase db = this.getWritableDatabase();

            db.update(KIGA_TABLE_NAME, values, where, whereArgs);
            db.close();

            kiga = getKindergarten(externalId);

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return kiga;
    }

    public void deactivateActiveKiga(){
        try
        {
            ContentValues values = new ContentValues();

            values.put(KIGA_TABLE_IS_LAST_COLUMN, 0);
            String where = KIGA_TABLE_IS_LAST_COLUMN + " = 1";

            SQLiteDatabase db = this.getWritableDatabase();

            db.update(KIGA_TABLE_NAME, values, where, null);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Kindergarten getKindergarten(String externalId){
        Kindergarten kindergarten = null;

        try {
            String sql = "";
            sql += "SELECT * FROM " + KIGA_TABLE_NAME;
            sql += " WHERE " + KIGA_EXTERNALID_COLUMN + " = ?";

            String[] whereArgs = { externalId };

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, whereArgs);

            if (cursor.moveToFirst())
            {
                kindergarten = GetKindergartenFromCursor(cursor);
            }
            cursor.close();
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return kindergarten;
    }

    public Kindergarten getKindergarten(String email, String password){
        Kindergarten kindergarten = null;

        String[] keys;
        try {
            String sql = "";
            sql += "SELECT * FROM " + KIGA_TABLE_NAME;
            sql += " WHERE " + KIGA_TABLE_EMAIL_COLUMN + " = ?";

            keys = new String[]{email};

            if(password != null){
                sql += " AND " + KIGA_TABLE_PASSWORD_HASH_COLUMN + " = ?";
                keys = new String[]{email, password};
            }

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, keys);

            if (cursor.moveToFirst())
            {
                kindergarten = GetKindergartenFromCursor(cursor);
            }

            cursor.close();

            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return kindergarten;
    }

    public Kindergarten getActiveKindergarten(){
        Kindergarten kindergarten = null;

        try {
            String sql = "";
            sql += "SELECT * FROM " + KIGA_TABLE_NAME;
            sql += " WHERE " + KIGA_TABLE_IS_LAST_COLUMN + " = 1";

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
            {
                kindergarten = GetKindergartenFromCursor(cursor);
            }
            cursor.close();
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return kindergarten;
    }

    private Kindergarten GetKindergartenFromCursor(Cursor cursor){
        int kigaId = Integer.parseInt(cursor.getString(cursor.getColumnIndex(KIGA_TABLE_ID_COLUMN)));
        String name = cursor.getString(cursor.getColumnIndex(KIGA_TABLE_NAME_COLUMN));
        String address = cursor.getString(cursor.getColumnIndex(KIGA_TABLE_ADDRESS_COLUMN));
        String phone = cursor.getString(cursor.getColumnIndex(KIGA_TABLE_PHONE_COLUMN));
        String email = cursor.getString(cursor.getColumnIndex(KIGA_TABLE_EMAIL_COLUMN));
        Integer isAutoLogin = cursor.getInt(cursor.getColumnIndex(KIGA_TABLE_IS_AUTOLOGIN_COLUMN));
        //Integer isLast = cursor.getInt(cursor.getColumnIndex(KIGA_TABLE_IS_LAST_COLUMN));
        String status = cursor.getString(cursor.getColumnIndex(KIGA_TABLE_STATUS_COLUMN));
        String password = cursor.getString(cursor.getColumnIndex(KIGA_TABLE_PASSWORD_HASH_COLUMN));
        String contactPerson = cursor.getString(cursor.getColumnIndex(KIGA_TABLE_CONTACT_PERSON_COLUMN));

        String kigaExternalId = cursor.getString(cursor.getColumnIndex(KIGA_EXTERNALID_COLUMN));
        Integer synchronizationAction = cursor.getInt(cursor.getColumnIndex(SYNCHRONIZATION_ACTION_COLUMN));

        return new Kindergarten(kigaExternalId, synchronizationAction, kigaId, name, address, phone, email, (isAutoLogin == 1), password, contactPerson, status);
    }

    public void setEntitySynchronized(String tableName, long entityId, String externalId){
        try
        {
            ContentValues values = new ContentValues();

            if(externalId != null)
                values.put(EXTERNALID_COLUMN, externalId);

            values.put(SYNCHRONIZATION_ACTION_COLUMN, SyncAction.None.getCode());

            String where = "_id = ?";

            String[] whereArgs = { Long.toString(entityId) };

            SQLiteDatabase db = this.getWritableDatabase();

            db.update(tableName, values, where, whereArgs);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Media getMedia(long id) {

        Media media = null;

        try {
            String sql = "";
            sql += " SELECT *";
            sql += " FROM " + MEDIA_TABLE_NAME;
            sql += " WHERE " + MEDIA_TABLE_ID_COLUMN + " = ?";

            String[] selectionArgs = { String.valueOf(id) };

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, selectionArgs);

            if (cursor.moveToFirst())
            {
                media = GetMediaFromCursor(cursor);
            }
            cursor.close();
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return media;
    }

    public List<Media> readMediaForSync(String kigaId, boolean onlyNew) {

        ArrayList<Media> medias = new ArrayList<Media>();

        Integer syncAction = onlyNew ? SyncAction.Add.getCode() : SyncAction.Change.getCode();

        try {
            String sql = "";
            sql += " SELECT *";
            sql += " FROM " + MEDIA_TABLE_NAME;
            sql += " WHERE "+KIGA_EXTERNALID_COLUMN +" = ?";
            sql += " AND " + SYNCHRONIZATION_ACTION_COLUMN + " = '"+syncAction+"'";

            String[] selectionArgs = { kigaId };

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, selectionArgs);

            cursor.moveToFirst();
            while (!cursor.isAfterLast())
            {
                Media media = GetMediaFromCursor(cursor);
                medias.add(media);

                cursor.moveToNext();
            }
            cursor.close();
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return medias;
    }

    private Media GetMediaFromCursor(Cursor cursor){

        long id = Long.parseLong(cursor.getString(cursor.getColumnIndex(MEDIA_TABLE_ID_COLUMN)));
        String name = cursor.getString(cursor.getColumnIndex(MEDIA_TABLE_NAME_COLUMN));
        String type = cursor.getString(cursor.getColumnIndex(MEDIA_TABLE_TYPE_COLUMN));
        byte[] data = cursor.getBlob(cursor.getColumnIndex(MEDIA_TABLE_DATA_COLUMN));
        String externalId = cursor.getString(cursor.getColumnIndex(EXTERNALID_COLUMN));
        String filePath = cursor.getString(cursor.getColumnIndex(MEDIA_TABLE_FILEPATH_COLUMN));

        return new Media(id, externalId, type, name, data, filePath);
    }

    public long addMedia(String kigaExternalId, String externalId, String type, String name, byte[] data, String filePath){
        long id = 0;
        try {
            ContentValues values = new ContentValues();

            SQLiteDatabase db = this.getWritableDatabase();

            values.put(KIGA_EXTERNALID_COLUMN, kigaExternalId);
            values.put(EXTERNALID_COLUMN, externalId);
            values.put(MEDIA_TABLE_TYPE_COLUMN, type);
            values.put(MEDIA_TABLE_NAME_COLUMN, name);
            values.put(MEDIA_TABLE_DATA_COLUMN, data);
            values.put(MEDIA_TABLE_FILEPATH_COLUMN, filePath);
            values.put(SYNCHRONIZATION_ACTION_COLUMN, SyncAction.Add.getCode());

            id = db.insert(MEDIA_TABLE_NAME, null, values);

            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return id;
    }
    public void updateMedia(long id, String externalId, String name, byte[] data){
        try
        {
            ContentValues values = new ContentValues();

            values.put(MEDIA_TABLE_NAME_COLUMN, name);
            values.put(MEDIA_TABLE_DATA_COLUMN, data);
            values.put(SYNCHRONIZATION_ACTION_COLUMN, SyncAction.Change.getCode());

            if(externalId != null)
                values.put(EXTERNALID_COLUMN, externalId);

            String where = MEDIA_TABLE_ID_COLUMN + " = ?";
            String[] whereArgs = { Long.toString(id) };

            SQLiteDatabase db = this.getWritableDatabase();

            db.update(MEDIA_TABLE_NAME, values, where, whereArgs);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateMediaId(String tableName, String externalId, long mediaId){
        try
        {
            ContentValues values = new ContentValues();

            values.put(MEDIA_ID_COLUMN, mediaId);


            String where = EXTERNALID_COLUMN + " = ?";
            String[] whereArgs = { externalId };

            SQLiteDatabase db = this.getWritableDatabase();

            db.update(tableName, values, where, whereArgs);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /*public void updateMediaExternalId(long id, String externalId){
        try
        {
            ContentValues values = new ContentValues();

            if(externalId != null){
                values.put(EXTERNALID_COLUMN, externalId);
                values.put(SYNCHRONIZATION_ACTION_COLUMN, SyncAction.None.getCode());
            }

            String where = MEDIA_TABLE_ID_COLUMN + " = ?";
            String[] whereArgs = { Long.toString(id) };

            SQLiteDatabase db = this.getWritableDatabase();

            db.update(MEDIA_TABLE_NAME, values, where, whereArgs);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    public List<Feedback> readFeedbacksForSync(String kigaId) {

        List<Feedback> recordsList = new ArrayList<Feedback>();

        try {
            String sql = "";

            sql += "SELECT *";
            sql += " FROM " + FEEDBACK_TABLE_NAME;
            sql += " WHERE "+KIGA_EXTERNALID_COLUMN +" = ?";
            sql += " AND " + SYNCHRONIZATION_ACTION_COLUMN + " != '0'";

            String[] whereArgs = { kigaId };

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, whereArgs);

            cursor.moveToFirst();
            while (!cursor.isAfterLast())
            {
                recordsList.add(GetFeedbackFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return recordsList;
    }

    private Feedback GetFeedbackFromCursor(Cursor cursor){
        int id = Integer.parseInt(cursor.getString(cursor.getColumnIndex(FEEDBACK_TABLE_ID_COLUMN)));
        String subject = cursor.getString(cursor.getColumnIndex(FEEDBACK_TABLE_SUBJECT_COLUMN));
        String message = cursor.getString(cursor.getColumnIndex(FEEDBACK_TABLE_MESSAGE_COLUMN));

        String kigaExternalId = cursor.getString(cursor.getColumnIndex(KIGA_EXTERNALID_COLUMN));
        Integer synchronizationAction = cursor.getInt(cursor.getColumnIndex(SYNCHRONIZATION_ACTION_COLUMN));

        return new Feedback(kigaExternalId, synchronizationAction, id, subject, message);
    }


    public void addFeedback(String kigaId, String subject, String message){
        try {
            ContentValues values = new ContentValues();

            values.put(FEEDBACK_TABLE_SUBJECT_COLUMN, subject);
            values.put(FEEDBACK_TABLE_MESSAGE_COLUMN, message);

            values.put(KIGA_EXTERNALID_COLUMN, kigaId);
            values.put(SYNCHRONIZATION_ACTION_COLUMN, SyncAction.Add.getCode());

            SQLiteDatabase db = this.getWritableDatabase();

            db.insert(FEEDBACK_TABLE_NAME, null, values);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public boolean isSystemOnSync(String kigaId) {
        Boolean isInSync = true;

        try{
            List<String> tables = new ArrayList<String>(){{
                add(KIGA_TABLE_NAME);
                add(GROUPS_TABLE_NAME);
                add(TEACHERS_TABLE_NAME);
                add(KIDS_TABLE_NAME);
                add(KID_CONTACTS_TABLE_NAME);
                add(FEEDBACK_TABLE_NAME);
                add(SCHEDULED_ACTIVITIES_TABLE_NAME);
                add(EVENTS_TABLE_NAME);  // should be last
            }};

            String sql = "";
            for (String table:tables){
                sql +=
                        " SELECT _id"+
                        " FROM " + table +
                        " WHERE "  + KIGA_EXTERNALID_COLUMN + "= '"+kigaId+"'" +
                        " AND " + SYNCHRONIZATION_ACTION_COLUMN + " != '0'";
                if(!table.equals(EVENTS_TABLE_NAME))
                    sql += " UNION ALL";
            }
            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
            {
                isInSync = false;
            }
            cursor.close();
            db.close();
        } catch (NullPointerException e) {
            isInSync = false;
            e.printStackTrace();
        } catch (Exception e) {
            isInSync = false;
            e.printStackTrace();
        }

        return isInSync;
    }

    public boolean isEventsOnSync(String kigaId) {
        Boolean isInSync = true;

        try{
            String sql = "";
                sql +=
                    " SELECT _id"+
                            " FROM " + EVENTS_TABLE_NAME +
                            " WHERE "  + KIGA_EXTERNALID_COLUMN + "= '"+kigaId+"'" +
                            " AND " + SYNCHRONIZATION_ACTION_COLUMN + " != '0'";
            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
            {
                isInSync = false;
            }
            cursor.close();
            db.close();
        } catch (NullPointerException e) {
            isInSync = false;
            e.printStackTrace();
        } catch (Exception e) {
            isInSync = false;
            e.printStackTrace();
        }

        return isInSync;
    }

    public void updateSetting(String name, String value){
       try{
           ContentValues values = new ContentValues();

           values.put(SETTINGS_TABLE_VALUE_COLUMN, value);

           String where = SETTINGS_TABLE_NAME_COLUMN + " = ?";
           String[] whereArgs = { name };

           SQLiteDatabase db = this.getWritableDatabase();

           db.update(SETTINGS_TABLE_NAME, values, where, whereArgs);
           db.close();

       } catch (NullPointerException e) {
           e.printStackTrace();
       } catch (Exception e) {
           e.printStackTrace();
       }
    }

    public String getOrCreateSetting(String name, String value){
        String finalValue = null;

        try {

            String sql = "SELECT * FROM "+SETTINGS_TABLE_NAME+" WHERE "+SETTINGS_TABLE_NAME_COLUMN+"= '"+name+"'";

            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            String oldValue = null;
            if (cursor.moveToFirst())
            {
                oldValue = cursor.getString(cursor.getColumnIndex(SETTINGS_TABLE_VALUE_COLUMN));
            }
            cursor.close();

            if(oldValue != null/* && value == null*/)
                finalValue = oldValue;
            else{
                finalValue = value;

                ContentValues values = new ContentValues();

                values.put(SETTINGS_TABLE_NAME_COLUMN, name);
                values.put(SETTINGS_TABLE_VALUE_COLUMN, value);


                //if(oldValue == null){
                    db.insert(SETTINGS_TABLE_NAME, null, values);
                /*}
                else{
                    String where = SETTINGS_TABLE_NAME_COLUMN + " = ?";
                    String[] whereArgs = { name };

                    db.update(SETTINGS_TABLE_NAME, values, where, whereArgs);
                }*/
            }

            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return finalValue;
    }

    public ScheduledActivity getScheduledActivity(String kigaId,String externalId) {
        List<ScheduledActivity> activities = readScheduledActivities(kigaId, 0, externalId, false);
        if(activities == null || activities.size() == 0)
            return null;

        return activities.get(0);
    }

    public ScheduledActivity getScheduledActivity(String kigaId,long id) {
        List<ScheduledActivity> activities = readScheduledActivities(kigaId, id, null, false);
        if(activities == null || activities.size() == 0)
            return null;

        return activities.get(0);
    }

    public List<ScheduledActivity> readScheduledActivities(String kigaId, boolean forSync) {
        return readScheduledActivities(kigaId, 0, null, forSync);
    }

    public List<ScheduledActivity> readScheduledActivities(String kigaId, long actiId, String actiExternalId, boolean forSync) {

        List<ScheduledActivity> recordsList = new ArrayList<ScheduledActivity>();

        String[] whereArgs = { kigaId };

        try {
            String sql = "";
            sql += " SELECT a.*";
            sql += " FROM " + SCHEDULED_ACTIVITIES_TABLE_NAME + " as a";

            if(forSync){
                sql += " WHERE a." + SYNCHRONIZATION_ACTION_COLUMN + " != '0'";
                sql += " AND a."+KIGA_EXTERNALID_COLUMN +" = ?";

                sql += " AND NOT EXISTS( SELECT k."+EXTERNALID_COLUMN;
                sql += " FROM "+KIDS_TABLE_NAME+" as k";
                sql += " JOIN "+SCHEDULED_ACTIVITY_TO_KIDS_TABLE_NAME + " as ek ON a."+SCHEDULED_ACTIVITIES_TABLE_ID_COLUMN + " = ek."+SCHEDULED_ACTIVITY_TO_KIDS_TABLE_SCHEDULED_ACTIVITYID_COLUMN;
                sql += " AND k."+EXTERNALID_COLUMN + " = ek."+SCHEDULED_ACTIVITY_TO_KIDS_TABLE_KID_EXTERNALID_COLUMN;
                sql += " WHERE k."+EXTERNALID_COLUMN + " IS NULL)";
            }else{
                sql += " WHERE a."+KIGA_EXTERNALID_COLUMN +" = ?";
            }

            if(actiId > 0){
                sql += " AND a."+SCHEDULED_ACTIVITIES_TABLE_ID_COLUMN + " = '"+actiId+"'";
            }

            if(actiExternalId != null){
                sql += " AND a."+EXTERNALID_COLUMN+" = '"+actiExternalId+"'";
            }

            String sqlKids = "";
            sqlKids += " SELECT ek."+SCHEDULED_ACTIVITY_TO_KIDS_TABLE_KID_EXTERNALID_COLUMN;
            sqlKids += " FROM "+SCHEDULED_ACTIVITY_TO_KIDS_TABLE_NAME + " as ek";
            sqlKids += " JOIN " + KIDS_TABLE_NAME + " as k on k."+EXTERNALID_COLUMN + " = ek."+SCHEDULED_ACTIVITY_TO_KIDS_TABLE_KID_EXTERNALID_COLUMN;
            sqlKids += " WHERE ek."+SCHEDULED_ACTIVITY_TO_KIDS_TABLE_SCHEDULED_ACTIVITYID_COLUMN + " = ?";

            String sqlDates = "";
            sqlDates += " SELECT *";
            sqlDates += " FROM "+SCHEDULED_ACTIVITY_DATES_TABLE_NAME;
            sqlDates += " WHERE "+SCHEDULED_ACTIVITY_DATES_TABLE_SCHEDULED_ACTIVITYID_COLUMN + " = ?";

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, whereArgs);

            cursor.moveToFirst();
            while (!cursor.isAfterLast())
            {
                String kigaExternalId = cursor.getString(cursor.getColumnIndex(KIGA_EXTERNALID_COLUMN));
                Integer synchronizationAction = cursor.getInt(cursor.getColumnIndex(SYNCHRONIZATION_ACTION_COLUMN));
                String externalId = cursor.getString(cursor.getColumnIndex(EXTERNALID_COLUMN));

                long id = Long.parseLong(cursor.getString(cursor.getColumnIndex(SCHEDULED_ACTIVITIES_TABLE_ID_COLUMN)));
                Boolean isActive = cursor.getInt(cursor.getColumnIndex(SCHEDULED_ACTIVITIES_TABLE_ISACTIVE_COLUMN)) == 1;
                Integer type = cursor.getInt(cursor.getColumnIndex(SCHEDULED_ACTIVITIES_TABLE_TYPE_COLUMN));
                String title = cursor.getString(cursor.getColumnIndex(SCHEDULED_ACTIVITIES_TABLE_TITLE_COLUMN));
                String description = cursor.getString(cursor.getColumnIndex(SCHEDULED_ACTIVITIES_TABLE_DESCRIPTION_COLUMN));
                long createdOn = cursor.getLong(cursor.getColumnIndex(SCHEDULED_ACTIVITIES_TABLE_CREATEDON_COLUMN));

                ArrayList<String> kidsExternalIds = new ArrayList<String>();
                // kids
                String[] kidsWhereArgs = { String.valueOf(id) };
                Cursor kidsCursor = db.rawQuery(sqlKids, kidsWhereArgs);
                kidsCursor.moveToFirst();
                while (!kidsCursor.isAfterLast())
                {
                    String kidExternalId = kidsCursor.getString(kidsCursor.getColumnIndex(SCHEDULED_ACTIVITY_TO_KIDS_TABLE_KID_EXTERNALID_COLUMN));
                    kidsExternalIds.add(kidExternalId);

                    kidsCursor.moveToNext();
                }
                kidsCursor.close();

                ArrayList<ScheduledActivityDate> activityDates = new ArrayList<ScheduledActivityDate>();

                //event details
                String[] activityDatesWhereArgs = { String.valueOf(id) };
                Cursor activityDatesCursor = db.rawQuery(sqlDates, activityDatesWhereArgs);
                activityDatesCursor.moveToFirst();
                while (!activityDatesCursor.isAfterLast())
                {
                    long dateid = Long.parseLong(activityDatesCursor.getString(cursor.getColumnIndex(SCHEDULED_ACTIVITY_DATES_TABLE_ID_COLUMN)));
                    long date = activityDatesCursor.getLong(activityDatesCursor.getColumnIndex(SCHEDULED_ACTIVITY_DATES_TABLE_DATE_COLUMN));
                    Integer reminderType = activityDatesCursor.getInt(activityDatesCursor.getColumnIndex(SCHEDULED_ACTIVITY_DATES_TABLE_REMINDER_TYPE_COLUMN));
                    String reminderMessage = activityDatesCursor.getString(activityDatesCursor.getColumnIndex(SCHEDULED_ACTIVITY_DATES_TABLE_REMINDER_MESSAGE_COLUMN));
                    String dateexternalId = cursor.getString(activityDatesCursor.getColumnIndex(EXTERNALID_COLUMN));

                    ScheduledActivityDate activityDate = new ScheduledActivityDate(dateid, dateexternalId, date, reminderType, reminderMessage);
                    activityDates.add(activityDate);
                    activityDatesCursor.moveToNext();
                }
                activityDatesCursor.close();

                ScheduledActivity activity = new ScheduledActivity(kigaExternalId, synchronizationAction, id,externalId, isActive,type,title,description,activityDates,kidsExternalIds);
                recordsList.add(activity);

                cursor.moveToNext();
            }

            cursor.close();
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return recordsList;
    }

    public long addOrUpdateScheduledActivity(ScheduledActivity sa) {
        long saId = 0;
        try{
            saId = sa.getId();
            boolean isNew = sa.getId() <= 0;
            ScheduledActivity existingActivity = null;
            if(!isNew)
                existingActivity = getScheduledActivity(sa.getKigaExternalId(), saId);

            ContentValues values = new ContentValues();

            values.put(SCHEDULED_ACTIVITIES_TABLE_ISACTIVE_COLUMN, sa.getIsActive());
            values.put(SCHEDULED_ACTIVITIES_TABLE_TYPE_COLUMN, sa.getType());
            values.put(SCHEDULED_ACTIVITIES_TABLE_TITLE_COLUMN, sa.getTitle());
            values.put(SCHEDULED_ACTIVITIES_TABLE_DESCRIPTION_COLUMN, sa.getDescription());

            values.put(EXTERNALID_COLUMN, sa.getExternalId());
            values.put(SYNCHRONIZATION_ACTION_COLUMN, sa.getSyncAction());
            values.put(KIGA_EXTERNALID_COLUMN, sa.getKigaExternalId());

            SQLiteDatabase db = this.getWritableDatabase();

            if(isNew){
                values.put(SCHEDULED_ACTIVITIES_TABLE_CREATEDON_COLUMN, System.currentTimeMillis());
                saId = db.insert(SCHEDULED_ACTIVITIES_TABLE_NAME, null, values);
            }else{
                String[] whereArgs = { String.valueOf(saId) };
                String where = SCHEDULED_ACTIVITIES_TABLE_ID_COLUMN + " = ?";
                db.update(SCHEDULED_ACTIVITIES_TABLE_NAME, values, where, whereArgs);
            }

            /*db.delete(SCHEDULED_ACTIVITY_DATES_TABLE_NAME, SCHEDULED_ACTIVITY_DATES_TABLE_SCHEDULED_ACTIVITYID_COLUMN+" = '"+saId+"'", null);

            if(sa.getDates() != null && sa.getDates().size() > 0){
                for(ScheduledActivityDate date:sa.getDates()){
                    values.clear();

                    values.put(SCHEDULED_ACTIVITY_DATES_TABLE_SCHEDULED_ACTIVITYID_COLUMN, saId);
                    values.put(EXTERNALID_COLUMN, date.getExternalId());
                    values.put(SCHEDULED_ACTIVITY_DATES_TABLE_DATE_COLUMN, date.getDate());
                    values.put(SCHEDULED_ACTIVITY_DATES_TABLE_REMINDER_TYPE_COLUMN, date.getReminderType());
                    values.put(SCHEDULED_ACTIVITY_DATES_TABLE_REMINDER_MESSAGE_COLUMN, date.getReminderMessage());

                    db.insert(SCHEDULED_ACTIVITY_DATES_TABLE_NAME, null, values);
                }
            }

            db.delete(SCHEDULED_ACTIVITY_TO_KIDS_TABLE_NAME, SCHEDULED_ACTIVITY_TO_KIDS_TABLE_SCHEDULED_ACTIVITYID_COLUMN+" = '"+saId+"'", null);

            if(sa.getKidsExternalIds() != null && sa.getKidsExternalIds().size() > 0){
                for(String newKidExternalId: sa.getKidsExternalIds()){
                    values.clear();

                    values.put(SCHEDULED_ACTIVITY_TO_KIDS_TABLE_SCHEDULED_ACTIVITYID_COLUMN, saId);
                    values.put(SCHEDULED_ACTIVITY_TO_KIDS_TABLE_KID_EXTERNALID_COLUMN, newKidExternalId);
                    db.insert(SCHEDULED_ACTIVITY_TO_KIDS_TABLE_NAME, null, values);
                }
            }*/
            if(sa.getDates() != null && sa.getDates().size() > 0) {

                List<Long> dateIds = new ArrayList<Long>();
                for (ScheduledActivityDate date : sa.getDates()) {
                    values.clear();

                    values.put(SCHEDULED_ACTIVITY_DATES_TABLE_SCHEDULED_ACTIVITYID_COLUMN, saId);
                    values.put(EXTERNALID_COLUMN, date.getExternalId());
                    values.put(SCHEDULED_ACTIVITY_DATES_TABLE_DATE_COLUMN, date.getDate());
                    values.put(SCHEDULED_ACTIVITY_DATES_TABLE_REMINDER_TYPE_COLUMN, date.getReminderType());
                    values.put(SCHEDULED_ACTIVITY_DATES_TABLE_REMINDER_MESSAGE_COLUMN, date.getReminderMessage());

                    long dateId = date.getId();
                    boolean isDateNew = dateId <= 0;

                    if (isDateNew)
                        dateId = db.insert(SCHEDULED_ACTIVITY_DATES_TABLE_NAME, null, values);
                    else {
                        String[] whereArgs = {String.valueOf(dateId)};
                        String where = SCHEDULED_ACTIVITY_DATES_TABLE_ID_COLUMN + " = ?";
                        db.update(SCHEDULED_ACTIVITY_DATES_TABLE_NAME, values, where, whereArgs);
                    }

                    dateIds.add(dateId);
                }

                if (existingActivity != null) {
                    List<Long> oldDateIds = new ArrayList<Long>();
                    List<Long> newDateIds = new ArrayList<Long>();

                    for (ScheduledActivityDate oldDate : existingActivity.getDates())
                        oldDateIds.add(oldDate.getId());
                    for (ScheduledActivityDate newDate : sa.getDates())
                        newDateIds.add(newDate.getId());


                    for (Long oldDateId : oldDateIds) {
                        if (!newDateIds.contains(oldDateId)) {
                            db.delete(SCHEDULED_ACTIVITY_DATES_TABLE_NAME, SCHEDULED_ACTIVITY_DATES_TABLE_ID_COLUMN + " = '" + oldDateId + "'", null);
                        }
                    }
                }
            }

            if (sa.getKidsExternalIds() != null && sa.getKidsExternalIds().size() > 0) {
                for (String newKidExternalId : sa.getKidsExternalIds()) {
                    if (existingActivity == null || !existingActivity.getKidsExternalIds().contains(newKidExternalId)) {
                        values.clear();

                        values.put(SCHEDULED_ACTIVITY_TO_KIDS_TABLE_SCHEDULED_ACTIVITYID_COLUMN, saId);
                        values.put(SCHEDULED_ACTIVITY_TO_KIDS_TABLE_KID_EXTERNALID_COLUMN, newKidExternalId);
                        db.insert(SCHEDULED_ACTIVITY_TO_KIDS_TABLE_NAME, null, values);
                    }
                }

                if (existingActivity != null) {
                    for (String oldKidExternalId : existingActivity.getKidsExternalIds()) {
                        if (!sa.getKidsExternalIds().contains(oldKidExternalId)) {
                            db.delete(SCHEDULED_ACTIVITY_TO_KIDS_TABLE_NAME, SCHEDULED_ACTIVITY_TO_KIDS_TABLE_KID_EXTERNALID_COLUMN + " = '" + oldKidExternalId + "'", null);
                        }
                    }
                }
            }

            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return saId;
    }

    public void deleteScheduledActivityDates(long id) {
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();

            db.delete(SCHEDULED_ACTIVITY_DATES_TABLE_NAME, SCHEDULED_ACTIVITY_DATES_TABLE_SCHEDULED_ACTIVITYID_COLUMN+" = '"+id+"'", null);

            db.close();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /*public void deleteAndCreateScheduledActivityDates(long id, List<SyncActivityDateRequest> dates) {
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();

            db.delete(SCHEDULED_ACTIVITY_DATES_TABLE_NAME, SCHEDULED_ACTIVITY_DATES_TABLE_SCHEDULED_ACTIVITYID_COLUMN+" = '"+id+"'", null);

            if(dates != null && dates.size() > 0){
                ContentValues values = new ContentValues();
                for(SyncActivityDateRequest date:dates){
                    values.clear();

                    values.put(SCHEDULED_ACTIVITY_DATES_TABLE_SCHEDULED_ACTIVITYID_COLUMN, id);
                    values.put(EXTERNALID_COLUMN, date.id);
                    values.put(SCHEDULED_ACTIVITY_DATES_TABLE_DATE_COLUMN, Long.parseLong(date.scheduled_at)*1000L);
                    values.put(SCHEDULED_ACTIVITY_DATES_TABLE_REMINDER_TYPE_COLUMN, date.notification_schedule);
                    values.put(SCHEDULED_ACTIVITY_DATES_TABLE_REMINDER_MESSAGE_COLUMN, date.description);

                    db.insert(SCHEDULED_ACTIVITY_DATES_TABLE_NAME, null, values);
                }
            }

            db.close();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
}
