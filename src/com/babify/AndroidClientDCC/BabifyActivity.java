package com.babify.AndroidClientDCC;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;
import com.babify.AndroidClientDCC.Activities.LoginActivity;
import com.babify.AndroidClientDCC.DataLayer.DatabaseHelper;
import com.babify.AndroidClientDCC.DomainModel.Kindergarten;
import com.babify.AndroidClientDCC.Helpers.DCCHelper;
import com.mbalychev.Shared.Helpers.UIHelper;

public abstract class BabifyActivity extends Activity {

    public Kindergarten getCurrentKindergarten() {
       return ((BabifyApplication)getApplicationContext()).getCurrentKiga();
    }

    public void setCurrentKindergarten(Kindergarten kiga){
        ((BabifyApplication)getApplicationContext()).setCurrentKiga(kiga);
    }

    public String getCurrentKigaId(){
        if(getCurrentKindergarten() != null)
            return getCurrentKindergarten().getKigaExternalId();

        return null;
    }

    public Boolean isSynchnonizing(){
        return ((BabifyApplication)getApplicationContext()).isSynchnonizing();
    }

    private DatabaseHelper dbHelper;

    public DatabaseHelper getDbHelper(){
        return dbHelper;
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Integer totalUpdates = intent.getIntExtra("total",0);
            Integer successUpdates = intent.getIntExtra("success",0);
            SyncFinished(totalUpdates, successUpdates);
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);

        dbHelper = new DatabaseHelper(getApplicationContext());

        if(getCurrentKindergarten() != null && !DCCHelper.isSyncTaskStarted){
            //((BabifyApplication)getApplicationContext()).scheduleSyncTask(getCurrentKindergarten().getKigaExternalId());
            DCCHelper.isSyncTaskStarted = true;
        }
    }

    @Override
    protected void onResume() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(DCCHelper.SyncFinishedBroadcastAction);
        registerReceiver(receiver, filter);

        super.onResume();

        if(getCurrentKindergarten() == null || dbHelper == null)
        {
            redirectToLogin();
        }
    }

    @Override
    protected void onPause() {
        unregisterReceiver(receiver);
        super.onPause();
    }

    public void SyncFinished(Integer total, Integer success){
        if(total > 0){
            //String message = syncFinishedSuccess
            if(success < total){
                String message = getString(R.string.syncFinishedWithErrors);
                //message = message.replace("{error}", String.valueOf (total - success));
                UIHelper.showToast(getApplicationContext(), message, Toast.LENGTH_LONG, UIHelper.TDError);
            }else{
                //String message = getString(R.string.syncFinishedSuccess);
                //message = message.replace("{success}",String.valueOf(success));
                //UIHelper.showToast(getApplicationContext(), message, Toast.LENGTH_LONG, UIHelper.TDSuccess);
            }
        }
    }

    void redirectToLogin(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}