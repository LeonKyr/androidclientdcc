package com.babify.AndroidClientDCC;

import android.app.Application;
import android.content.Intent;
import com.babify.AndroidClientDCC.DataLayer.DatabaseHelper;
import com.babify.AndroidClientDCC.DomainModel.Kindergarten;
import com.babify.AndroidClientDCC.Helpers.DCCHelper;
import com.babify.AndroidClientDCC.Helpers.WebServiceHelper;
import com.babify.AndroidClientDCC.Synchronization.ISyncFinishedListener;
import com.babify.AndroidClientDCC.Synchronization.SyncTask;
import com.babify.AndroidClientDCC.Synchronization.SyncTaskInput;
import com.mbalychev.Shared.Helpers.UIHelper;

public class BabifyApplication extends Application implements ISyncFinishedListener {
    private Kindergarten CurrentKiga;
    //private ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
    private SyncTask syncTask;
    SyncTaskInput input;
    //private Locale myLocale;
    DatabaseHelper dbHelper;

    public Kindergarten getCurrentKiga() {
        return CurrentKiga;
    }

    public void setCurrentKiga(Kindergarten currentKiga) {
        CurrentKiga = currentKiga;
    }

    public boolean isSynchnonizing(){
        return input.isSynchnonizing;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        dbHelper = new DatabaseHelper(getApplicationContext());
        // initial DeviceId init
        String deviceId = dbHelper.getOrCreateSetting(DCCHelper.deviceIdPrefKey, UIHelper.getDeviceUniqueId());

        input = new SyncTaskInput(this);
        input.deviceId = deviceId;
        input.isSynchnonizing = false;
    }

    /*public void setLang(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String lang = prefs.getString(SettingsActivity.KEY_PREF_LANG, "");
        changeLang(lang);
    }

    public void changeLang(String lang)
    {
        //lang = "es";
        if (lang.equalsIgnoreCase("")) return;
        //myLocale = new Locale(lang);
        //saveLocale(lang);
        //Locale.setDefault(myLocale);
        //android.content.res.Configuration config = new android.content.res.Configuration();
        //config.locale = myLocale;
        //getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        Resources res = getResources();
        // Change locale settings in the app.
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.locale = new Locale(lang.toLowerCase());
        res.updateConfiguration(conf, dm);
    }*/

    /*public void saveLocale(String lang)
    {
        SharedPreferences prefs = getSharedPreferences(DCCHelper.sharedPrefsName, MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(SettingsActivity.KEY_PREF_LANG, lang);
        editor.commit();
    }*/

    /*public void scheduleSyncTask(final String kigaId) {
        super.onCreate();

        scheduler.scheduleAtFixedRate
                (new Runnable() {
                    public void run() {
                        runSyncTask(kigaId, SyncTaskInput.GeneralType);
                    }
                }, 0, 10, TimeUnit.MINUTES);
    }*/

    public void runSyncTask(final String kigaId){
        runSyncTask(kigaId, SyncTaskInput.NewImagesType);
    }

    private void runSyncTask(final String kigaId, String type){
        if(!input.isSynchnonizing){
            input.isSynchnonizing = true;
            if(UIHelper.hasNetwork(getApplicationContext()))
            {
                input.clear();
                input.isSynchnonizing = true;

                input.context = getApplicationContext();
                input.kigaId = kigaId;
                input.type = type;

                syncTask = new SyncTask();
                syncTask.execute(input);
            }else{
                input.isSynchnonizing = false;
            }
        }
    }

    @Override
    public void SyncFinished(int totalCount, int successCount, String type) {

        boolean runExtraSync = false;

        if(type.equals(SyncTaskInput.NewImagesType)){
            runExtraSync = true;
            input.isSynchnonizing = false;
            runSyncTask(input.kigaId, SyncTaskInput.GeneralType);
        }
        else if((type.equals(SyncTaskInput.GeneralType)
            || type.equals(SyncTaskInput.KidsOnlyType)
            || type.equals(SyncTaskInput.KidContactsOnlyType))
          && totalCount > 0
          && totalCount == successCount)
        {
            if(type.equals(SyncTaskInput.GeneralType)
                && dbHelper.readKids(input.kigaId, 0, true).size() > 0)
            {
                input.isSynchnonizing = false;
                runSyncTask(input.kigaId, SyncTaskInput.KidsOnlyType);
                runExtraSync = true;
            }
            else if(!type.equals(SyncTaskInput.KidContactsOnlyType)
                    && dbHelper.readKidContactsForSync(input.kigaId).size() > 0)
            {
                input.isSynchnonizing = false;
                runSyncTask(input.kigaId, SyncTaskInput.KidContactsOnlyType);
                runExtraSync = true;
            }else{
                Boolean isEventsOnSync = dbHelper.isEventsOnSync(getCurrentKiga().getKigaExternalId());
                if(!isEventsOnSync){
                    input.isSynchnonizing = false;
                    runSyncTask(input.kigaId, SyncTaskInput.EventsOnlyType);
                    runExtraSync = true;
                }
            }
        }

        if(!runExtraSync){
            input.isSynchnonizing = false;

            Intent broadcast = new Intent();
            broadcast.putExtra("total",totalCount);
            broadcast.putExtra("success",successCount);
            broadcast.setAction(DCCHelper.SyncFinishedBroadcastAction);
            sendBroadcast(broadcast);
        }
    }
}