package com.babify.AndroidClientDCC.Helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.babify.AndroidClientDCC.DataLayer.DatabaseHelper;
import com.babify.AndroidClientDCC.DomainModel.*;
import com.mbalychev.Shared.Domain.ActionKey;
import com.mbalychev.Shared.Domain.ActionType;
import com.babify.AndroidClientDCC.Synchronization.*;
import com.kodart.httpzoid.*;
import com.mbalychev.Shared.Helpers.ImageHelper;
import com.mbalychev.Shared.Helpers.UIHelper;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class WebServiceHelper
{
    public static void TrySyncGroup(final SyncTaskInput input, final Long groupId) throws InterruptedException {
        final DatabaseHelper dbHelper = new DatabaseHelper(input.context);
        Group group = dbHelper.getGroup(groupId, null);
        TrySyncGroup(input, group);
    }

    public static void TrySyncGroup(final SyncTaskInput input, final Group group) throws InterruptedException {
        Http http = HttpFactory.create(input.context);

        String url = GetServiceURL("group",group.getKigaExternalId());
        String externalId = group.getExternalId();

        if(externalId != null && !externalId.isEmpty()){
            url += "/"+externalId;
        }

        SyncGroupRequest request = new SyncGroupRequest();
        request.name = group.getName();
        request.color = String.valueOf(group.getColor());
        request.device = GetDevice(input.deviceId);

        Cancellable canc = http.post(url)
                .data(request)
                .handler(new ResponseHandler<SyncGroupResponse>() {
                    @Override
                    public void success(SyncGroupResponse response, com.kodart.httpzoid.HttpResponse httpResponse)
                    {
                         final DatabaseHelper dbHelper = new DatabaseHelper(input.context);
                        dbHelper.setEntitySynchronized(DatabaseHelper.GROUPS_TABLE_NAME, group.getId(), response.id);

                        input.decrement(true);
                    }
                    @Override
                    public void error(String message, HttpResponse response) {
                        input.decrement(false);
                    }

                    @Override
                    public void failure(NetworkError error) {
                        input.decrement(false);
                    }
                }).send();

        if(canc == Cancellable.Empty)
            input.decrement(false);
    }

    public static SyncDevice GetDevice(String deviceId) {
        SyncDevice device = new SyncDevice();
        device.id =  deviceId;

        return device;
    }

    public static void TrySyncTeacher(final SyncTaskInput input, final Long teacherId)
    {
        final DatabaseHelper dbHelper = new DatabaseHelper(input.context);
        Teacher teacher = dbHelper.getTeacher(teacherId, null);
        TrySyncTeacher(input, teacher);
    }
    public static void TrySyncTeacher(final SyncTaskInput input, final Teacher teacher)
    {
        Http http = HttpFactory.create(input.context);

        String url = GetServiceURL("teacher", teacher.getKigaExternalId());

        SyncTeacherRequest request = new SyncTeacherRequest();
        request.name = teacher.getName();
        request.device = GetDevice(input.deviceId);

        String externalId = teacher.getExternalId();
        if(externalId != null && !externalId.isEmpty()){
            url += "/"+externalId;
        }

        request.image = GetImage(teacher.getImage());

        Cancellable canc = http.post(url)
                .data(request)
                .handler(new ResponseHandler<SyncTeacherResponse>() {
                    @Override
                    public void success(SyncTeacherResponse response, com.kodart.httpzoid.HttpResponse httpResponse) {
                        final DatabaseHelper dbHelper = new DatabaseHelper(input.context);
                        dbHelper.setEntitySynchronized(DatabaseHelper.TEACHERS_TABLE_NAME, teacher.getId(), response.id);

                        //if(teacher.getImage() != null && response.image != null && response.image.id != null)
                        //    dbHelper.setEntitySynchronized(DatabaseHelper.MEDIA_TABLE_NAME, teacher.getImage().getId(), response.image.id);

                        input.decrement(true);
                    }

                    @Override
                    public void error(String message, HttpResponse response) {
                        input.decrement(false);
                    }

                    @Override
                    public void failure(NetworkError error) {
                        input.decrement(false);
                    }
                }).send();

        if(canc == Cancellable.Empty)
            input.decrement(false);
    }

    public static void TrySyncKid(final SyncTaskInput input, final Long kidId){
        final DatabaseHelper dbHelper = new DatabaseHelper(input.context);
        Kid kid = dbHelper.getKid(kidId, null);
        TrySyncKid(input, kid);
    }
    public static void TrySyncKid(final SyncTaskInput input, final Kid kid)
    {
        Http http = HttpFactory.create(input.context);

        String url = DCCHelper.ServiceUrl+"child/"+kid.getKigaExternalId();

        SyncKidRequest request = new SyncKidRequest();
        request.name = kid.getName();
        request.dob = String.valueOf(kid.getBirthday());
        request.group_id = kid.getGroupExternalId();
        request.device = GetDevice(input.deviceId);

        String externalId = kid.getExternalId();
        if(externalId != null && !externalId.isEmpty())
            url += "/"+externalId;


        request.image = GetImage(kid.getImage());

        Cancellable canc = http.post(url)
            .data(request)
            .handler(new ResponseHandler<SyncKidResponse>() {
                @Override
                public void success(SyncKidResponse response, com.kodart.httpzoid.HttpResponse httpResponse)
                {
                    final DatabaseHelper dbHelper = new DatabaseHelper(input.context);
                    dbHelper.setEntitySynchronized(DatabaseHelper.KIDS_TABLE_NAME, kid.getId(), response.id);

                    Boolean hasListeners = Boolean.valueOf(response.hasListeners);
                    Integer hasListenersInt = 0;
                    if(hasListeners)
                        hasListenersInt = 1;

                    dbHelper.updateKidCodeAndHasListeners(response.id, response.code, hasListenersInt);

                    //if(kid.getImage() != null && response.image != null && response.image.id != null)
                    //    dbHelper.setEntitySynchronized(DatabaseHelper.MEDIA_TABLE_NAME, kid.getImage().getId(), response.image.id);

                    input.decrement(true);
                }
                @Override
                public void error(String message, HttpResponse response) {
                    input.decrement(false);
                }

                @Override
                public void failure(NetworkError error) {
                    input.decrement(false);
                }
            }).send();

        if(canc == Cancellable.Empty)
            input.decrement(false);
    }

    public static void TrySyncKidContact(final SyncTaskInput input, final KidContact kidContact)
    {
        Http http = HttpFactory.create(input.context);

        String url = DCCHelper.ServiceUrl+"child-contact/"+kidContact.getKidExternalId();

        SyncKidContactRequest request = new SyncKidContactRequest();
        request.name = kidContact.getName();
        request.email = kidContact.getEmail();
        request.phone = kidContact.getPhone();
        request.relation = DCCHelper.GetRelation(kidContact.getRelation());

        request.device = GetDevice(input.deviceId);

        String externalId = kidContact.getExternalId();
        if(externalId != null && !externalId.isEmpty())
            url += "/"+externalId;

        Cancellable canc = http.post(url)
                .data(request)
                .handler(new ResponseHandler<SyncKidContactResponse>() {
                    @Override
                    public void success(SyncKidContactResponse response, com.kodart.httpzoid.HttpResponse httpResponse)
                    {
                        final DatabaseHelper dbHelper = new DatabaseHelper(input.context);
                        dbHelper.setEntitySynchronized(DatabaseHelper.KID_CONTACTS_TABLE_NAME, kidContact.getId(), response.id);

                        input.decrement(true);
                    }
                    @Override
                    public void error(String message, HttpResponse response) {
                        input.decrement(false);
                    }

                    @Override
                    public void failure(NetworkError error) {
                        input.decrement(false);
                    }
                }).send();

        if(canc == Cancellable.Empty)
            input.decrement(false);
    }

    public static void TrySyncKiga(final SyncTaskInput input, final Kindergarten kiga)
    {
        Http http = HttpFactory.create(input.context);

        SyncKigaRequest request = new SyncKigaRequest();
        request.name = kiga.getName();
        request.password = kiga.getPassword();
        request.address = kiga.getAddress();
        request.email = kiga.getEmail();
        request.phone = kiga.getPhone();
        request.contact_person = kiga.getContactPerson();
        request.device = GetDevice(input.deviceId);

        String url = DCCHelper.ServiceUrl+"kindergarten/"+kiga.getKigaExternalId();

        Cancellable canc = http.post(url)
            .data(request)
            .handler(new ResponseHandler<SyncKigaResponse>() {
                @Override
                public void success(SyncKigaResponse response, com.kodart.httpzoid.HttpResponse httpResponse) {
                    final DatabaseHelper dbHelper = new DatabaseHelper(input.context);
                    dbHelper.setEntitySynchronized(DatabaseHelper.KIGA_TABLE_NAME, kiga.getId(), null);

                    dbHelper.updateKigaStatus(kiga.getKigaExternalId(), response.status);

                    input.decrement(true);
                }

                @Override
                public void error(String message, HttpResponse response) {
                    input.decrement(false);
                }

                @Override
                public void failure(NetworkError error) {
                    input.decrement(false);
                }
            }).send();

        if(canc == Cancellable.Empty)
            input.decrement(false);
    }

    public static void TrySyncFeedback(final SyncTaskInput input, final Feedback feedback)
    {
        Http http = HttpFactory.create(input.context);

        SyncFeedbackRequest request = new SyncFeedbackRequest();
        request.source = feedback.getSubject();
        request.text = feedback.getMessage();

        String url = DCCHelper.ServiceUrl+"feedback/kindergarten/"+feedback.getKigaExternalId()+"/";

        Cancellable canc = http.post(url)
                .data(request)
                .handler(new ResponseHandler<Object>() {
                    @Override
                    public void success(Object response, com.kodart.httpzoid.HttpResponse httpResponse) {
                        final DatabaseHelper dbHelper = new DatabaseHelper(input.context);
                        dbHelper.setEntitySynchronized(DatabaseHelper.FEEDBACK_TABLE_NAME, feedback.getId(), null);

                        input.decrement(true);
                    }

                    @Override
                    public void error(String message, HttpResponse response) {
                        input.decrement(false);
                    }

                    @Override
                    public void failure(NetworkError error) {
                        input.decrement(false);
                    }
                }).send();

        if(canc == Cancellable.Empty)
            input.decrement(false);
    }

    public static void TrySyncEvent(final SyncTaskInput input, final Event event)
    {
        Http http = HttpFactory.create(input.context);
        final DatabaseHelper dbHelper = new DatabaseHelper(input.context);

        String url = DCCHelper.ServiceUrl+"event/"+event.getTeacherId()+"/"+event.getType()+"/";

        SyncEventRequest request = new SyncEventRequest();

        ArrayList<SyncEventChild> syncEventChildren = new ArrayList<SyncEventChild>();
        for(String childId:event.getKidsExternalIds()){
            SyncEventChild child = new SyncEventChild();
            child.childId = childId;
            syncEventChildren.add(child);
        }

        ArrayList<SyncEventProperty> syncEventProperties = new ArrayList<SyncEventProperty>();

        if(event.getType().equals(ActionType.Photos)){
            List<SyncPhotosEventValueProperty> photosValue = new ArrayList<SyncPhotosEventValueProperty>();

            for(EventDetail eventDetail:event.getEventDetails()){
                String key = eventDetail.getKey();
                Object value = eventDetail.getValue();

                if(key.equals(ActionKey.MediaId)){
                    long mediaId = Long.valueOf(value.toString());
                    Media image = dbHelper.getMedia(mediaId);
                    if(image.getExternalId() != null)
                    {
                        SyncPhotosEventValueProperty photoValue = new SyncPhotosEventValueProperty();
                        photoValue.id = image.getExternalId();

                        photosValue.add(photoValue);
                    }
                }
            }

            if(photosValue.size() == 0){
                input.decrement(false);
                return;
            }

            SyncEventProperty property = new SyncEventProperty();
            property.name = event.getType();
            property.value = photosValue;
            property.is_bag = true;
            syncEventProperties.add(property);
        }
        else{
            for(EventDetail eventDetail:event.getEventDetails()){
                SyncEventProperty property = new SyncEventProperty();

                String key = eventDetail.getKey();
                String value = eventDetail.getValue();

                property.name = key;
                property.value = value;
                property.is_bag = false;
                syncEventProperties.add(property);
            }
        }

        request.children = syncEventChildren;
        request.properties = syncEventProperties;

        Cancellable canc = http.post(url)
                .data(request)
                .handler(new ResponseHandler<SyncEventResponse>() {
                    @Override
                    public void success(SyncEventResponse response, com.kodart.httpzoid.HttpResponse httpResponse)
                    {
                        //final DatabaseHelper dbHelper = new DatabaseHelper(input.context);

                        dbHelper.setEntitySynchronized(DatabaseHelper.EVENTS_TABLE_NAME, event.getId(), response.id);

                        input.decrement(true);
                    }
                    @Override
                    public void error(String message, HttpResponse response) {
                        input.decrement(false);
                    }

                    @Override
                    public void failure(NetworkError error) {
                        input.decrement(false);
                    }
                }).send();

        if(canc == Cancellable.Empty)
            input.decrement(false);
    }

    public static void TrySyncScheduledActivity(final SyncTaskInput input, final ScheduledActivity activity) {
        Http http = HttpFactory.create(input.context);
        final DatabaseHelper dbHelper = new DatabaseHelper(input.context);

        String url = DCCHelper.ServiceUrl+"activity";
        if(activity.getExternalId() != null && !activity.getExternalId().isEmpty()){
            url += "/"+activity.getExternalId();
        }

        final SyncActivityRequest request = new SyncActivityRequest();

        request.device = GetDevice(input.deviceId);
        //request.teacher_id = "161f0390-1944-4209-9f7a-3a812e4b29ed"; // TODO: remove
        request.is_active = activity.getIsActive();
        request.name = activity.getTitle();
        request.description = activity.getDescription();
        request.activity_type_id = String.valueOf(activity.getType());

        List<SyncActivityChildRequest> children = new ArrayList<SyncActivityChildRequest>();
        for (String kidExternalId:activity.getKidsExternalIds()){
            SyncActivityChildRequest child = new SyncActivityChildRequest();
            child.id = kidExternalId;
            child.is_active = true;
            children.add(child);
        }

        List<SyncActivityDateRequest> dates = new ArrayList<SyncActivityDateRequest>();
        for(ScheduledActivityDate activityDate:activity.getDates()){
            SyncActivityDateRequest date = new SyncActivityDateRequest();
            date.id = activityDate.getExternalId();
            date.scheduled_at = String.valueOf(activityDate.getDate()/1000L);
            date.description = activityDate.getReminderMessage();
            date.notification_schedule = String.valueOf(activityDate.getReminderType());

            dates.add(date);
        }

        request.children = children;
        request.dates = dates;

        Cancellable canc = http.post(url)
                .data(request)
                .handler(new ResponseHandler<SyncActivityResponse>() {
                    @Override
                    public void success(SyncActivityResponse response, com.kodart.httpzoid.HttpResponse httpResponse)
                    {
                        ScheduledActivity sa = response.getScheduledActivity(input.kigaId, activity.getId());

                        dbHelper.deleteScheduledActivityDates(activity.getId());
                        dbHelper.addOrUpdateScheduledActivity(sa);
                        //dbHelper.setEntitySynchronized(DatabaseHelper.SCHEDULED_ACTIVITIES_TABLE_NAME, activity.getId(), response.id);
                        //dbHelper.deleteAndCreateScheduledActivityDates(activity.getId(), response.dates);

                        input.decrement(true);
                    }
                    @Override
                    public void error(String message, HttpResponse response) {
                        input.decrement(false);
                    }

                    @Override
                    public void failure(NetworkError error) {
                        input.decrement(false);
                    }
                }).send();

        if(canc == Cancellable.Empty)
            input.decrement(false);
    }

    public static void TrySyncMedia(final SyncTaskInput input, final Media media)
    {
        Http http = HttpFactory.create(input.context);

        if(media.getData() != null || media.getFilePath() != null){

            InputStream is = null;

            if(media.getFilePath() != null)
                try
                {
                    Bitmap  bm = ImageHelper.decodeFile(media.getFilePath(), 800);
                    is = ImageHelper.getStreamFromBitmap(bm);
                } catch (FileNotFoundException e) {
                    is = null;
                } catch (IOException e) {
                    is = null;
                }
            else
                is = new ByteArrayInputStream(media.getData());

            String url = DCCHelper.ServiceUrl + "image/" + input.deviceId;
            if(media.getExternalId() != null && !media.getExternalId().isEmpty()){
                url += "/"+media.getExternalId();
            }

            if(is == null){
                input.decrement(false);
                return;
            }

            Cancellable canc = http.post(url)
                .data(is)
                .handler(new ResponseHandler<SyncImageResponse>() {
                    @Override
                    public void success(SyncImageResponse response, com.kodart.httpzoid.HttpResponse httpResponse) {
                        final DatabaseHelper dbHelper = new DatabaseHelper(input.context);
                        //dbHelper.updateMediaExternalId(media.getId(), response.id);

                        dbHelper.setEntitySynchronized(DatabaseHelper.MEDIA_TABLE_NAME, media.getId(), response.id);

                        input.decrement(true);

                        /*try {
                            is.close();
                        } catch (IOException e) {}*/
                    }

                    @Override
                    public void error(String message, HttpResponse response) {
                        input.decrement(false);
                    }

                    @Override
                    public void failure(NetworkError error) {
                        input.decrement(false);
                    }
                }).send();

            if(canc == Cancellable.Empty)
                input.decrement(false);

        }else{
            input.decrement(false);
        }
    }

    public static void TryGetMedia(final String kigaExternalId, final Context context, final String imageId, final String entityId, final String entityTableName)
    {
        Http http = HttpFactory.create(context);
        http.get(DCCHelper.ServiceUrl + "image/"+imageId)
                .handler(new ResponseHandler<InputStream>(){
                    @Override
                    public void success(InputStream imageresponse, HttpResponse httpResponse)
                    {
                        try{
                            final DatabaseHelper dbHelper = new DatabaseHelper(context);

                            BitmapFactory.Options options=new BitmapFactory.Options();
                            Bitmap bm = BitmapFactory.decodeStream(imageresponse, null, options);
                            byte[] bytes = ImageHelper.getBytes(bm);

                            long imageid = dbHelper.addMedia(kigaExternalId, imageId, MediaType.image.toString(),"",bytes, null);

                            if(imageid > 0)
                                dbHelper.updateMediaId(entityTableName, entityId, imageid);

                        }catch(Exception ex){
                        }
                        finally{
                        }
                    }
                }).send();
    }

    public static void TryGetUpdates(final SyncTaskInput input)
    {
        Http http = HttpFactory.create(input.context);
        final DatabaseHelper dbHelper = new DatabaseHelper(input.context);

        SyncObjectWithDeviceRequest request = new SyncObjectWithDeviceRequest();
        request.device = GetDevice(input.deviceId);

        final String settingKey = input.deviceId + "_" + input.kigaId;
        String lastUpdatedDate = dbHelper.getOrCreateSetting(settingKey, "0");
        final String currTime = String.valueOf(UIHelper.getCurrentTimeUnix());
        final String url = DCCHelper.ServiceUrl+"sync/"+input.kigaId+"/"+lastUpdatedDate+"/";

        Cancellable canc = http.post(url)
                .data(request)
                .handler(new ResponseHandler<SyncUpdatesResponse>() {
                    @Override
                    public void success(SyncUpdatesResponse response, com.kodart.httpzoid.HttpResponse httpResponse)
                    {
                        if(response.kindergarten != null && response.kindergarten.id != null){
                            dbHelper.syncKigaInfo(
                                    response.kindergarten.id,
                                    response.kindergarten.name,
                                    response.kindergarten.address,
                                    response.kindergarten.phone,
                                    response.kindergarten.email,
                                    response.kindergarten.contact_person,
                                    response.kindergarten.status);
                        }

                        if(response.teachers != null && response.teachers.size() > 0){
                            for(SyncTeacherResponse teacher:response.teachers){
                                long teacherId = dbHelper.syncTeacher(input.kigaId, teacher);

                                if(teacher.image != null && teacherId > 0)
                                    TryGetMedia(input.kigaId, input.context, teacher.image.id, teacher.id, DatabaseHelper.TEACHERS_TABLE_NAME);
                            }
                        }

                        if(response.groups != null && response.groups.size() > 0){
                            for(SyncGroupResponse group:response.groups)
                                dbHelper.syncGroup(input.kigaId, group);
                        }

                        if(response.children != null && response.children.size() > 0){
                            for(SyncKidResponse kid:response.children){
                                long kidId = dbHelper.syncKid(input.kigaId, kid);

                                if(kid.image != null && kidId > 0)
                                    TryGetMedia(input.kigaId, input.context, kid.image.id, kid.id, DatabaseHelper.KIDS_TABLE_NAME);

                                if(kid.contacts != null && kid.contacts.size() > 0){
                                    for(SyncKidContactResponse kidContact:kid.contacts){
                                        dbHelper.syncKidContact(input.kigaId, kid.id, kidContact);
                                    }
                                }
                            }
                        }

                        if(response.activities != null && response.activities.size() > 0){
                            for(SyncActivityResponse activity:response.activities){

                                ScheduledActivity existingSA = dbHelper.getScheduledActivity(input.kigaId, activity.id);

                                boolean isNewActivity = existingSA == null;

                                long saId = isNewActivity ? 0 : existingSA.getId();
                                ScheduledActivity sa = activity.getScheduledActivity(input.kigaId, saId);

                                if(!isNewActivity)
                                    dbHelper.deleteScheduledActivityDates(saId);

                                dbHelper.addOrUpdateScheduledActivity(sa);
                            }
                        }

                        //dbHelper.setEntitySynchronized(DatabaseHelper.KIGA_TABLE_NAME, kiga.getId(), response.externalId);
                        dbHelper.updateSetting(settingKey, currTime);
                        input.decrement(true);
                    }
                    @Override
                    public void error(String message, HttpResponse response) {
                        input.decrement(false);
                    }

                    @Override
                    public void failure(NetworkError error) {
                        input.decrement(false);
                    }

                }).send();

        if(canc == Cancellable.Empty)
            input.decrement(false);
    }

    private static String GetServiceURL(String entity, String kigaExternalId) {
        return  DCCHelper.ServiceUrl+entity+"/"+kigaExternalId;
    }

    private static SyncImageBase GetImage(Media image) {
        if(image == null || image.getExternalId() == null || image.getExternalId().isEmpty()) return null;

        SyncImageBase syncImage = new SyncImageBase();
        syncImage.id = image.getExternalId();

        return syncImage;
    }
}