package com.babify.AndroidClientDCC.Helpers;

import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.babify.AndroidClientDCC.Activities.*;
import com.babify.AndroidClientDCC.R;
import com.mobsandgeeks.saripaar.Rule;

import java.util.HashMap;

public class DCCHelper
{
    public static Boolean isSyncTaskStarted = false;

    public static String sharedPrefsName = "com.babify.prefs";

    public static String deviceIdPrefKey = "deviceId";
    public static boolean istutorialAfterLoginAlreadyShown = false;
    public static int tutorialAfterLoginShowCount = 1;
    public static String redirectToExtra = "redirectTo";
    public static String tutorialImages = "tutorialImages";

    public static HashMap<String, int[]> tutorialImagesMap = new HashMap<String, int[]>()
    {{
            put(MainActivity.class.getSimpleName(),
                    new int[]{
                            R.drawable.tut_main1,
                    }
            );
            put(GroupsActivity.class.getSimpleName(),
                    new int[]{
                            R.drawable.tut_groups1,
                            R.drawable.tut_groups2,
                            R.drawable.tut_groups3,
                    }
            );
            put(TeachersActivity.class.getSimpleName(),
                    new int[]{
                            R.drawable.tut_teachers1,
                            R.drawable.tut_teachers2,
                            R.drawable.tut_teachers3,
                    }
            );
            put(KidsActivity.class.getSimpleName(),
                    new int[]{
                            R.drawable.tut_children1,
                            R.drawable.tut_children2,
                    }
            );
            put(GroupEditActivity.class.getSimpleName(),
                    new int[]{
                            R.drawable.tut_add_group1,
                    }
            );
            put(TeacherEditActivity.class.getSimpleName(),
                    new int[]{
                            R.drawable.tut_add_teacher1,
                    }
            );
            put(KidEditActivity.class.getSimpleName(),
                    new int[]{
                            R.drawable.tut_add_kid1,
                    }
            );
            put(ReportsActivity.class.getSimpleName(),
                    new int[]{
                            R.drawable.tut_reports1,
                            R.drawable.tut_reports2,
                            R.drawable.tut_reports3,
                    }
            );
            put(NewEventActivity.class.getSimpleName(),
                    new int[]{
                            R.drawable.tut_events1,
                            R.drawable.tut_events2,
                            R.drawable.tut_events3,
                            R.drawable.tut_events4,
                            R.drawable.tut_events5,
                            R.drawable.tut_events6,
                    }
            );
        }};

    public static int[] getTutorialImages(String name){
        if(tutorialImagesMap.containsKey(name))
            return tutorialImagesMap.get(name);

        return null;
    }

    public static final int AddEntityId = -999;

    public static String ServiceUrl = "http://api.parentmgr.com/v1/";
    public static String SyncFinishedBroadcastAction = "com.babify.AndroidClientDCC.SyncFinished";

    public static void validationFailedRule(Context context, View failedView, Rule<?> failedRule){
        String message = failedRule.getFailureMessage();

        if (failedView instanceof EditText) {
            failedView.requestFocus();
            ((EditText) failedView).setError(message);
        } else {
            com.mbalychev.Shared.Helpers.UIHelper.showToast(context, message, Toast.LENGTH_SHORT, com.mbalychev.Shared.Helpers.UIHelper.TDError);
        }

    }

    public static final String SCHEDULED_ACTIVITY_FORMAT = "dd MMM yy";
    public static final String BIRTHDAY_FORMAT = "dd MMM yyyy";
    public static final String TIME_FORMAT = "H:m";

    public static String GetRelation(int relation) {
        switch(relation){
            case 0:
                return "mother";
            case 1:
                return "father";
            case 2:
                return "sister";
            case 3:
                return "brother";
            case 4:
                return "grandmother";
            case 5:
                return "grandfather";
            default:
                return "unknown";
        }
    }

    public static int GetRelation(String relation) {
        if (relation.equals("mother")) {
            return 0;
        } else if (relation.equals("father")) {
            return 1;
        } else if (relation.equals("sister")) {
            return 2;
        } else if (relation.equals("brother")) {
            return 3;
        } else if (relation.equals("grandmother")) {
            return 4;
        } else if (relation.equals("grandfather")) {
            return 5;
        } else {
            return -1;
        }
    }
}
