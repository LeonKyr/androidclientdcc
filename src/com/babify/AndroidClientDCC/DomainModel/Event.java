package com.babify.AndroidClientDCC.DomainModel;

import java.util.List;

public class Event extends SynchronizableBase {

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    private int Id;

    public long getCreatedOn() {
        return mCreatedOn;
    }

    public void setCreatedOn(long mCreatedOn) {
        this.mCreatedOn = mCreatedOn;
    }

    private long mCreatedOn;

    public String getType() {
        return mType;
    }

    public void setType(String mType) {
        this.mType = mType;
    }

    private String mType;

    public String getTeacherId() {
        return mTeacherId;
    }

    public void setTeacherId(String mTeacherId) {
        this.mTeacherId = mTeacherId;
    }

    private String mTeacherId;

    public List<EventDetail> getEventDetails() {
        return mEventDetails;
    }

    public void setEventDetails(List<EventDetail> eventDetails) {
        this.mEventDetails = eventDetails;
    }

    private List<EventDetail> mEventDetails;

    public List<String> getKidsExternalIds() {
        return mKidsExternalIds;
    }

    public void setKidsExternalIds(List<String> kidsExternalIds) {
        this.mKidsExternalIds = kidsExternalIds;
    }

    private List<String> mKidsExternalIds;

    public String getExternalId() {
        return mExternalId;
    }

    public void setExternalId(String mExternalId) {
        this.mExternalId = mExternalId;
    }

    private String mExternalId;

    public Event(String kigaExternalId, int syncAction, int id, String externalId, long createdOn, String type, String teacherExternalId, List<EventDetail> eventDetails, List<String> kidsExternalIds)
    {
        super(kigaExternalId, syncAction);

        setId(id);
        setExternalId(externalId);
        setCreatedOn(createdOn);
        setType(type);
        setTeacherId(teacherExternalId);
        setEventDetails(eventDetails);
        setKidsExternalIds(kidsExternalIds);
    }
}