package com.babify.AndroidClientDCC.DomainModel;

import java.util.List;

public class ScheduledActivity extends SynchronizableBase {

    private boolean isActive;
    private long Id;
    private int mType;
    private String mExternalId;
    private String mTitle;
    private String mDescription;
    private List<String> mKidsExternalIds;
    private List<ScheduledActivityDate> mDates;

    public ScheduledActivity(String kigaExternalId, int syncAction, long id, String externalId, boolean isActive, int type, String title, String description, List<ScheduledActivityDate> dates, List<String> kidsExternalIds)
    {
        super(kigaExternalId, syncAction);

        setIsActive(isActive);
        setId(id);
        setExternalId(externalId);
        setType(type);
        setTitle(title);
        setDescription(description);
        setDates(dates);
        setKidsExternalIds(kidsExternalIds);
    }

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public int getType() {
        return mType;
    }

    public void setType(int mType) {
        this.mType = mType;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public List<String> getKidsExternalIds() {
        return mKidsExternalIds;
    }

    public void setKidsExternalIds(List<String> kidsExternalIds) {
        this.mKidsExternalIds = kidsExternalIds;
    }

    public String getExternalId() {
        return mExternalId;
    }

    public void setExternalId(String mExternalId) {
        this.mExternalId = mExternalId;
    }

    public List<ScheduledActivityDate> getDates() {
        return mDates;
    }

    public void setDates(List<ScheduledActivityDate> mDates) {
        this.mDates = mDates;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean active) {
        isActive = active;
    }
}
