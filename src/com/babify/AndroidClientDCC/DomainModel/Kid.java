package com.babify.AndroidClientDCC.DomainModel;

public class Kid extends SynchronizableBase {
    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    private int mId;

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    private String mName;

    public Integer getGroupId() {
        return mGroupId;
    }

    public void setGroupId(Integer mGroupId) {
        this.mGroupId = mGroupId;
    }

    private Integer mGroupId;

    public long getBirthday() {
        return mBirthday;
    }

    public void setBirthday(long mBirthday) {
        this.mBirthday = mBirthday;
    }

    private long mBirthday;


    public String getGroupName() {
        return mGroupName;
    }

    public void setGroupName(String mName) {
        this.mGroupName = mName;
    }

    private String mGroupName;

    public String getGroupExternalId() {
        return mGroupExternalId;
    }

    public void setGroupExternalId(String mExternalId) {
        this.mGroupExternalId = mExternalId;
    }

    private String mGroupExternalId;

    public Media getImage() {
        return mImage;
    }

    public void setImage(Media mImage) {
        this.mImage = mImage;
    }

    private Media mImage;

    public String getExternalId() {
        return mExternalId;
    }

    public void setExternalId(String mExternalId) {
        this.mExternalId = mExternalId;
    }

    private String mExternalId;

    public Integer getHasListeners() {
        return mHasListeners;
    }

    public void setHasListeners(Integer mHasListeners) {
        this.mHasListeners = mHasListeners;
    }

    private Integer mHasListeners;

    public String getUniqueId() {
        return mUniqueId;
    }

    public void setUniqueId(String mUniqueId) {
        this.mUniqueId = mUniqueId;
    }

    private String mUniqueId;

    public Kid(String kigaExternalId, int syncAction, int id, String externalId, int groupId, String groupName, String groupExternalId, String name, Media image, long birthday, Integer hasListeners, String uniqueId)
    {
        super(kigaExternalId, syncAction);

        setId(id);
        setExternalId(externalId);
        setName(name);
        setGroupId(groupId);
        setGroupName(groupName);
        setGroupExternalId(groupExternalId);
        setBirthday(birthday);
        setImage(image);
        setHasListeners(hasListeners);
        setUniqueId(uniqueId);
    }

    public Kid(String name){
        this(null, 0, 0, null, 0, null, null, name, null, 0, 0, null);
    }
}
