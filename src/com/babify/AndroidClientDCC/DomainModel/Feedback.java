package com.babify.AndroidClientDCC.DomainModel;

public class Feedback extends SynchronizableBase {

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    private int Id;

    public String getSubject() {
        return mSubject;
    }

    public void setSubject(String mSubject) {
        this.mSubject = mSubject;
    }

    private String mSubject;

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    private String mMessage;



    public Feedback(String kigaExternalId, int syncAction, int id, String subject, String message)
    {
        super(kigaExternalId, syncAction);

        setId(id);
        setSubject(subject);
        setMessage(message);
    }
}