package com.babify.AndroidClientDCC.DomainModel;

public class KidContact extends SynchronizableBase{
    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    private int mId;

    public int getKidId() {
        return mKidId;
    }

    public void setKidId(int mKidId) {
        this.mKidId = mKidId;
    }

    private int mKidId;

    public String getKidExternalId() {
        return mKidExternalId;
    }

    public void setKidExternalId(String mKidExternalId) {
        this.mKidExternalId = mKidExternalId;
    }

    private String mKidExternalId;

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    private String mName;

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String mPhone) {
        this.mPhone = mPhone;
    }

    private String mPhone;

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    private String mEmail;

    public boolean getIsInvited() {
        return mIsInvited;
    }

    public void setIsInvited(boolean mIsInvited) {
        this.mIsInvited = mIsInvited;
    }

    private boolean mIsInvited;

    public int getRelation() {
        return mRelation;
    }

    public void setRelation(int mRelation) {
        this.mRelation = mRelation;
    }

    private int mRelation;

    public String getExternalId() {
        return mExternalId;
    }

    public void setExternalId(String mExternalId) {
        this.mExternalId = mExternalId;
    }

    private String mExternalId;

    public KidContact(String kigaExternalId, int syncAction, int id, String externalId, int kidId, String name, String phone, String email, int relation, boolean isInvited)
    {
        super(kigaExternalId, syncAction);

        setId(id);
        setExternalId(externalId);
        setKidId(kidId);
        setName(name);
        setPhone(phone);
        setEmail(email);
        setRelation(relation);
        setIsInvited(isInvited);
    }

    public KidContact(String name, String phone, String email, int relation){
        this(null, 0,0,null, 0, name, phone, email,relation, false);
    }
}
