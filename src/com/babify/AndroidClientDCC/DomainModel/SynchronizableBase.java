package com.babify.AndroidClientDCC.DomainModel;


public abstract class SynchronizableBase
{
    public int getSyncAction() {
        return mSyncAction;
    }

    public void setSyncAction(int mSyncAction) {
        this.mSyncAction = mSyncAction;
    }

    private int mSyncAction;

    public String getKigaExternalId() {
        return mKigaExternalId;
    }

    public void setKigaExternalId(String mKigaExternalId) {
        this.mKigaExternalId = mKigaExternalId;
    }

    private String mKigaExternalId;


    public SynchronizableBase(String kigaExternalId, int syncAction){
        setKigaExternalId(kigaExternalId);
        setSyncAction(syncAction);
    }
}
