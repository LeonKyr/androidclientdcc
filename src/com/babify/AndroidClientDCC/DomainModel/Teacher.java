package com.babify.AndroidClientDCC.DomainModel;

public class Teacher extends SynchronizableBase {
    public long getId() {
        return mId;
    }

    public void setId(long mId) {
        this.mId = mId;
    }

    private long mId;

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    private String mName;

    public Integer getIsActive() {
        return mIsActive;
    }

    public void setIsActive(Integer mIsActive) {
        this.mIsActive = mIsActive;
    }

    private Integer mIsActive;

    public Media getImage() {
        return mImage;
    }

    public void setImage(Media mImage) {
        this.mImage = mImage;
    }

    private Media mImage;

    public String getExternalId() {
        return mExternalId;
    }

    public void setExternalId(String mExternalId) {
        this.mExternalId = mExternalId;
    }

    private String mExternalId;

    public Teacher(String kigaExternalId, int syncAction, long id, String externalId, String name, Media image, Integer isActive)
    {
        super(kigaExternalId, syncAction);

        setId(id);
        setExternalId(externalId);
        setName(name);
        setImage(image);
        setIsActive(isActive);
    }

    public Teacher(String name){
        this(null, 0, 0, null, name, null, 0);
    }

    public String toString(){
        return mName;
    }
}
