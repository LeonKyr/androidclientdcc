package com.babify.AndroidClientDCC.DomainModel;

public class Media {

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        this.mId = id;
    }

    private long mId;

    public String getExternalId() {
        return mExternalId;
    }

    public void setExternalId(String externalId) {
        this.mExternalId = externalId;
    }

    private String mExternalId;

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        this.mType = type;
    }

    private String mType;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    private String mName;

    public byte[] getData() {
        return mData;
    }

    public void setData(byte[] data) {
        this.mData = data;
    }

    private byte[] mData;

    public String getFilePath() {
        return mFilePath;
    }

    public void setFilePath(String filePath) {
        this.mFilePath = filePath;
    }

    private String mFilePath;


    public Media(long id, String externalId, String type, String name, byte[] data, String filePath){
        setId(id);
        setExternalId(externalId);
        setType(type);
        setName(name);
        setData(data);
        setFilePath(filePath);
    }

    public Media(byte[] data, String filePath){
        this(0,null,null,null,data, filePath);
    }
}