package com.babify.AndroidClientDCC.DomainModel;

public enum SyncAction
{
    None(0),Add(1),Remove(2),Change(3);

    private int code;

    private SyncAction(int c) {
        code = c;
    }

    public int getCode() {
        return code;
    }
}