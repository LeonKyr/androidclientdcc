package com.babify.AndroidClientDCC.DomainModel;

public class Group extends SynchronizableBase
{
    public long getId() {
        return mId;
    }

    public void setId(long mId) {
        this.mId = mId;
    }

    private long mId;

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    private String mName;

    public int getColor() {
        return mColor;
    }

    public void setColor(int mColor) {
        this.mColor = mColor;
    }

    private int mColor;

    public Integer getIsActive() {
        return mIsActive;
    }

    public void setIsActive(Integer mIsActive) {
        this.mIsActive = mIsActive;
    }

    private Integer mIsActive;

    public String getExternalId() {
        return mExternalId;
    }

    public void setExternalId(String mExternalId) {
        this.mExternalId = mExternalId;
    }

    private String mExternalId;

    public Group(String kigaExternalId, int syncAction, long id, String externalId, String name, int color, Integer isActive)
    {
        super(kigaExternalId, syncAction);

        setId(id);
        setExternalId(externalId);
        setName(name);
        setColor(color);
        setIsActive(isActive);
    }

    public Group(String name){
        this(null, 0, 0, null, name, 0, 0);
    }

    public String toString(){
        return mName;
    }
}
