package com.babify.AndroidClientDCC.DomainModel.Internal;

public class ButtonTagContainer {
    private String Name;
    private Object Value;


    public ButtonTagContainer(String name, Object value) {
        Name = name;
        Value = value;
    }

    public String getName() {
        return Name;
    }

    public Object getValue() {
        return Value;
    }
}
