package com.babify.AndroidClientDCC.DomainModel.Action;

import java.util.List;

public class MessageAction extends Action {

    public String getSubject() {
        return mSubject;
    }

    public void setSubject(String mSubject) {
        this.mSubject = mSubject;
    }

    private String mSubject;

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    private String mMessage;
    public MessageAction(int teacherId, List<Integer> kidIds,long createdOnMilis, String subject, String message) {
        super(teacherId, kidIds, createdOnMilis);

        setSubject(subject);
        setMessage(message);
    }

    public MessageAction(String subject, String message) {
        super(0, null, 0);

        setSubject(subject);
        setMessage(message);
    }
}
