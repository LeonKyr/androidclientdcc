package com.babify.AndroidClientDCC.DomainModel.Action;

public interface IActionFactory {
    public Action GetAction();
    public String GetActionType();
    public void Reset();
    boolean isValid();
}
