package com.babify.AndroidClientDCC.DomainModel.Action;

import java.util.List;

public class AbsenceAction extends Action {

    private int mReason;
    private String mMessage;

    public AbsenceAction(int teacherId, List<Integer> kidIds,long createdOnMilis, int reason, String message) {
        super(teacherId, kidIds, createdOnMilis);

        setReason(reason);
        setMessage(message);
    }

    public AbsenceAction(int reason, String message) {
        super(0, null, 0);

        setReason(reason);
        setMessage(message);
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public int getReason() {
        return mReason;
    }

    public void setReason(int mReason) {
        this.mReason = mReason;
    }
}