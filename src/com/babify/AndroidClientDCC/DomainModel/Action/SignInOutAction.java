package com.babify.AndroidClientDCC.DomainModel.Action;

import java.util.List;

public class SignInOutAction extends Action {

    public long getDateTime() {
        return mDateTime;
    }

    public void setDateTime(long mDateTime) {
        this.mDateTime = mDateTime;
    }

    private long mDateTime;

    public SignInOutAction(int teacherId, List<Integer> kidIds,long createdOnMilis, long dateTimeMilis) {
        super(teacherId, kidIds, createdOnMilis);

        setDateTime(dateTimeMilis);
    }

    public SignInOutAction(long dateTimeMilis) {
        this(0, null, 0, dateTimeMilis);
    }
}
