package com.babify.AndroidClientDCC.DomainModel.Action;

import java.util.List;

public class MealAction extends Action {

    private int mType;
    private int mQuality;
    private String mMessage;

    public MealAction(int teacherId, List<Integer> kidIds,long createdOnMilis, int type, int quality, String message) {
        super(teacherId, kidIds, createdOnMilis);

        setType(type);
        setQuality(quality);
        setMessage(message);
    }

    public MealAction(int type, int quality, String message) {
        super(0, null, 0);

        setType(type);
        setQuality(quality);
        setMessage(message);
    }

    public int getQuality() {
        return mQuality;
    }

    public void setQuality(int mQuality) {
        this.mQuality = mQuality;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public int getType() {
        return mType;
    }

    public void setType(int mType) {
        this.mType = mType;
    }
}