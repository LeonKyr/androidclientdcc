package com.babify.AndroidClientDCC.DomainModel.Action;

import java.util.List;

public class SleepAction extends Action {

    private int mDuration;
    private int mQuality;
    private String mMessage;

    public SleepAction(int teacherId, List<Integer> kidIds,long createdOnMilis, int duration, int quality, String message) {
        super(teacherId, kidIds, createdOnMilis);

        setDuration(duration);
        setQuality(quality);
        setMessage(message);
    }

    public SleepAction(int duration, int quality, String message) {
        super(0, null, 0);

        setDuration(duration);
        setQuality(quality);
        setMessage(message);
    }

    public int getDuration() {
        return mDuration;
    }

    public void setDuration(int mDuration) {
        this.mDuration = mDuration;
    }

    public int getQuality() {
        return mQuality;
    }

    public void setQuality(int mQuality) {
        this.mQuality = mQuality;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String mMessage) {
        this.mMessage = mMessage;
    }
}
