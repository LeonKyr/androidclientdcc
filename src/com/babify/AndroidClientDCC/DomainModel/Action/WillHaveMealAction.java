package com.babify.AndroidClientDCC.DomainModel.Action;

import java.util.List;

public class WillHaveMealAction extends Action {

    private int mType;
    private String mMessage;

    public WillHaveMealAction(int teacherId, List<Integer> kidIds, long createdOnMilis, int type, String message) {
        super(teacherId, kidIds, createdOnMilis);

        setType(type);
        setMessage(message);
    }

    public WillHaveMealAction(int type, String message) {
        super(0, null, 0);

        setType(type);
        setMessage(message);
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public int getType() {
        return mType;
    }

    public void setType(int mType) {
        this.mType = mType;
    }
}