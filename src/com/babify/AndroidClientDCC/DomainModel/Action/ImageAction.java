package com.babify.AndroidClientDCC.DomainModel.Action;

import java.util.List;

public class ImageAction extends Action {

    public List<String> getImagePaths() {
        return mImagePaths;
    }

    public void setImagePaths(List<String> mImagePaths) {
        this.mImagePaths = mImagePaths;
    }

    private List<String> mImagePaths;

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    private String mMessage;

    public ImageAction(int teacherId, List<Integer> kidIds, long createdOnMilis, List<String> imagePaths, String message) {
        super(teacherId, kidIds, createdOnMilis);

        setImagePaths(imagePaths);
        setMessage(message);
    }

    public ImageAction(List<String> imagePaths, String message) {
        super(0, null, 0);

        setImagePaths(imagePaths);
        setMessage(message);
    }
}
