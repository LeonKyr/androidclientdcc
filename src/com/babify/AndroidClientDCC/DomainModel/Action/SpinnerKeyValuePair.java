package com.babify.AndroidClientDCC.DomainModel.Action;


public class SpinnerKeyValuePair {

    private int mKey;
    private String mValue;

    public SpinnerKeyValuePair(int mKey, String mValue) {
        this.mKey = mKey;
        this.mValue = mValue;
    }

    public int getKey() {
        return mKey;
    }

    public void setKey(int mKey) {
        this.mKey = mKey;
    }

    public String getValue() {
        return mValue;
    }

    public void setValue(String mValue) {
        this.mValue = mValue;
    }

    public String toString(){
        return mValue;
    }
}
