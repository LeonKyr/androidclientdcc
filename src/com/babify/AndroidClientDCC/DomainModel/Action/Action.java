package com.babify.AndroidClientDCC.DomainModel.Action;

import java.util.List;

public abstract class Action {
    /*public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    private int mId;*/

    public int getTeacherId() {
        return mTeacherId;
    }

    public void setTeacherId(int mId) {
        this.mTeacherId = mId;
    }

    private int mTeacherId;

    public List<Integer> getKidIds() {
        return mKidIds;
    }

    public void setKidIds(List<Integer> mIds) {
        this.mKidIds = mIds;
    }

    private List<Integer> mKidIds;

    public long getCreatedOnMilis() {
        return mCreatedOnMilis;
    }

    public void setCreatedOnMilis(long mCreatedOnMilis) {
        this.mCreatedOnMilis = mCreatedOnMilis;
    }

    private long mCreatedOnMilis;

    public Action(int teacherId, List<Integer> kidIds, long createdOnInMilis){
        //setId(id);
        setTeacherId(teacherId);
        setKidIds(kidIds);
        setCreatedOnMilis(createdOnInMilis);
    }
}
