package com.babify.AndroidClientDCC.DomainModel;

public class Kindergarten extends SynchronizableBase {

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    private int Id;

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    private String mName;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    private String mAddress;


    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String mPhone) {
        this.mPhone = mPhone;
    }

    private String mPhone;

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    private String mEmail;

    public boolean getIsAutoLogin() {
        return mIsAutoLogin;
    }

    public void setIsAutoLogin(boolean mIsAutoLogin) {
        this.mIsAutoLogin = mIsAutoLogin;
    }

    private boolean mIsAutoLogin;

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    private String mStatus;

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    private String mPassword;

    public String getContactPerson() {
        return mContactPerson;
    }

    public void setContactPerson(String mContactPerson) {
        this.mContactPerson = mContactPerson;
    }

    private String mContactPerson;


    public Kindergarten(String kigaExternalId, int syncAction, int id, String name, String address, String phone, String email, boolean isAutoLogin, String hashPassword, String contactPerson, String status)
    {
        super(kigaExternalId, syncAction);

        setId(id);
        setName(name);
        setAddress(address);
        setPhone(phone);
        setEmail(email);
        setIsAutoLogin(isAutoLogin);
        setStatus(status);
        setPassword(hashPassword);
        setContactPerson(contactPerson);
    }
}