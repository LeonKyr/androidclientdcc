package com.babify.AndroidClientDCC.DomainModel;

import java.util.List;

public class EventsSearchParams {
    public List<Integer> kidIds;
    public int type;
}
