package com.babify.AndroidClientDCC.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import com.babify.AndroidClientDCC.Activities.FullScreenImageViewActivity;
import com.mbalychev.Shared.Helpers.ImageHelper;

import java.util.ArrayList;

public class ImageAdapter extends BaseAdapter {

    private Context _context;
    private ArrayList<String> _filePaths = new ArrayList<String>();
    private int imageWidth;

    public ImageAdapter(Context context, ArrayList<String> filePaths,
                                int imageWidth) {
        this._context = context;
        this._filePaths = filePaths;
        this.imageWidth = imageWidth;
    }

    @Override
    public int getCount() {
        return this._filePaths.size();
    }

    @Override
    public Object getItem(int position) {
        return this._filePaths.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(_context);
        } else {
            imageView = (ImageView) convertView;
        }

        // get screen dimensions
        Bitmap image =  ImageHelper.decodeFile(_filePaths.get(position), imageWidth);
        //ImageHelper.getImageThumbnail(_filePaths.get(position), _context.getContentResolver(), MediaStore.Images.Thumbnails.MICRO_KIND);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setLayoutParams(new GridView.LayoutParams(imageWidth, imageWidth));
        imageView.setImageBitmap(image);

        return imageView;
    }
}