package com.babify.AndroidClientDCC.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.babify.AndroidClientDCC.DomainModel.ScheduledActivity;
import com.babify.AndroidClientDCC.DomainModel.ScheduledActivityDate;
import com.babify.AndroidClientDCC.Helpers.DCCHelper;
import com.babify.AndroidClientDCC.R;
import com.mbalychev.Shared.Helpers.ImageHelper;
import com.mbalychev.Shared.Helpers.UIHelper;

import java.util.Collections;
import java.util.List;

public class ScheduledActivitiesAdapter extends ArrayAdapter<ScheduledActivity> {

    private final Context context;
    private final int layoutResourceId;
    private List<ScheduledActivity> data;
    private ScheduledActivityHolder holder;
    private LayoutInflater inflater;

    public ScheduledActivitiesAdapter(Context context, int layoutResourceId, List<ScheduledActivity> data)
    {
        super(context, layoutResourceId, data);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
        this.inflater = LayoutInflater.from(context);

    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        ScheduledActivity element = data.get(position);
        View row = convertView;
        if (row == null) {
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ScheduledActivityHolder();
            holder.title = (TextView)row.findViewById(R.id.scheduled_activity_row_title);
            holder.dates = (TextView)row.findViewById(R.id.scheduled_activity_row_dates);
            holder.kidsCount = (TextView)row.findViewById(R.id.scheduled_activity_row_kidscount);
            holder.type = (TextView)row.findViewById(R.id.scheduled_activity_row_type);
            holder.isActiveImage = (ImageView) row.findViewById(R.id.scheduled_activity_row_isactive_image);

            row.setTag(holder);
        }else{
            holder = (ScheduledActivityHolder)row.getTag();
        }

        holder.title.setText(element.getTitle());
        holder.dates.setText(getDatesString(element.getDates()));
        holder.kidsCount.setText(String.valueOf(element.getKidsExternalIds().size())+" "+UIHelper.getStringResourceByName(context,"kids"));
        holder.type.setText(UIHelper.getStringResourceByName(context,"scheduledactivity_type_"+element.getType()));
        if(element.getIsActive())
            holder.isActiveImage.setImageResource(R.drawable.scheduler_active);
        else
            holder.isActiveImage.setImageResource(R.drawable.scheduler_notactive);

        return row;
    }

    private String getDatesString(List<ScheduledActivityDate> dates) {
        if(dates == null || dates.size() == 0)
            return "";

        Collections.sort(dates);

        if(dates.size() == 1)
            return UIHelper.getDateString(dates.get(0).getDate(), DCCHelper.SCHEDULED_ACTIVITY_FORMAT);

        return
                  UIHelper.getDateString(dates.get(0).getDate(), DCCHelper.SCHEDULED_ACTIVITY_FORMAT)
                + "-"
                + UIHelper.getDateString(dates.get(dates.size() - 1).getDate(), DCCHelper.SCHEDULED_ACTIVITY_FORMAT);
    }

    /*public void swapItems(List<ScheduledActivity> data) {
        this.data = data;
        notifyDataSetChanged();
    }*/

    static class ScheduledActivityHolder {
        TextView title;
        TextView dates;
        TextView type;
        ImageView isActiveImage;
        TextView kidsCount;
    }
}