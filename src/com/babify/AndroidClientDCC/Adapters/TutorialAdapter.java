package com.babify.AndroidClientDCC.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.babify.AndroidClientDCC.Activities.TutorialFragment;

public class TutorialAdapter extends FragmentPagerAdapter {
    public final int[] imageIds;

    public TutorialAdapter(FragmentManager fm, int[] imageIds) {
        super(fm);

        this.imageIds = imageIds;
    }

    @Override
    public int getCount() {
        return imageIds.length;
    }

    @Override
    public Fragment getItem(int position) {
        return new TutorialFragment(imageIds[position]);
    }
}
