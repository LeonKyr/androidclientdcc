package com.babify.AndroidClientDCC.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.babify.AndroidClientDCC.Activities.SliderBaseActivity;
import com.babify.AndroidClientDCC.DomainModel.Internal.ButtonTagContainer;
import com.babify.AndroidClientDCC.DomainModel.Kid;
import com.babify.AndroidClientDCC.DomainModel.ScheduledActivityDate;
import com.babify.AndroidClientDCC.Helpers.DCCHelper;
import com.babify.AndroidClientDCC.R;
import com.mbalychev.Shared.Helpers.ImageHelper;
import com.mbalychev.Shared.Helpers.UIHelper;

import java.util.List;

public class ActivityDatesAdapter extends ArrayAdapter<ScheduledActivityDate> {

    private final Context context;
    private final int layoutResourceId;
    private List<ScheduledActivityDate> data;
    private ScheduledActivityDateHolder holder;
    private LayoutInflater inflater;

    public ActivityDatesAdapter(Context context, int layoutResourceId, List<ScheduledActivityDate> data)
    {
        super(context, layoutResourceId, data);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
        this.inflater = LayoutInflater.from(context);

    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        ScheduledActivityDate element = data.get(position);
        View row = convertView;
        if (row == null) {
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ScheduledActivityDateHolder();
            holder.date = (TextView)row.findViewById(R.id.date_row_date);
            holder.reminder = (TextView)row.findViewById(R.id.date_row_reminder);
            holder.deleteImage = (ImageView)row.findViewById(R.id.date_row_delete);

            if(holder.deleteImage != null)
                holder.deleteImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(context instanceof SliderBaseActivity){
                            ((SliderBaseActivity) context).onGenericClick(view);
                        }
                    }
                });

            row.setTag(holder);
        }else{
            holder = (ScheduledActivityDateHolder)row.getTag();
        }

        holder.date.setText(UIHelper.getDateString( element.getDate(), DCCHelper.SCHEDULED_ACTIVITY_FORMAT));
        holder.reminder.setText(UIHelper.getStringResourceByName(context, "scheduledactivitydate_type_"+element.getReminderType()));

        ButtonTagContainer deleteContainer = new ButtonTagContainer("activityDate",element.getId());
        holder.deleteImage.setTag(deleteContainer);

        return row;
    }

    static class ScheduledActivityDateHolder {
        TextView date;
        TextView reminder;
        ImageView deleteImage;
    }
}