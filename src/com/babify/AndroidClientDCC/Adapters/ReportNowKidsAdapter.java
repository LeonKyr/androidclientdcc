package com.babify.AndroidClientDCC.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.babify.AndroidClientDCC.Activities.Reports.ReportNowFragment;
import com.babify.AndroidClientDCC.R;
import com.mbalychev.Shared.Helpers.ImageHelper;
import com.mbalychev.Shared.Helpers.UIHelper;

import java.util.List;

public class ReportNowKidsAdapter extends ArrayAdapter<ReportNowFragment.ReportNowResult> {

    private final Context context;
    private final int layoutResourceId;
    private List<ReportNowFragment.ReportNowResult> data;
    private KidHolder holder;
    private LayoutInflater inflater;

    public ReportNowKidsAdapter(Context context, int layoutResourceId, List<ReportNowFragment.ReportNowResult> data)
    {
        super(context, layoutResourceId, data);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
        this.inflater = LayoutInflater.from(context);

    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        ReportNowFragment.ReportNowResult element = data.get(position);
        View row = convertView;
        if (row == null) {
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new KidHolder();
            holder.name = (TextView)row.findViewById(R.id.kid_row_name);
            holder.group = (TextView)row.findViewById(R.id.kid_row_group);
            holder.type = (TextView)row.findViewById(R.id.kid_row_type);
            holder.image = (ImageView)row.findViewById(R.id.kid_row_image);

            row.setTag(holder);
        }else{
            holder = (KidHolder)row.getTag();
        }

        holder.name.setText(element.Kid.getName());
        holder.group.setText(element.Kid.getGroupName());

        holder.type.setText(UIHelper.getStringResourceByName(context, "report_now_type_"+element.Type));

        if(holder.image != null)
            if(element.Kid.getImage() != null)
                holder.image.setImageBitmap(ImageHelper.getBitmap(element.Kid.getImage().getData()));
            else
                holder.image.setImageResource(R.drawable.pm_default_kid);

        return row;
    }

    static class KidHolder {
        TextView name;
        TextView type;
        TextView group;
        ImageView image;
    }
}
