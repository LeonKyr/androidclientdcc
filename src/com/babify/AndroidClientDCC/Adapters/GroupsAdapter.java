package com.babify.AndroidClientDCC.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.babify.AndroidClientDCC.DomainModel.Group;
import com.babify.AndroidClientDCC.Helpers.DCCHelper;
import com.babify.AndroidClientDCC.R;

import java.util.List;

public class GroupsAdapter extends ArrayAdapter<Group> {

    private final Context context;
    private final int layoutResourceId;
    private final List<Group> data;
    private GroupHolder holder;
    private LayoutInflater inflater;
    private boolean isDropdown;

    public GroupsAdapter(Context context, int layoutResourceId, List<Group> data, boolean isDropdown)
    {
        super(context, layoutResourceId, data);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
        this.inflater = LayoutInflater.from(context);
        this.isDropdown = isDropdown;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        Group element = data.get(position);
        View row = convertView;
        if (row == null) {
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new GroupHolder();
            holder.name = (TextView)row.findViewById(R.id.group_row_name);
            holder.color = (ImageView)row.findViewById(R.id.group_row_color);

            row.setTag(holder);
        }else{
            holder = (GroupHolder)row.getTag();
        }

        holder.name.setText(element.getName());

        if(element.getId() == DCCHelper.AddEntityId){
            holder.color.setImageResource(R.drawable.pm_new);
        }
        else{
            holder.color.setBackgroundColor(element.getColor());
        }

        if(!isDropdown){
            if(element.getIsActive() == 1)
                row.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.pm_selected_item));
            else
                row.setBackgroundColor(Color.TRANSPARENT);
        }
        return row;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent)
    {
        return getView(position,convertView,parent);
    }

    static class GroupHolder {
        TextView name;
        ImageView color;
    }
}
