package com.babify.AndroidClientDCC.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.babify.AndroidClientDCC.DomainModel.KidContact;
import com.babify.AndroidClientDCC.R;

import java.util.List;

public class KidContactsAdapter extends ArrayAdapter<KidContact> {

    private final Context context;
    private final int layoutResourceId;
    private List<KidContact> data;
    private KidContactHolder holder;
    private LayoutInflater inflater;
    String[] relations;

    public KidContactsAdapter(Context context, int layoutResourceId, List<KidContact> data)
    {
        super(context, layoutResourceId, data);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
        this.inflater = LayoutInflater.from(context);
        relations = context.getResources().getStringArray(R.array.relations_array);
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        KidContact element = data.get(position);
        View row = convertView;
        if (row == null) {
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new KidContactHolder();
            holder.name = (TextView)row.findViewById(R.id.kidcontact_row_name);
            holder.relation = (TextView)row.findViewById(R.id.kidcontact_row_relation);

            row.setTag(holder);
        }else{
            holder = (KidContactHolder)row.getTag();
        }

        holder.name.setText(element.getName());
        holder.relation.setText(relations[element.getRelation()]);

        return row;
    }

    public void swapItems(List<KidContact> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    static class KidContactHolder {
        TextView name;
        TextView relation;
    }
}