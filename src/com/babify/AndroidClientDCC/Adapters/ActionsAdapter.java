package com.babify.AndroidClientDCC.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.babify.AndroidClientDCC.R;
import com.mbalychev.Shared.Helpers.UIHelper;

import java.util.List;

public class ActionsAdapter extends ArrayAdapter<String> {

    private final Context context;
    private final int layoutResourceId;
    private final List<String> data;
    private ActionHolder holder;
    private LayoutInflater inflater;

    public ActionsAdapter(Context context, int layoutResourceId, List<String> data)
    {
        super(context, layoutResourceId, data);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
        this.inflater = LayoutInflater.from(context);

    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        String element = data.get(position);
        View row = convertView;
        if (row == null) {
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ActionHolder();
            holder.action = element;
            holder.name = (TextView)row.findViewById(R.id.newevent_action_row_name);
            holder.image = (ImageView)row.findViewById(R.id.newevent_action_row_image);

            row.setTag(holder);
        }else{
            holder = (ActionHolder)row.getTag();
        }

        String filenamePart = element.replace("-","");

        holder.name.setText(UIHelper.getStringResourceByName(context, "action_"+filenamePart+"_type"));
        holder.image.setImageDrawable(UIHelper.getDrawableResourceByName(context, ("action_"+filenamePart+"_icon").toLowerCase()));

        return row;
    }

    public class ActionHolder {
        public String action;
        TextView name;
        ImageView image;
    }
}
