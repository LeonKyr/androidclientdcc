package com.babify.AndroidClientDCC.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.babify.AndroidClientDCC.Activities.SliderBaseActivity;
import com.babify.AndroidClientDCC.DomainModel.Internal.ButtonTagContainer;
import com.babify.AndroidClientDCC.DomainModel.Kid;
import com.babify.AndroidClientDCC.R;
import com.mbalychev.Shared.Helpers.ImageHelper;

import java.util.List;

public class KidsAdapter extends ArrayAdapter<Kid> {

    private final Context context;
    private final int layoutResourceId;
    private List<Kid> data;
    private KidHolder holder;
    private LayoutInflater inflater;

    public KidsAdapter(Context context, int layoutResourceId, List<Kid> data)
    {
        super(context, layoutResourceId, data);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
        this.inflater = LayoutInflater.from(context);

    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        Kid element = data.get(position);
        View row = convertView;
        if (row == null) {
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new KidHolder();
            //holder.container = (RelativeLayout)row.findViewById(R.id.kid_row_container);
            //holder.container.setTag(element.getId());
            holder.name = (TextView)row.findViewById(R.id.kid_row_name);
            holder.group = (TextView)row.findViewById(R.id.kid_row_group);
            holder.image = (ImageView)row.findViewById(R.id.kid_row_image);
            holder.hasListenersImage = (ImageView) row.findViewById(R.id.kid_haslisteners_image);
            holder.deleteImage = (ImageView)row.findViewById(R.id.kid_row_delete);

            if(holder.deleteImage != null)
                holder.deleteImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(context instanceof SliderBaseActivity){
                            ((SliderBaseActivity) context).onGenericClick(view);
                        }
                    }
                });

            row.setTag(holder);
        }else{
            holder = (KidHolder)row.getTag();
        }

        holder.name.setText(element.getName());
        holder.group.setText(element.getGroupName());

        if(holder.image != null)
            if(element.getImage() != null)
                try{
                    holder.image.setImageBitmap(ImageHelper.getBitmap(element.getImage().getData()));
                }catch(Exception ex){
                    holder.image.setImageResource(R.drawable.pm_default_kid);
                }
            else
                holder.image.setImageResource(R.drawable.pm_default_kid);


        if(holder.hasListenersImage != null)
            if(element.getHasListeners() == 1)
                holder.hasListenersImage.setVisibility(View.VISIBLE);
            else
                holder.hasListenersImage.setVisibility(View.GONE);

        if(holder.deleteImage != null){
            ButtonTagContainer deleteContainer = new ButtonTagContainer("kid",element.getExternalId());
            holder.deleteImage.setTag(deleteContainer);
        }

        return row;
    }

    public void swapItems(List<Kid> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    static class KidHolder {
        TextView name;
        TextView group;
        ImageView image;
        ImageView hasListenersImage;
        ImageView deleteImage;
        //RelativeLayout container;
    }
}