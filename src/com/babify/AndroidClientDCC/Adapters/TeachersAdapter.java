package com.babify.AndroidClientDCC.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.babify.AndroidClientDCC.DomainModel.Teacher;
import com.babify.AndroidClientDCC.R;
import com.mbalychev.Shared.Helpers.ImageHelper;

import java.util.List;

public class TeachersAdapter extends ArrayAdapter<Teacher> {

    private final Context context;
    private final int layoutResourceId;
    private final List<Teacher> data;
    private TeacherHolder holder;
    private LayoutInflater inflater;
    private boolean isDropdown;

    public TeachersAdapter(Context context, int layoutResourceId, List<Teacher> data, boolean isDropdown)
    {
        super(context, layoutResourceId, data);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
        this.inflater = LayoutInflater.from(context);
        this.isDropdown = isDropdown;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        Teacher element = data.get(position);
        View row = convertView;
        if (row == null) {
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new TeacherHolder();
            holder.container = (LinearLayout)row.findViewById(R.id.teacher_row_container);
            holder.container.setTag(element.getId());
            holder.name = (TextView)row.findViewById(R.id.teacher_row_name);
            holder.image = (ImageView)row.findViewById(R.id.teacher_row_image);

            row.setTag(holder);
        }else{
            holder = (TeacherHolder)row.getTag();
        }

        holder.name.setText(element.getName());

        if(element.getImage() != null)
            holder.image.setImageBitmap(ImageHelper.getBitmap(element.getImage().getData()));
        else
            holder.image.setImageResource(R.drawable.pm_default_teacher);

        if(!isDropdown){
            if(element.getIsActive() == 1)
                row.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.pm_selected_item));
            else
                holder.container.setBackgroundColor(Color.TRANSPARENT);
        }
        return row;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent)
    {
        return getView(position,convertView,parent);
    }

    static class TeacherHolder {
        TextView name;
        ImageView image;
        LinearLayout container;
    }
}
